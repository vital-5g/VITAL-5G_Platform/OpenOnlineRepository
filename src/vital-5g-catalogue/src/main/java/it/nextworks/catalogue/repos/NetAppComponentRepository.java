package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.NetAppComponent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NetAppComponentRepository extends JpaRepository<NetAppComponent, Long> {
}
