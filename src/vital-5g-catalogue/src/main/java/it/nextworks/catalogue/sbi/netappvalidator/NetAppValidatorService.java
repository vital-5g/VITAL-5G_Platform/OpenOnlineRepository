package it.nextworks.catalogue.sbi.netappvalidator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.sbi.netappvalidator.interfaces.NetAppValidationResponse;
import it.nextworks.catalogue.sbi.netappvalidator.interfaces.NetAppValidatorInterface;
import it.nextworks.catalogue.services.SecurityService;
import it.nextworks.netappvalidator.client.ApiClient;
import it.nextworks.netappvalidator.client.ApiException;
import it.nextworks.netappvalidator.client.api.NetAppValidatorClient;
import it.nextworks.netappvalidator.client.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class NetAppValidatorService implements NetAppValidatorInterface {

    private static final Logger log = LoggerFactory.getLogger(NetAppValidatorService.class);

    @Value("${netapp-validator.url:http://}")
    private String netAppValidatorUrl;

    @Value("${netapp-validator.dummy:false}")
    private boolean dummy;


    @Value("${netapp-validator.validate-sensors:true}")
    private boolean validateSensors;

    @Value("${netapp-validator.validate-syntaxis:true}")
    private boolean validateSyntaxis;

    @Autowired
    private SecurityService securityService;

    @Override
    public NetAppValidationResponse validateNetAppBlueprint(NetAppBlueprint blueprint) throws FailedOperationException {
        log.debug("Received request to validate NetApp Blueprint");

        if(!dummy){
            log.debug("Requesting external NetApp Blueprint validation");
            NetAppValidatorClient client = getNetAppValidatorClient();
            ApiClient apiClient = client.getApiClient();
            apiClient.addDefaultHeader("Authorization", securityService.getToken());
            try {
                //ObjectMapper objectMapper = new ObjectMapper();
                Response validationResponse= null;
                boolean isValid = true;
                if(validateSyntaxis){

                    validationResponse= client.apiBlueprintValidateSyntaxPost(blueprint);
                    isValid = validationResponse.isValid();
                    log.debug("Validating Syntax response:"+isValid+validationResponse.getValidationString());
                }
                if(isValid && validateSensors){
                    validationResponse= client.apiBlueprintValidateSensorsPost(blueprint);
                    isValid = validationResponse.isValid();
                    log.debug("Validating Sensor response:"+isValid+validationResponse.getValidationString());
                }
                String validationMessage="";
                if(validationResponse!=null)
                    validationMessage=validationResponse.getValidationString();

                return new NetAppValidationResponse(isValid, validationMessage );
            } catch (ApiException  e) {
                log.error("Error during NetApp Validation:",e);
                throw new FailedOperationException(e);
            }
        }else return new NetAppValidationResponse(true, "");

    }


    private NetAppValidatorClient getNetAppValidatorClient(){
        NetAppValidatorClient client = new NetAppValidatorClient();
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(netAppValidatorUrl)
                        .setDebugging(true)
                        .setWriteTimeout(0)
                        .setReadTimeout(0)
                        .setConnectTimeout(0);

        client.setApiClient(apiClient);
        return client;
    }
}
