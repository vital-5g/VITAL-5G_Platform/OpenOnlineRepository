/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.nbi;

import java.util.List;
import java.util.UUID;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.services.VsDescriptorCatalogueService;
import it.nextworks.catalogue.elements.VsDescriptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


@Api(tags = "Vertical Service Descriptor Catalogue API")

@RestController
@CrossOrigin
@RequestMapping("/portal/catalogue/vsds")
public class VsDescriptorCatalogueRestController {

	private static final Logger log = LoggerFactory.getLogger(VsDescriptorCatalogueRestController.class);
	
	@Autowired
	private VsDescriptorCatalogueService vsDescriptorCatalogueService;

	
	public VsDescriptorCatalogueRestController() { }


	@ApiOperation(value = "Onboard a new Vertical Service Descriptor")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "The ID of the created Vertical Service Descriptor.", response = String.class),
			@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			@ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
			@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> createVsDescriptor(@RequestBody VsDescriptor request) {
		log.debug("Received request to create a VS descriptor.");

		try {
			UUID vsDescriptorId = vsDescriptorCatalogueService.onBoardVsDescriptor(request);
			return new ResponseEntity<UUID>(vsDescriptorId, HttpStatus.CREATED);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request",e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (AlreadyExistingEntityException | FailedOperationException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@ApiOperation(value = "Query ALL the Vertical Service Descriptor")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "List of all the Vertical Service Descriptor of the user.", response = VsDescriptor.class, responseContainer = "Set"),
			@ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
	})
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAllVsDescriptors() {
		log.debug("Received request to retrieve all the VS descriptors.");

		try {

			List<VsDescriptor> response = vsDescriptorCatalogueService.queryVsDescriptor();
			return new ResponseEntity<List<VsDescriptor>>(response, HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation(value = "Get Vertical Service Descriptor")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message =  "Details of the Vertical Service Descriptor with the given ID", response = VsDescriptor.class),
			@ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
			@ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
			@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
	})
	@RequestMapping(value = "/{vsdId}", method = RequestMethod.GET)
	public ResponseEntity<?> getVsDescriptor(@PathVariable UUID vsdId) {
		log.debug("Received request to retrieve VS descriptor with ID " + vsdId);

		try {

			VsDescriptor response = vsDescriptorCatalogueService.getVsd(vsdId);
			return new ResponseEntity<VsDescriptor>(response, HttpStatus.OK);
		}  catch (NotExistingEntityException e) {
			log.error("VS Descriptor not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ApiOperation(value = "Delete Vertical Service Descriptor")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message =  "Deleted the Vertical Service Descriptor", response = VsDescriptor.class),
			@ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
			@ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
			@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
	})
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "/{vsdId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteVsDescriptor(@PathVariable UUID vsdId, @RequestParam(required = false) boolean forced) {
		log.debug("Received request to retrieve VS descriptor with ID " + vsdId);

		try {

			vsDescriptorCatalogueService.deleteVsDescriptor(vsdId, forced);
			return new ResponseEntity<>(HttpStatus.OK);
		}  catch (NotExistingEntityException e) {
			log.error("VS Descriptor not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (UnAuthorizedRequestException e) {

				return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);

		} catch (Exception e) {
			log.error("Internal exception",e );
			return new ResponseEntity<String>("Internal server error. Please contact a Platform Administrator", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	




	

	
}
