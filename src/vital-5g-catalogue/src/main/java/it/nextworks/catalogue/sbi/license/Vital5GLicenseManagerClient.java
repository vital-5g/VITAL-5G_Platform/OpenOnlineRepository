package it.nextworks.catalogue.sbi.license;

import it.nextworks.catalogue.elements.SoftwareLicense;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.services.NetAppPackageService;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class Vital5GLicenseManagerClient {

    private static final Logger log = LoggerFactory.getLogger(Vital5GLicenseManagerClient.class);

    @Value("${licensemgr.url:http://localhost:8090}")
    private String licenseManageUrl;
    private Vital5GElicenseRestClient restClient;

    @Autowired
    private KeycloakRestTemplate restTemplate;


    public UUID createLicense(UUID netappBlueprintId, SoftwareLicense license) throws MalformattedElementException, FailedOperationException{
        log.debug("Received request to create a license");
        restClient=new Vital5GElicenseRestClient(licenseManageUrl,  restTemplate);
        try {
            return restClient.onboardNetAppLicense(netappBlueprintId, license);
        } catch (UnAuthorizedRequestException e) {
            throw new FailedOperationException(e.getMessage());
        }
    }
}
