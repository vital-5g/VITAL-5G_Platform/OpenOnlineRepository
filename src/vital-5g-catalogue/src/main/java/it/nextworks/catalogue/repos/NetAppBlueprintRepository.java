package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.elements.NetAppSpecLevel;
import it.nextworks.catalogue.elements.Testbed;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NetAppBlueprintRepository extends JpaRepository<NetAppBlueprint, Long> {

    Optional<NetAppBlueprint> findByNameAndVersion(String name, String version);
    List<NetAppBlueprint> findByTestbed(Testbed testbed);

    List<NetAppBlueprint> findByAccessLevel(AccessLevel accessLevel);
    List<NetAppBlueprint> findBySpecLevel(NetAppSpecLevel specLevel);

}
