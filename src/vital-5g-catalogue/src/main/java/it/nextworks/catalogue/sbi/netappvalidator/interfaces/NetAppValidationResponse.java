package it.nextworks.catalogue.sbi.netappvalidator.interfaces;

public class NetAppValidationResponse {

    private boolean successful;
    private String errorMsg;

    public NetAppValidationResponse(boolean successful, String errorMsg) {
        this.successful = successful;
        this.errorMsg = errorMsg;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
