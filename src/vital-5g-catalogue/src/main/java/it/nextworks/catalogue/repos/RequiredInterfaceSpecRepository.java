package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.RequiredInterfaceServiceSpec;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequiredInterfaceSpecRepository extends JpaRepository<RequiredInterfaceServiceSpec, Long> {
}
