package it.nextworks.catalogue.sbi.nfvo.interfaces;

import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.FailedOperationException;

import java.io.File;
import java.util.UUID;

public interface NfvoCatalogueInterface {

    UUID onboardNSD(File nsd, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException;

    UUID onboardVNF(File vnfPackage, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException;
}
