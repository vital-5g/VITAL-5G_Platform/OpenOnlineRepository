/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.sbi.license;


import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.messages.OnboardVSBRequest;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.*;


public class Vital5GElicenseRestClient{

	private static final Logger log = LoggerFactory.getLogger(Vital5GElicenseRestClient.class);

	private KeycloakRestTemplate restTemplate;


	private String licenseUrl;

	public Vital5GElicenseRestClient(String baseUrl, KeycloakRestTemplate restTemplate) {

	    this.licenseUrl = baseUrl + "/portal/license/";
	    this.restTemplate=restTemplate;
	   // this.restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));


	}





	public UUID  onboardNetAppLicense(UUID blueprintId, SoftwareLicense softwareLicense) throws
			 FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		log.debug("Building HTTP request for onboard License with ID ");
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");

		HttpEntity<?> getEntity = new HttpEntity<SoftwareLicense>(softwareLicense, header);

		String url = licenseUrl+"?netappBlueprintId={netappBlueprintId}";

		log.debug("Sending HTTP request to Onboard License.");
		Map<String, String> params = new HashMap<>();
		params.put("netappBlueprintId", blueprintId.toString());
		try {
			ResponseEntity<UUID> httpResponse =
					restTemplate.exchange(url, HttpMethod.POST, getEntity, UUID.class, params);


			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();

			if (code.equals(HttpStatus.CREATED)) {
				log.debug("License correctly onboarded");
				return httpResponse.getBody();

			} else if (code.equals(HttpStatus.FORBIDDEN)) {
				throw new UnAuthorizedRequestException(httpResponse.getBody().toString());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during License Creation: " + httpResponse.getBody());

			} else {
				throw new FailedOperationException("Generic error during interaction with License Management module");
			}
		}catch (HttpClientErrorException e){
			if(e.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
				throw new MalformattedElementException(e.getMessage());
			}else throw new FailedOperationException(e.getMessage());
		}


	}



}
