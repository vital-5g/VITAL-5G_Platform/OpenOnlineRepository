package it.nextworks.catalogue.sbi.nfvo.osm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.sbi.inventory.elements.TestbedNfvoInformation;
import it.nextworks.catalogue.sbi.nfvo.NfvoCatalogueService;
import it.nextworks.catalogue.sbi.nfvo.interfaces.NfvoCatalogueInterface;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.ApiClient;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.ApiException;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.api.NsPackagesApi;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.api.VnfPackagesApi;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.auth.OAuthSimpleClient;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.model.CreateNsdInfoRequest;
import it.nextworks.catalogue.sbi.nfvo.osm.rest.model.ObjectId;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.UUID;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

import java.io.*;
import java.nio.file.Files;
import java.util.zip.GZIPOutputStream;

public class Osm10CatalogueClient implements NfvoCatalogueInterface {

    private static final Logger log = LoggerFactory.getLogger(Osm10CatalogueClient.class);
    private NsPackagesApi nsPackagesApi;
    private VnfPackagesApi vnfPackagesApi;
    private TestbedNfvoInformation testbedNfvoInformation;
    private final OAuthSimpleClient oAuthSimpleClient;

    public Osm10CatalogueClient(TestbedNfvoInformation info){
        nsPackagesApi = new NsPackagesApi();
        vnfPackagesApi = new VnfPackagesApi();
        this.testbedNfvoInformation = info;
        this.oAuthSimpleClient = new OAuthSimpleClient(info.getBaseUrl()+"/admin/v1/tokens",
                info.getUsername(),
                info.getPassword(),
                info.getProject());

    }


    @Override
    public UUID onboardNSD(File nsd, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException {
        log.debug("Received request for NSD onboarding");
        nsPackagesApi.setApiClient(getClient());

        try {
            ObjectId oNsPackageId = nsPackagesApi.addNSD(new CreateNsdInfoRequest());
            UUID  nsPackageId = oNsPackageId.getId();

            File targetFile = nsd;
            if(nsd.getAbsolutePath().endsWith("yaml")){
                log.debug("Received request to  onboard NSD using YAML format");


// Instantiating a new ObjectMapper as a YAMLFactory
                ObjectMapper om = new ObjectMapper(new YAMLFactory());
                log.debug(om.writerWithDefaultPrettyPrinter().writeValueAsString(om.readTree(targetFile)));
                log.debug("Compressing NSD yaml into an NS package");
                String fileNameWithOutExt = FilenameUtils.removeExtension(nsd.getName());

                File nsPackageDir = Files.createTempDirectory(nsPackageId.toString()).toFile();
                log.debug("Created folder for NS Package: "+nsPackageDir.getAbsolutePath());
                FileUtils.copyFileToDirectory(nsd, nsPackageDir);
                File nsPackage = File.createTempFile(nsPackageId.toString(), ".tar.gz");
                log.debug("Created file for NS Package: "+nsPackage.getAbsolutePath());
                log.debug("Created temp package file:"+nsPackage.getAbsolutePath());
                compress(nsPackageDir, nsPackage);
                targetFile = nsPackage;
            }else log.debug("Ignoring unknown file extension for: "+nsd.getAbsolutePath());


            log.debug("Successfully created NSD Info ID: "+nsPackageId);
            nsPackagesApi.updateNSDcontent(nsPackageId.toString(), targetFile);
            log.debug("Successfully onboarded NSD content for NSD Info ID: "+nsPackageId);
            return nsPackageId;
        } catch (ApiException e){
            log.warn("Error during NSD onboarding", e);
            if(e.getCode()==409){
                throw new AlreadyExistingEntityException("CONFLICT exception while onboarding NSD on NFVO");
            }else throw new FailedOperationException(e);
        } catch (IOException e) {
           log.error("Error during NSD onboarding", e);
           throw new FailedOperationException("Error during NFVO NSD onboarding. Received error:"+e.getMessage());
        }

    }

    @Override
    public UUID onboardVNF(File vnfPackage, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException {
        log.debug("Received request for VNF Package onboarding");
        vnfPackagesApi.setApiClient(getClient());


        try {
            ObjectId vnfPackageRecordId = vnfPackagesApi.uploadVnfPkgsContent(vnfPackage);
            log.debug("Onboarded VNF with record ID:"+vnfPackageRecordId.getId());
            return vnfPackageRecordId.getId();
        } catch (ApiException e) {
            if(e.getCode()==409){
                throw new AlreadyExistingEntityException("CONFLICT exception while onboarding VNF on NFVO");
            }else throw new FailedOperationException(e);
        }
    }


    private static void compress(File input, File output) throws IOException {
        log.debug("Compressing input file:"+input.getAbsolutePath()+" to: "+output.getAbsolutePath());
        File rootDir = input.getParentFile();

        try (
                FileOutputStream fos = new FileOutputStream(output);
                GZIPOutputStream gos = new GZIPOutputStream(new BufferedOutputStream(fos));
                TarArchiveOutputStream tos = new TarArchiveOutputStream(gos)
        ) {
            SimpleFileVisitor<Path> archiver = new SimpleFileVisitor<Path>() {

                private File ROOT = input.getParentFile();

                private String relPath(Path target) {
                    return ROOT.toPath().relativize(target).toString();
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {
                    tos.putArchiveEntry(new TarArchiveEntry(file.toFile(), relPath(file)));
                    Files.copy(file, tos);
                    tos.closeArchiveEntry();
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes basicFileAttributes)
                        throws IOException {
                    tos.putArchiveEntry(new TarArchiveEntry(path.toFile(), relPath(path)));
                    tos.closeArchiveEntry();
                    return FileVisitResult.CONTINUE;
                }
            };
            Files.walkFileTree(input.toPath(), archiver);

        } catch (IOException e) {
            throw new IllegalStateException(
                    String.format("Could not compress package. Error: %s", e.getMessage())
            );
        }
    } //same of ArchiveBuilder




    private ApiClient getClient() throws FailedOperationException {

        ApiClient apiClient = new ApiClient();
        apiClient.setHttpClient(OAuthSimpleClient.getUnsafeOkHttpClient());
        apiClient.setBasePath(this.testbedNfvoInformation.getBaseUrl());
        apiClient.setUsername(this.testbedNfvoInformation.getUsername());
        apiClient.setPassword(this.testbedNfvoInformation.getPassword());
        apiClient.setAccessToken(oAuthSimpleClient.getToken());
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        apiClient.getHttpClient().interceptors().add(interceptor);
        apiClient.setDebugging(true);
        apiClient.setConnectTimeout(0);
        apiClient.setReadTimeout(0);
        apiClient.setWriteTimeout(0);
        return apiClient;
    }
}
