package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.InfrastructureMetric;
import it.nextworks.catalogue.elements.RuntimeAction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuntimeActionRepository extends JpaRepository<RuntimeAction, Long> {
}
