package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.ConnectivityService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NetAppConnectivityServiceRepository extends JpaRepository<ConnectivityService, Long> {
}
