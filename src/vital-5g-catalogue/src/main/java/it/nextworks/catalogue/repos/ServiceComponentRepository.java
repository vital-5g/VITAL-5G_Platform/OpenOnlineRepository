package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.ServiceComponent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceComponentRepository extends JpaRepository<ServiceComponent, Long> {
}
