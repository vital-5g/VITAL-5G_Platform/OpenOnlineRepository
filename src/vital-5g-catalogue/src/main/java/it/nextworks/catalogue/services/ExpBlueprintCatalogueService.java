/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.catalogue.services;



import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.repos.KeyPerformanceIndicatorRepository;
import it.nextworks.catalogue.repos.ExpBlueprintInfoRepository;
import it.nextworks.catalogue.repos.ExpBlueprintRepository;
import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpBlueprintInfo;
import it.nextworks.catalogue.elements.exp.KeyPerformanceIndicator;
import it.nextworks.catalogue.interfaces.ExpBlueprintCatalogueInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExpBlueprintCatalogueService implements ExpBlueprintCatalogueInterface {

	private static final Logger log = LoggerFactory.getLogger(ExpBlueprintCatalogueService.class);

	@Autowired
	private SecurityService securityService;

	@Autowired
	private ExpBlueprintRepository expBlueprintRepository;

	@Autowired
	private ExpBlueprintInfoRepository expBlueprintInfoRepository;


	@Autowired
	private VsBlueprintCatalogueService vsBlueprintRepository;



	@Autowired
	private KeyPerformanceIndicatorRepository keyPerformanceIndicatorRepository;




	public ExpBlueprintCatalogueService() {	}

	@Override
	public synchronized UUID onboardExperimentBlueprint(ExpBlueprint expb)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException, UnAuthorizedRequestException {
		log.debug("Processing request to onboard a new Exp blueprint");
		expb.isValid();

		VerticalServiceBlueprint verticalServiceBlueprint =  vsBlueprintRepository.getVerticalServiceBlueprint(expb.getVsBlueprintId());
		if(!expb.getTestbed().equals(verticalServiceBlueprint.getTestbed())){
			throw new MalformattedElementException("ExpB and VSB testbed mismatch");
		}
		UUID expbId = storeExpBlueprint(expb);


		return expbId;



	}



	@Override
	public List<ExpBlueprintInfo> queryExperimentBlueprint(Testbed testbed)
			throws MalformattedElementException,  FailedOperationException {
		log.debug("Processing request to query an Experiment blueprint");
		List<ExpBlueprint> expBlueprints = new ArrayList<>();
		if(testbed!=null){
			expBlueprints = expBlueprintRepository.findByTestbed(testbed);

		}else throw new MalformattedElementException("No query Experiment Blueprint query parameter provided");

		return  expBlueprints.stream().filter(expb->authorizeEXPBAccess(expb.getExpBlueprintId())).map(expb -> expb.getExpBlueprintInfo()).collect(Collectors.toList());
	}


	@Override
	public synchronized  void deleteExperimentBlueprint(UUID expBlueprintId, boolean force) throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {


		log.debug("Processing request to delete a Experiment blueprint with ID " + expBlueprintId);

		if (expBlueprintId == null) throw new MalformattedElementException("Experiment Blueprint ID not provided");

		ExpBlueprintInfo expbi = getExpBlueprintInfo(expBlueprintId);
		String username = securityService.getUsername();
		if(expbi.getOwner()!=null){
			if(!expbi.getOwner().equals(username) && !securityService.userIsPlatformAdmin()){
			throw new UnAuthorizedRequestException("Only the ExpB owner or the Platform Admin can delete an ExpB");
			}
		}else log.debug("ExpB without owner. Skipping access control");
		if(force){

		}else if (!(expbi.getActiveExpdId().isEmpty())) {
			log.error("There are some ExpDs associated to the Experiment Blueprint. Try with forced");
			throw new FailedOperationException("There are some ExpDs associated to the Experiment Blueprint. Impossible to remove it.");

		}

		ExpBlueprint expb = getExperimentBlueprint(expBlueprintId);
		expBlueprintRepository.delete(expb);
		log.debug("Removed ExpB from DB.");
		expBlueprintInfoRepository.delete(expbi);
		log.debug("Removed ExpB info from DB.");


	}

	@Override
	public List<ExpBlueprintInfo> getExperimentBlueprintsByTestCaseBlueprint(UUID testCaseBlueprintId) {
		log.debug("Processing request to query an Experiment blueprint associated with TestCaseBlueprint:"+ testCaseBlueprintId);
		List<ExpBlueprint> tcExpBlueprints = expBlueprintRepository.findByTcBlueprintId(testCaseBlueprintId);
		return tcExpBlueprints.stream().map(expb-> expb.getExpBlueprintInfo()).collect(Collectors.toList());
	}


	public synchronized void addExpdInBlueprint(UUID expBlueprintId, UUID expdId)
			throws NotExistingEntityException {
		log.debug("Adding EXPD " + expdId + " to blueprint " + expBlueprintId);
		ExpBlueprintInfo expi = getExpBlueprintInfo(expBlueprintId);
		expi.addExpd(expdId);
		expBlueprintInfoRepository.saveAndFlush(expi);
		log.debug("Added ExpD " + expdId + " to blueprint " + expBlueprintId);
	}

	public synchronized void removeExpdInBlueprint(UUID expBlueprintId, UUID expdId)
			throws NotExistingEntityException {
		log.debug("Removing EXPD " + expdId + " from blueprint " + expBlueprintId);
		ExpBlueprintInfo expi = getExpBlueprintInfo(expBlueprintId);
		expi.removeExpd(expdId);;
		expBlueprintInfoRepository.saveAndFlush(expi);
		log.debug("Removed ExpD " + expdId + " from blueprint " + expBlueprintId);
	}


	private ExpBlueprint getExpBlueprint(String name, String version) throws NotExistingEntityException {
		Optional<ExpBlueprint> expBlueprintOpt = expBlueprintRepository.findByNameAndVersion(name, version);
		if (expBlueprintOpt.isPresent()) return expBlueprintOpt.get();
		else throw new NotExistingEntityException("Experiment Blueprint with name " + name + " and version " + version + " not found in DB.");
	}

	private ExpBlueprintInfo getExpBlueprintInfo(String name, String version) throws NotExistingEntityException {
		ExpBlueprintInfo expBlueprintInfo;
		Optional<ExpBlueprintInfo> expbInfoOpt = expBlueprintInfoRepository.findByNameAndVersion(name, version);
		if (expbInfoOpt.isPresent()) expBlueprintInfo = expbInfoOpt.get();
		else throw new NotExistingEntityException("Experiment Blueprint info with name " + name + " and version " + version + " not found in DB.");
		ExpBlueprint expb = getExpBlueprint(name, version);
		expBlueprintInfo.setExpBlueprint(expb);
		return expBlueprintInfo;
	}

	public ExpBlueprint getExperimentBlueprint(UUID expbId) throws NotExistingEntityException, MalformattedElementException {
		log.debug("Received request to retrieve Experiment Blueprint:"+expbId);
		if(expbId==null) throw new MalformattedElementException("Experiment Blueprint ID not provided");
		Optional<ExpBlueprint> expBlueprintOpt = expBlueprintRepository.findByExpBlueprintId(expbId);
		if (expBlueprintOpt.isPresent()) return expBlueprintOpt.get();
		else throw new NotExistingEntityException("Experiment Blueprint with ID " + expbId + " not found in DB.");
	}

	private ExpBlueprintInfo getExpBlueprintInfo(UUID expbId) throws NotExistingEntityException {
		ExpBlueprintInfo expBlueprintInfo;
		Optional<ExpBlueprintInfo> expbInfoOpt = expBlueprintInfoRepository.findByExpBlueprintId(expbId);
		if (expbInfoOpt.isPresent()) expBlueprintInfo = expbInfoOpt.get();
		else throw new NotExistingEntityException("Experiment Blueprint info for ExpB with ID " + expbId + " not found in DB.");

		return expBlueprintInfo;
	}

	private List<ExpBlueprintInfo> getExpBlueprintInfoFromVsb(UUID vsbId) throws NotExistingEntityException {
		log.debug("Searching for experiment blueprints associated to VSB " + vsbId);
		List<ExpBlueprint> expbs = expBlueprintRepository.findByVsBlueprintId(vsbId);
		if (expbs.isEmpty()) throw new NotExistingEntityException("Could NOT find ExpB associated to VSB with ID " + vsbId);
		List<ExpBlueprintInfo> expbis = new ArrayList<>();
		for (ExpBlueprint expb : expbs) {
			UUID expbId = expb.getExpBlueprintId();
			ExpBlueprintInfo expbi = expBlueprintInfoRepository.findByExpBlueprintId(expbId).get();
			expbi.setExpBlueprint(expb);
			expbis.add(expbi);
			log.debug("Added ExpB with ID " + expbId);
		}
		return expbis;
	}

	public List<ExpBlueprintInfo> getAllExperimentBlueprints()  {
		List<ExpBlueprintInfo> expBInfos = expBlueprintInfoRepository.findAll();

		return expBInfos;
	}

	private UUID storeExpBlueprint(ExpBlueprint expBlueprint) throws AlreadyExistingEntityException {

		log.debug("Onboarding EXP blueprint with name " + expBlueprint.getName() + " and version " + expBlueprint.getVersion());
		if ( (expBlueprintInfoRepository.findByNameAndVersion(expBlueprint.getName(), expBlueprint.getVersion()).isPresent())) {
			log.error("EXP Blueprint with name " + expBlueprint.getName() + " and version " + expBlueprint.getVersion() + " already present in DB.");
			throw new AlreadyExistingEntityException("ExpB with name " + expBlueprint.getName() + " and version " + expBlueprint.getVersion() + " already present in DB.");
		}
		ExpBlueprintInfo expbInfo = new ExpBlueprintInfo(expBlueprint.getVersion(), expBlueprint.getName(), securityService.getUsername()) ;
		expBlueprintInfoRepository.saveAndFlush(expbInfo);
		log.debug("Added Experiment Blueprint Info with ID " + expbInfo.getExpBlueprintId());
		ExpBlueprint target = new ExpBlueprint(
				expbInfo,
				expBlueprint.getVersion(),
				expBlueprint.getName(),
				expBlueprint.getDescription(),
				expBlueprint.getTestbed(),
				expBlueprint.getVsBlueprintId(),
				expBlueprint.getTcBlueprintId(),
				expBlueprint.getMetrics(),
				expBlueprint.getAccessLevel(),
				expBlueprint.getUseCase()
		);

		expBlueprintRepository.saveAndFlush(target);


		List<KeyPerformanceIndicator> kpis = expBlueprint.getKpis();
		if (kpis != null) {
			for (KeyPerformanceIndicator kpi : kpis) {
				KeyPerformanceIndicator targetKpi = new KeyPerformanceIndicator(target,
						kpi.getKpiId(),
						kpi.getName(),
						kpi.getFormula(),
						kpi.getUnit(),
						kpi.getMetricIds(),
						kpi.getInterval(),
						kpi.getGraphType());
				keyPerformanceIndicatorRepository.saveAndFlush(targetKpi);
				log.debug("Stored kpi " + kpi.getKpiId() + " in experiment blueprint " + expbInfo.getExpBlueprintId());
			}
		}




		return expbInfo.getExpBlueprintId();
	}


	private boolean authorizeEXPBAccess(UUID expbId){
		String username = securityService.getUsername();
		log.debug("Authorizing access to EXPB:" +expbId+" to:"+username);
		ExpBlueprintInfo info = expBlueprintInfoRepository.findByExpBlueprintId(expbId).get();
		ExpBlueprint blueprint = expBlueprintRepository.findByExpBlueprintId(expbId).get();
		if(info.getOwner()!=null && info.getOwner().equals(username)){
			log.debug("User is owner. Access granted");
			return true;
		}else if(securityService.userIsPlatformAdmin()){
			log.debug("User is admin. Access granted");
			return true;
		}else if(blueprint.getAccessLevel().equals(AccessLevel.PUBLIC)){
			log.debug("ExpB is PUBLIC. Access granted");
			return true;

		}else if(blueprint.getAccessLevel().equals(AccessLevel.RESTRICTED) && securityService.userInUseCase(blueprint.getUseCase())){
			log.debug("ExpB is RESTRICTED, user in use case. Access granted");
			return true;
		}else return false;
	}


}
