package it.nextworks.catalogue;

import it.nextworks.catalogue.keycloak.CustomKeycloakSpringBootConfigResolver;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.filter.KeycloakPreAuthActionsFilter;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.savedrequest.NullRequestCache;

@KeycloakConfiguration
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {



    @Value("${catalogue.admin-role:Platform_admin}")
    private String adminRole;

    @Value("${catalogue.technician-role:Platform_Technician}")
    private String technicianRole;

    @Value("${catalogue.netapp-developer-role:NetApp_Developer}")
    private String netAppDeveloperRole;

    @Value("${catalogue.vertical-service-provider-role:Vertical_Service_Provider}")
    private String vspRole;

    @Value("${catalogue.experimenter-role:Experimenter}")
    private String experimenterRole;

    @Value("${catalogue.testbed-admin-role:Testbed_Admin}")
    private String testbedAdminRole;

    @Value("${catalogue.site-infrastructure-manager-role:T&L_Site Infrastructure_Manager}")
    private String simRole;


    /**
     * Registers the KeycloakAuthenticationProvider with the authentication manager.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Provide a session authentication strategy bean which should be of type
     * RegisterSessionAuthenticationStrategy for public or confidential applications
     * and NullAuthenticatedSessionStrategy for bearer-only applications.
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    /**
     * Use properties in application.properties instead of keycloak.json
     */
    @Bean
    @Primary
    public KeycloakConfigResolver keycloakConfigResolver(KeycloakSpringBootProperties properties) {
        return new CustomKeycloakSpringBootConfigResolver(properties);
    }

    /**
     * Secure appropriate endpoints
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/v2/api-docs").permitAll()
                .antMatchers(HttpMethod.GET, "/portal/catalogue/netapppackages/").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/netapppackages/").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/netapppackages").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/netapppackages/{netappPackageId}").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.DELETE, "/portal/catalogue/netapppackages/{netappPackageId}").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole)
                .antMatchers(HttpMethod.PUT, "/portal/catalogue/netapppackages/{netappPackageId}").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/netapppackages/{netappPackageId}").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/netapppackages/{netappPackageId}/**").hasAnyAuthority(adminRole, netAppDeveloperRole, technicianRole, experimenterRole, vspRole)

                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsbs").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsbs/").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/vsbs").hasAnyAuthority(adminRole, vspRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/vsbs/").hasAnyAuthority(adminRole, vspRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsbs/{vsbid}").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.DELETE, "/portal/catalogue/vsbs/{vsbId}").hasAnyAuthority(adminRole, vspRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsbs/{vsbId}/**").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)


                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsds").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsds/").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/vsds/").hasAnyAuthority(adminRole, vspRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/vsds").hasAnyAuthority(adminRole, vspRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/vsds/{vsdid}").hasAnyAuthority(adminRole,  technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.DELETE, "/portal/catalogue/vsds/{vsdId}").hasAnyAuthority(adminRole, vspRole, experimenterRole, technicianRole)

                .antMatchers(HttpMethod.GET, "/portal/catalogue/translator/{vsdId}").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)


                .antMatchers(HttpMethod.GET, "/portal/catalogue/expbs").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/expbs/").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/expbs/").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/expbs").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/expbs/{expbId}").hasAnyAuthority(adminRole, experimenterRole, technicianRole, netAppDeveloperRole, vspRole)
                .antMatchers(HttpMethod.DELETE, "/portal/catalogue/expbs/{expbid}").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.PUT, "/portal/catalogue/expbs/{expbid}").hasAnyAuthority(adminRole, experimenterRole, technicianRole)


                .antMatchers(HttpMethod.GET, "/portal/catalogue/tcbs").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/tcbs/").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/tcbs/").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.POST, "/portal/catalogue/tcbs").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.GET, "/portal/catalogue/tcbs/{tcbId}").hasAnyAuthority(adminRole, experimenterRole, technicianRole, netAppDeveloperRole, vspRole)
                .antMatchers(HttpMethod.DELETE, "/portal/catalogue/tcbs/{tcbid}").hasAnyAuthority(adminRole, experimenterRole, technicianRole)
                .antMatchers(HttpMethod.PUT, "/portal/catalogue/tcbs/{tcbid}").hasAnyAuthority(adminRole, experimenterRole, technicianRole)



                .anyRequest().authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}