/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.catalogue.services.ExpDescriptorCatalogueService;

import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.exp.ExpDescriptorInfo;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@Api(tags = "Experiment Descriptor Catalogue API")
@RestController
@CrossOrigin
@RequestMapping("/portal/catalogue/expds")
public class ExpDescriptorCatalogueRestController {

	private static final Logger log = LoggerFactory.getLogger(ExpDescriptorCatalogueRestController.class);
	
	@Autowired
	private ExpDescriptorCatalogueService expDescriptorCatalogueService;
	



	public ExpDescriptorCatalogueRestController() { } 
	
	@ApiOperation(value = "Onboard Experiment Descriptor")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Element created. Returns the id of the element created.", response = UUID.class),
			@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> createExpDescriptor(@RequestBody ExpDescriptor request) {
		log.debug("Received request to onboard an ExpD");

		try {
			UUID expDescriptorId = expDescriptorCatalogueService.onboardExpDescriptor(request);
			return new ResponseEntity<UUID>(expDescriptorId, HttpStatus.CREATED);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request"+e.getMessage(), e );
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (AlreadyExistingEntityException e) {
			log.error("EXP Descriptor already existing");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}


	}
	
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAllExpDescriptors() {
		log.debug("Received request to retrieve all the EXP descriptors.");

		try {
			List<ExpDescriptorInfo> response= expDescriptorCatalogueService.queryExpDescriptor();


			return new ResponseEntity<List<ExpDescriptorInfo>>(response, HttpStatus.OK);
		}  catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{expdId}", method = RequestMethod.GET)
	public ResponseEntity<?> getExpDescriptor(@PathVariable UUID expdId) {
		log.debug("Received request to retrieve EXP descriptor with ID " + expdId);



		try {


			return new ResponseEntity<ExpDescriptor>(expDescriptorCatalogueService.getExpDescriptor(expdId), HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request", e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("EXP Descriptor not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{expdId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteExpDescriptor(@PathVariable UUID expdId, @RequestParam(required = false) boolean force) {
		log.debug("Received request to delete EXP descriptor with ID " + expdId);

		try {

			expDescriptorCatalogueService.deleteExpDescriptor(expdId, force);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("EXP Blueprints not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@RequestMapping(value = "/{expdId}/use/{experimentId}", method = RequestMethod.POST)
	public ResponseEntity<?> useExpDescriptor(@PathVariable UUID expdId, @PathVariable UUID experimentId) {
		log.debug("Received request to use EXP descriptor with ID " + expdId+" in experiment:"+experimentId);

		try {

			expDescriptorCatalogueService.useExpDescriptor(expdId, experimentId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("EXPD not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/expdId}/release/{experimentId}", method = RequestMethod.POST)
	public ResponseEntity<?> releaseExpDescriptor(@PathVariable UUID expdId, @PathVariable UUID experimentId) {
		log.debug("Received request to use EXP descriptor with ID " + expdId+" in experiment:"+experimentId);

		try {

			expDescriptorCatalogueService.releaseExpDescriptor(expdId, experimentId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("EXPD not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
