package it.nextworks.catalogue.services;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jlefebure.spring.boot.minio.MinioException;
import com.jlefebure.spring.boot.minio.MinioService;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.interfaces.NetAppPackageManagementInterface;
import it.nextworks.catalogue.repos.*;

import it.nextworks.catalogue.sbi.license.Vital5GLicenseManagerClient;
import it.nextworks.catalogue.sbi.netappvalidator.NetAppValidatorService;
import it.nextworks.catalogue.sbi.netappvalidator.interfaces.NetAppValidationResponse;
import it.nextworks.catalogue.sbi.nfvo.NfvoCatalogueService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.interfaces.*;
import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class NetAppPackageService implements NetAppPackageManagementInterface {


    private static final Logger log = LoggerFactory.getLogger(NetAppPackageService.class);

    @Value("${catalogue.netapp.folder:/tmp/netapps/}")
    private String netAppFolder;

    @Value("${licensemgr.enable:true}")
    private boolean enableLicenseMgr;


    @Value("${catalogue.netapp.storage_type:MINIO}")
    private String storageType;

    @Autowired
    private NetAppBlueprintRepository netAppBlueprintRepository;

    @Autowired
    private NetAppPackageInfoRepository netAppPackageInfoRepository;

    @Autowired
    private NetAppComponentRepository netAppComponentRepository;

    @Autowired
    private NetAppEndpointRepository netAppEndpointRepository;

    @Autowired
    private InfrastructureMetricRepository infrastructureMetricRepository;

    @Autowired
    private NetAppConnectivityServiceRepository netAppConnectivityServiceRepository;

    @Autowired
    private ProvidedInterfaceSpecRepository providedInterfaceSpecRepository;

    @Autowired
    private RequiredInterfaceSpecRepository requiredInterfaceSpecRepository;
    @Autowired
    private SliceProfileRepository sliceProfileRepository;

    @Autowired
    private SoftwareLicenseRepo softwareLicenseRepo;

    @Autowired
    private MinioService minioService;

    @Autowired
    private NfvoCatalogueService nfvoCatalogueService;

    @Autowired
    private NetAppValidatorService netAppValidatorService;

    @Autowired
    private SecurityService securityService;


    @Autowired
    private Vital5GLicenseManagerClient licenseManagerService;

    @PostConstruct
    private void netAppServicePostContstruct(){

        if(storageType.equals("FILE")){
            log.debug("Creating NetApp package folder:"+netAppFolder);
            File netAppDir = new File(netAppFolder);
            if(!netAppDir.exists()){
                if (netAppDir.mkdir()) {

                    log.debug("Successfully created NetApp package folder");
                }
                else {
                    log.error("Failed to create NetApp package folder");
                }
            }else log.debug("File exists. IGNORING");
        }


    }

    private NetAppBlueprint extractBlueprintFromPackage(String  tempPath) throws FailedOperationException, IOException, MalformattedElementException {

        File blueprintFile = new File(tempPath+File.separator+"blueprint.json");
        ObjectMapper mapper = new ObjectMapper();
        if(!blueprintFile.exists())
            throw new MalformattedElementException("NetApp Package without blueprint file");
        NetAppBlueprint blueprint = null;
        try{
            blueprint = mapper.readValue(blueprintFile, NetAppBlueprint.class);
            return blueprint;
        } catch (JsonMappingException e1){
            log.error("NetApp blueprint deserialization error:", e1);
            throw new MalformattedElementException("Malformatted NetApp Blueprint: "+e1.getMessage());
        }


    }

    private void validateNetAppBlueprint(NetAppBlueprint blueprint) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException {
        Optional<NetAppPackageInfo> sInfo = netAppPackageInfoRepository.findByNameAndVersion(blueprint.getName(), blueprint.getVersion());
        if(sInfo.isPresent())
            throw new AlreadyExistingEntityException("NetApp with name and version already existing in the catalogue");

        blueprint.isValid();
        NetAppValidationResponse response = netAppValidatorService.validateNetAppBlueprint(blueprint);
        if(!response.isSuccessful()){
            log.debug("Error received from NetApp Validator:"+ response.getErrorMsg());
            throw new MalformattedElementException("Malformatted NetApp blueprint. Message:"+response.getErrorMsg());
        }
        log.debug("Successfully validated NetApp blueprint");
    }

    private UUID onboardNetAppBlueprint(NetAppBlueprint blueprint, File file, String tempPath) throws FailedOperationException, MalformattedElementException {
        try{
            String username = securityService.getUsername();
            NetAppPackageInfo info = new NetAppPackageInfo(blueprint.getName(), blueprint.getVersion(), null, blueprint.getTestbed(), username);
            netAppPackageInfoRepository.saveAndFlush(info);
            log.debug("Successfully created NetApp Package info");
            storeNetAppBlueprint(blueprint, info, tempPath);
            String fileExtension = FilenameUtils.getExtension(file.getName());

            if(storageType.equals("FILE")){
                log.debug("Using file storage backend");
                Path originalPath = Paths.get(file.getAbsolutePath());
                Path copied = Paths.get(netAppFolder+"/"+info.getNetAppPackageId().toString()+"."+fileExtension);

                Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);

            }else if(storageType.equals("MINIO")){
                log.debug("Using MINIO storage backend");
                Path minioPath = Paths.get(info.getNetAppPackageId()+File.separator+"package.zip");
                log.debug("Uploading package");
                minioService.upload(minioPath, new FileInputStream(file), "application/zip");
                log.debug("Uploading licenses");
                for(SoftwareLicense license: blueprint.getSoftwareLicenses()){
                    File licenseFile = new File(tempPath+File.separator+license.getLicenseFile());

                    Path minioLicensePath =  Paths.get(info.getNetAppPackageId()+File.separator+"license"+File.separator+license.getSoftwareLicenseId());
                    minioService.upload(minioLicensePath, new FileInputStream(licenseFile), "text/*");
                }
            }else log.warn("UNSUPPORTED storage type");
            return  info.getNetAppPackageId();

        } catch (IOException | MinioException e) {

            throw new FailedOperationException("IO Error during NetApp Package unpacking/storing");
        }
    }

    @Override
    public UUID onboardNetAppPackage(File file) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
        log.debug("Received request to onboard a new NetApp Package");
        if(!securityService.userIsNetAppDeveloper() && !securityService.userIsPlatformAdmin())
            throw new UnAuthorizedRequestException("User does NOT have NetApp Developer or admin roles");

        NetAppBlueprint blueprint = null;
        try {
            String tempPath = unpackZipFile(file);
            blueprint = extractBlueprintFromPackage(tempPath);
            validateNetAppBlueprint(blueprint);
            return onboardNetAppBlueprint(blueprint,file, tempPath);
        } catch (IOException e) {
            throw new FailedOperationException("IO Error during NetApp Package unpacking");
        }



    }

    @Override
    public UUID onboardNetAppPackage(File file, File vnfPackage) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
        log.debug("Received request to onboard a new NetApp Package and VNF");
        if(!securityService.userIsNetAppDeveloper() && !securityService.userIsPlatformAdmin())
            throw new UnAuthorizedRequestException("User does NOT have NetApp Developer or admin roles");

        NetAppBlueprint blueprint = null;

        String tempPath= null;
        try {
            tempPath = unpackZipFile(file);
            blueprint = extractBlueprintFromPackage(tempPath);
        } catch (IOException e) {
            log.error("Error during NetApp Package unpacking:", e);
            throw  new FailedOperationException("IO Error unpacking VNF Package. Contact a Platform ADMIN");
        }
        validateNetAppBlueprint(blueprint);
        try{
            nfvoCatalogueService.onboardVNF(vnfPackage, blueprint.getTestbed());
        }catch (AlreadyExistingEntityException e){
            log.warn("VNF Package already onboarded, IGNORING");
        }  catch (Exception e){
            log.error("Error during VNF Package onboarding:", e);
            throw  new FailedOperationException("Error during onboarding VNF in OSM "+blueprint.getTestbed()+" Contact a Platform ADMIN");
        }


        return onboardNetAppBlueprint(blueprint, file, tempPath);



    }


    private void storeNetAppBlueprint(NetAppBlueprint blueprint, NetAppPackageInfo info, String tempPath) throws MalformattedElementException, FailedOperationException {
        NetAppBlueprint target = new NetAppBlueprint(
                info,
                blueprint.getName(),
                blueprint.getDescription(),
                blueprint.getVersion(),
                blueprint.getVnfPackagePath(),
                blueprint.getApplicationMetrics(),
                blueprint.getType(),
                blueprint.getSpecLevel(),
                blueprint.getAccessLevel(),
                blueprint.getConfigurableParameters(),
                blueprint.getServiceParameters(),
                blueprint.getRequiredEquipments(),
                blueprint.getUseCase(),
                blueprint.getTestbed(),
                blueprint.getRequired5GCoreServices());

        netAppBlueprintRepository.saveAndFlush(target);
        log.debug("Storing Netapp infrastructure metrics");
        for(InfrastructureMetric im: blueprint.getInfrastructureMetrics()){
            InfrastructureMetric nIm = new InfrastructureMetric(target, null, im.getMetricId(), im.getName(), im.getMetricCollectionType(), im.getUnit(),
                    im.getInterval(), im.getType(), im.getMetricGraphType(), im.getComponentIds(), im.getVnfReference());
            infrastructureMetricRepository.saveAndFlush(nIm);
        }
        log.debug("Storing Netapp components");
        for(NetAppComponent am :  blueprint.getAtomicComponents()){
            NetAppComponent nAm = new NetAppComponent(target, am.getComponentId(), am.getMinInstances(), am.getMaxInstances(), am.getEndPointsIds(), am.getPlacement());
            netAppComponentRepository.saveAndFlush(nAm);
        }

        log.debug("Storing Netapp connectivity services");
        for(ConnectivityService cs: blueprint.getConnectivityServices()){
            ConnectivityService nCs = new ConnectivityService(target, null, cs.getConnectivityServiceId(), cs.getEndPointIds(),
                    cs.isExternal(), cs.getConnectivityProperties(), cs.getName(), cs.isManagement());
            netAppConnectivityServiceRepository.saveAndFlush(nCs);
        }

        log.debug("Storing Netapp endpoints");
        for(NetAppEndpoint ep: blueprint.getEndPoints()){

            NetAppEndpoint nEp = new NetAppEndpoint(target, ep.getEndPointId(), ep.getType(), ep.isManagement(), ep.isMobileConnection(),
                    null, ep.getCoverageArea(), ep.getRadioAccessTechnology());
            netAppEndpointRepository.saveAndFlush(nEp);
            for(SliceProfile sliceProfile: ep.getSliceProfile()){
                log.debug("Storing endpoint slice profile");
                sliceProfile.setNetAppEndpoint(nEp);
                sliceProfileRepository.saveAndFlush(sliceProfile);
            }
        }
        log.debug("Storing Netapp provided interfaces specs");
        for(ProvidedInterfaceServiceSpec pi : blueprint.getProvidedInterfaceSpec()){
            ProvidedInterfaceServiceSpec nPi = new ProvidedInterfaceServiceSpec(target, pi.getInterfaceServiceSpecId(), pi.getName(),
                    pi.getVersion(), pi.getProtocol(), pi.getCommType(), pi.getDataFormat(), pi.getAttachedSpecifications(), pi.getRole(),
                    pi.getEndpointIds(), pi.getProtocolParams());
            providedInterfaceSpecRepository.saveAndFlush(nPi);
        }

        log.debug("Storing Netapp required interfaces specs");
        for(RequiredInterfaceServiceSpec pi : blueprint.getRequiredInterfaceSpec()){
            RequiredInterfaceServiceSpec nPi = new RequiredInterfaceServiceSpec(target, pi.getInterfaceServiceSpecId(), pi.getName(),
                    pi.getVersion(), pi.getProtocol(), pi.getCommType(), pi.getDataFormat(), pi.getAttachedSpecifications(), pi.getRole(),
                    pi.getEndpointIds(), pi.getProtocolParams());
            requiredInterfaceSpecRepository.saveAndFlush(nPi);
        }

        log.debug("Storing Netapp test cases");
        log.debug("Storing NetApp software licenses");
        for(SoftwareLicense license:blueprint.getSoftwareLicenses()){
            if(license.getLicenseFile()!=null){
                File licenseFile = new File(tempPath+File.separator+license.getLicenseFile());
                if(!licenseFile.exists()){
                    log.debug("License file not found. Deleting info package");
                    netAppPackageInfoRepository.delete(info);
                    throw new MalformattedElementException("Could not find license file:"+license.getLicenseFile());
                }
            }
            SoftwareLicense nSl = new SoftwareLicense(target, license.getSoftwareLicenseId(), license.isOpenLicense(), license.getLicenseFile(), license.getValidationURL(),
                    license.getType(), license.getComponentIds(), license.getLicenseParams());
            if(enableLicenseMgr){
                log.debug("Creating licenses");

                licenseManagerService.createLicense(info.getNetAppPackageId(), nSl);


            }
            softwareLicenseRepo.saveAndFlush(nSl);


        }
        info.setNetAppBlueprint(target);
        netAppPackageInfoRepository.saveAndFlush(info);
        log.debug("Successfully stored NetApp blueprint");

    }


    private String unpackZipFile(File file) throws IOException, FailedOperationException {
        Path path = Files.createTempDirectory("netapp-");
        log.debug("Generated temp directory:"+path);
        File destDir = new File(path.toString());
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(file));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new FailedOperationException("Failed to create directory " + newFile);
                }
            } else {

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();

            }
            zipEntry = zis.getNextEntry();

        }
        zis.closeEntry();
        zis.close();
        return path.toString();
    }
    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }
        File parent = destFile.getParentFile();
        if (!parent.isDirectory() && !parent.mkdirs()) {
            throw new IOException("failed to create directory " + parent);
        }

        return destFile;
    }

    private boolean authorizeNetAppAccess(UUID netAppPackageId){
        String username = securityService.getUsername();

        Optional<NetAppPackageInfo>  info = netAppPackageInfoRepository.findByNetAppPackageId(netAppPackageId);
        String netAppUseCase = info.get().getNetAppBlueprint().getUseCase();
        if(securityService.userIsPlatformAdmin()){
            log.debug("User is platform admin. Access granted");
            return true;
        }else if(info.get().getOwner()!=null && info.get().getOwner().equals(username)){
            log.debug("User is owner. Access granted");
            return true;
        }else if(info.get().getNetAppBlueprint().getAccessLevel().equals(AccessLevel.PUBLIC)){
            log.debug("NetApp is public. Access granted");
            return true;
        }else if(info.get().getNetAppBlueprint().getAccessLevel().equals(AccessLevel.RESTRICTED) &&
                securityService.userInUseCase(netAppUseCase)){
            log.debug("NetApp is Restricted, user in Use Case. Access granted");
            return true;
        }else return false;
    }

    @Override
    public NetAppBlueprint getNetAppBlueprint(UUID netAppPackageId) throws UnAuthorizedRequestException, NotExistingEntityException {
        log.debug("Received request to retrieve NetApp Blueprint for NetApp package with id:"+netAppPackageId);
        Optional<NetAppPackageInfo>  info = netAppPackageInfoRepository.findByNetAppPackageId(netAppPackageId);
        if(info.isPresent()){
            log.debug("NetApp Package successfully retrieved");

            if(authorizeNetAppAccess(netAppPackageId)){
                return info.get().getNetAppBlueprint();
            }else throw  new UnAuthorizedRequestException("Unauthorized access to NetApp Blueprint");

        }else throw new NotExistingEntityException("NetApp Package with ID:"+netAppPackageId+" NOT found");

    }

    @Override
    public File getNetAppPackage(UUID netAppPackageId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
        log.debug("Received request to retrieve a NetApp package:"+netAppPackageId);
        if(netAppPackageInfoRepository.findByNetAppPackageId(netAppPackageId).isPresent()){
            if(!authorizeNetAppAccess(netAppPackageId))
                throw  new UnAuthorizedRequestException("Unauthorized access to NetApp Package");
            if(storageType.equals("FILE")){
                return new File(netAppFolder+"/"+netAppPackageId.toString()+".zip");
            }else if(storageType.equals("MINIO")){

                try {
                    File file = new File (Files.createTempFile("netapp-"+netAppPackageId, ".zip").toString());
                    InputStream inputStream = minioService.get(Paths.get(netAppPackageId+File.separator+"package.zip"));
                    try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
                        int read;
                        byte[] bytes = new byte[8192];
                        while ((read = inputStream.read(bytes)) != -1) {
                            outputStream.write(bytes, 0, read);
                        }
                    }
                    return file;
                } catch (IOException | MinioException e) {
                    log.error("Error while retrieving NEtApp Package:", e);
                    throw  new FailedOperationException(e);
                }

            } throw  new FailedOperationException("UNKNOWN storage backend");

        }else throw new NotExistingEntityException("NetApp Package not found");

    }

    @Override
    public List<NetAppPackageInfo> queryNetAppPackages(Testbed testbed, AccessLevel accessLevel, NetAppSpecLevel specLevel) {
        log.debug("Received request to query NetApp packages");
        List<NetAppBlueprint> blueprints = new ArrayList<>();
        if(testbed!=null){
            log.debug("Retrieving NetApp packages for testbed:"+testbed);
            blueprints =  netAppBlueprintRepository.findByTestbed(testbed);

        }else if(accessLevel!=null){
            log.debug("Retrieving NetApp packages with access level:"+accessLevel);
            blueprints =  netAppBlueprintRepository.findByAccessLevel(accessLevel);
        }else if(specLevel!=null){
            log.debug("Retrieving NetApp packages with spec level:"+specLevel);
            blueprints =  netAppBlueprintRepository.findBySpecLevel(specLevel);
        }else{
            log.warn("No query parameters specified");
        }
        List<NetAppPackageInfo> result = blueprints.stream().map(bp->bp.getNetAppPackageInfo()).collect(Collectors.toList());
        for(NetAppPackageInfo info: result){
            log.debug("Found match:"+info.getNetAppPackageId()+" "+info.getName()+" "+info.getVersion());
        }
        return result;
    }

    @Override
    public List<NetAppPackageInfo> getAllNetAppPackages() {
        log.debug("Received request to retrieve NetApp Packages");
        List<NetAppPackageInfo> netAppPackageInfos = netAppPackageInfoRepository.findAll();
        return netAppPackageInfos.stream().filter(info -> authorizeNetAppAccess(info.getNetAppPackageId()))
                .collect(Collectors.toList());
    }

    public void addVsbReference(UUID netAppPackageId, UUID vsbId) throws NotExistingEntityException{
        log.debug("Adding reference to VSB: "+vsbId+" to NetApp: "+netAppPackageId);
        Optional<NetAppPackageInfo> info = netAppPackageInfoRepository.findByNetAppPackageId(netAppPackageId);
        if(info.isPresent()){
            NetAppPackageInfo rInfo = info.get();
            rInfo.addReferencedVsb(vsbId);
            netAppPackageInfoRepository.saveAndFlush(rInfo);
        }else throw new NotExistingEntityException("NetApp package NOT FOUND. ID: "+netAppPackageId);
    }
    public void removeVsbReference(UUID netAppPackageId, UUID vsbId) throws NotExistingEntityException{
        log.debug("Adding reference to VSB: "+vsbId+" to NetApp: "+netAppPackageId);
        Optional<NetAppPackageInfo> info = netAppPackageInfoRepository.findByNetAppPackageId(netAppPackageId);
        if(info.isPresent()){
            NetAppPackageInfo rInfo = info.get();
            rInfo.removeReferencedVsb(vsbId);
            netAppPackageInfoRepository.saveAndFlush(rInfo);
        }else throw new NotExistingEntityException("NetApp package NOT FOUND. ID: "+netAppPackageId);
    }

    @Override
    public void deleteNetAppPackage(UUID netAppPackageId, boolean forced) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
        log.debug("Received request to delete NetApp Package with ID:"+netAppPackageId);
        Optional<NetAppPackageInfo> info = netAppPackageInfoRepository.findByNetAppPackageId(netAppPackageId);
        if(info.isPresent()){
            String username = securityService.getUsername();
            if(info.get().getOwner()!=null&&!info.get().getOwner().equals(username) && !securityService.userIsPlatformAdmin()){
                throw new UnAuthorizedRequestException("Only the NetApp owner or the platform admin may delete a NetApp");
            }
            if(!forced && !info.get().getReferencedVsbs().isEmpty()){
                throw new FailedOperationException(("There are some VSBs associated to the NetApp:" +info.get().getReferencedVsbs()+". Try to force delete"));
            }
            List<NetAppEndpoint> endpoints = info.get().getNetAppBlueprint().getEndPoints();
            log.debug("Deleting slice profiles");
            for(NetAppEndpoint endpoint: endpoints){
                for(SliceProfile sp: endpoint.getSliceProfile()){
                    sliceProfileRepository.delete(sp);
                }
                netAppEndpointRepository.delete(endpoint);
            }

            List<ConnectivityService> cSs = info.get().getNetAppBlueprint().getConnectivityServices();
            for(ConnectivityService cs : cSs){
                netAppConnectivityServiceRepository.delete(cs);
            }

            if(storageType.equals("FILE")){
                deleteDir(new File(netAppFolder+File.separator+info.get().getNetAppPackageId()));
            }else if(storageType.equals("MINIO")){
                try {
                    String packageId = info.get().getNetAppPackageId().toString();
                    List<SoftwareLicense> licenses = info.get().getNetAppBlueprint().getSoftwareLicenses();
                    log.debug("Deleting license files");
                    for(SoftwareLicense license : licenses){
                        log.debug("Deleting license for: "+ license.getSoftwareLicenseId());
                        if(license.getLicenseFile()!=null&&!license.getLicenseFile().equals("")){
                            Path lPath = Paths.get(packageId+File.separator+"license"+File.separator+license.getSoftwareLicenseId());
                            log.debug("Deleting license file: "+lPath);
                            minioService.remove(lPath);
                        }
                    }
                    Path ldPath= Paths.get(packageId+File.separator+"license");
                    log.debug("Deleting license folder:"+ldPath);
                    minioService.remove(ldPath);
                    Path packagePath= Paths.get(packageId+File.separator+"package.zip");
                    log.debug("Deleting NetApp package file:"+packagePath);
                    minioService.remove(packagePath);
                    Path folder = Paths.get(info.get().getNetAppPackageId().toString());
                    log.debug("Removing NetApp folder from MINIO:"+folder.toString());
                    minioService.remove(folder);
                } catch (MinioException e) {
                    log.error("Failed to remove NEtApp Package from MINIO", e);
                    throw new FailedOperationException("Could not remove NetApp package from storage backend.");
                }
            }else log.warn("UNSUPPORTED storage type:"+storageType);
            netAppPackageInfoRepository.delete(info.get());
            log.debug("Successfully deleted NetApp Package");
        }else throw new NotExistingEntityException("NetApp package NOT FOUND. ID: "+netAppPackageId);
    }

    @Override
    public File getNetAppPackageSoftwareDoc(UUID netAppPackageId, String softwareDocId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
        return null;
    }


    private void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (! Files.isSymbolicLink(f.toPath())) {
                    deleteDir(f);
                }
            }
        }
        file.delete();
    }
}
