package it.nextworks.catalogue.services;

import it.nextworks.catalogue.repos.TranslationRuleRepository;
import it.nextworks.catalogue.repos.VsDescriptorRepository;
import it.nextworks.catalogue.elements.VsDescriptor;
import it.nextworks.catalogue.elements.VsdNsdTranslationRule;
import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.interfaces.TranslatorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class TranslatorService  implements TranslatorInterface {

    private static final Logger log = LoggerFactory.getLogger(TranslatorService.class);

    @Autowired
    private TranslationRuleRepository translationRuleRepository;
    @Autowired
    private VsDescriptorRepository vsDescriptorRepository;

    @Override
    public NfvNsInstantiationInfo translateVsd(UUID vsdId) throws NotExistingEntityException, FailedOperationException {
        log.debug("VSD->NSD translation at basic translator.");


        Optional<VsDescriptor> optVsd = vsDescriptorRepository.findByVsDescriptorId(vsdId);
        if(!optVsd.isPresent()) throw new NotExistingEntityException("Could not find VSD with ID:" +vsdId);

        VsDescriptor vsd = optVsd.get();
        VsdNsdTranslationRule rule = findMatchingTranslationRule(vsd);
        NfvNsInstantiationInfo info = new NfvNsInstantiationInfo(
                rule.getNstId(),
                rule.getNsdId(),
                rule.getNsdVersion(),
                rule.getNsFlavourId(),
                rule.getNsInstantiationLevelId());

        log.debug("Added NS instantiation info for VSD " + vsdId + " - NST ID: " + rule.getNstId() + " - NSD ID: " + rule.getNsdId() + " - NSD version: " + rule.getNsdVersion() + " - DF ID: "
                + rule.getNsFlavourId() + " - IL ID: " + rule.getNsInstantiationLevelId());

        return info;

    }

    private VsdNsdTranslationRule findMatchingTranslationRule(VsDescriptor vsd) throws FailedOperationException, NotExistingEntityException {
        UUID vsbId = vsd.getVsBlueprintId();
        Map<String, String> vsdParameters = vsd.getQosParameters();
        VsdNsdTranslationRule matchingRule = findMatchingTranslationRule(vsbId, vsdParameters);
        if(matchingRule!=null)
            return matchingRule;
        else throw new FailedOperationException("Impossible to find a translation rule matching the given descriptor parameters");

    }

    private VsdNsdTranslationRule findMatchingTranslationRule(UUID blueprintId, Map<String, String> descriptorParameters) throws FailedOperationException, NotExistingEntityException {
        if ((blueprintId == null) || (descriptorParameters.isEmpty())) throw new NotExistingEntityException("Impossible to translate descriptor into NSD because of missing parameters");
        List<VsdNsdTranslationRule> rules = translationRuleRepository.findByBlueprintId(blueprintId);
        VsdNsdTranslationRule defaultRule = null;
        for (VsdNsdTranslationRule rule : rules) {
            if( rule.isDefault())
                defaultRule=rule;
            if (rule.matchesVsdParameters(descriptorParameters)) {
                log.debug("Found translation rule");
                return rule;
            }
        }
        log.debug("Impossible to find a translation rule matching the given descriptor parameters, using default rule");

        return defaultRule;

    }

}
