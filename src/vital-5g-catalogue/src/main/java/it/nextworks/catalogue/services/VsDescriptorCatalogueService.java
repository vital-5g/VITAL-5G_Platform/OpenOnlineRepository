/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.services;

import java.util.*;
import java.util.stream.Collectors;


import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.VsDescriptorInfo;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.repos.VsDescriptorInfoRepository;
import it.nextworks.catalogue.repos.VsDescriptorRepository;
import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.elements.VsDescriptor;
import it.nextworks.catalogue.interfaces.VsDescriptorCatalogueInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class VsDescriptorCatalogueService implements VsDescriptorCatalogueInterface {

	private static final Logger log = LoggerFactory.getLogger(VsDescriptorCatalogueService.class);


	@Autowired
	private SecurityService securityService;

	@Autowired
	private VsDescriptorRepository vsDescriptorRepository;


	@Autowired
	private VsDescriptorInfoRepository vsDescriptorInfoRepository;


	@Autowired
	private VsBlueprintCatalogueService vsBlueprintCatalogueService;
	

	public VsDescriptorCatalogueService() { }
	
	@Override
	public synchronized UUID onBoardVsDescriptor(VsDescriptor request)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, UnAuthorizedRequestException {
		log.debug("Processing request to on-board a new VS descriptor");
		request.isValid();
		VerticalServiceBlueprint vsBlueprint = null;
		try {
			 vsBlueprint= vsBlueprintCatalogueService.getVerticalServiceBlueprint(request.getVsBlueprintId());
		} catch (NotExistingEntityException e) {
			log.error("Error during VSB retrieval during VSD onboarding", e);
			throw new MalformattedElementException("Could not find VSB with ID:"+ request.getVsBlueprintId());
		}
		if(vsDescriptorInfoRepository.findByNameAndVersion(request.getName(), request.getVersion()).isPresent())
			throw new AlreadyExistingEntityException("VSD with name and version already onboarded");
		String username = securityService.getUsername();
		VsDescriptorInfo info = new VsDescriptorInfo(request.getName(), request.getVersion(), username);
		vsDescriptorInfoRepository.saveAndFlush(info);
		VsDescriptor vsd = new VsDescriptor(
				info,
				request.getName(),
				request.getVersion(),
				request.getVsBlueprintId(),
				request.getQosParameters(),
				request.getAccessLevel(),
				request.getTenantId()
				);
		UUID vsdId = storeVsd(vsd);
		try {
			vsBlueprintCatalogueService.addVsdInBlueprint(vsd.getVsBlueprintId(), vsdId);
		} catch (NotExistingEntityException e) {
			throw new FailedOperationException(e.getMessage());
		}
		return vsdId;
	}

	private boolean authorizeVSDAccess(UUID vsdId){
		String username = securityService.getUsername();
		log.debug("Authorizing access to: "+username+" to VSD: "+vsdId);
		String useCase = null;
		VsDescriptorInfo info = vsDescriptorInfoRepository.findByVsDescriptorId(vsdId).get();
		if(securityService.userIsPlatformAdmin()){
			log.debug("User is platform admin. Access granted");
			return true;
		}else if(info.getOwner()!=null&& info.getOwner().equals(username)){
			log.debug("User is owner. Access granted");
			return true;
		}else if(info.getVsDescriptor().getAccessLevel().equals(AccessLevel.PUBLIC)){
			log.debug("VSD is public. Access granted");
			return true;
		}else if(info.getVsDescriptor().getAccessLevel().equals(AccessLevel.RESTRICTED) &&
				securityService.userInUseCase(useCase)){
			log.debug("VSD is Restricted, user in Use Case. Access granted");
			return true;
		}else return false;
	}


	@Override
	public List<VsDescriptor> queryVsDescriptor()
			throws MalformattedElementException,  FailedOperationException {
		log.debug("Processing a query for a VS descriptor");
		//request.isValid();

		return vsDescriptorRepository.findAll().stream().filter(vsd -> authorizeVSDAccess(vsd.getVsDescriptorId())).collect(Collectors.toList());
	}
	
	@Override
	public synchronized void deleteVsDescriptor(UUID vsDescriptorId, boolean force)
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {
		log.debug("Processing request to delete a VS descriptor");
		if  (vsDescriptorId == null) throw new MalformattedElementException("VSD ID not provided");
		
		Optional<VsDescriptor> vsdOpt = vsDescriptorRepository.findByVsDescriptorId(vsDescriptorId);
		if (vsdOpt.isPresent()) {
			String username = securityService.getUsername();
			VsDescriptorInfo info = vsDescriptorInfoRepository.findByVsDescriptorId(vsDescriptorId).get();
			if(info.getOwner()!=null && !info.getOwner().equals(username) && !securityService.userIsPlatformAdmin()){
				throw  new UnAuthorizedRequestException("Only VSD owner and Platform admins can delete a VSD");
			}
			if(!force && !info.getActiveVsiId().isEmpty()){
				throw new FailedOperationException("VSD has active vertical service instances. Try force delete");
			}
			VsDescriptor vsd = vsdOpt.get();

			UUID vsbId = vsd.getVsBlueprintId();
			vsDescriptorRepository.delete(vsd);
			vsBlueprintCatalogueService.removeVsdInBlueprint(vsbId, vsDescriptorId);
			log.debug("VSD " + vsDescriptorId + " removed from the internal DB.");

		} else {
			log.error("VSD " + vsDescriptorId + " not found");
			throw new NotExistingEntityException("VSD " + vsDescriptorId + " not found");
		}
	}

	@Override
	public void useVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException {
		log.debug("Processing request to use a VS descriptor");
		if  (vsdId == null) throw new MalformattedElementException("VSD ID is null");

		if  (vsiId == null) throw new MalformattedElementException("VSI ID is null");
		Optional<VsDescriptorInfo> vsdOpt = vsDescriptorInfoRepository.findByVsDescriptorId(vsdId);
		if (vsdOpt.isPresent()) {
			vsdOpt.get().addVsi(vsiId);
			vsDescriptorInfoRepository.saveAndFlush(vsdOpt.get());

		} else {
			log.warn("VSD " + vsdId + " not found");
			throw new NotExistingEntityException("VSD " + vsdId + " not found");
		}

	}

	@Override
	public void releaseVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException {
		log.debug("Processing request to release a VS descriptor");
		if  (vsdId == null) throw new MalformattedElementException("VSD ID is null");

		if  (vsiId == null) throw new MalformattedElementException("VSI ID is null");
		Optional<VsDescriptorInfo> vsdOpt = vsDescriptorInfoRepository.findByVsDescriptorId(vsdId);
		if (vsdOpt.isPresent()) {
			vsdOpt.get().removeVsi(vsiId);
			vsDescriptorInfoRepository.saveAndFlush(vsdOpt.get());

		} else {
			log.warn("VSD " + vsdId + " not found");
			throw new NotExistingEntityException("VSD " + vsdId + " not found");
		}

	}

	public VsDescriptor getVsd(UUID vsdId) throws NotExistingEntityException, UnAuthorizedRequestException {
		log.debug("Internal request to retrieve VSD with ID " + vsdId);
		Optional<VsDescriptor> vsdOpt = vsDescriptorRepository.findByVsDescriptorId(vsdId);
		if (vsdOpt.isPresent()){
			if(authorizeVSDAccess(vsdId)){
				return vsdOpt.get();
			}else throw new UnAuthorizedRequestException("Unauthorized access to VSD: "+vsdId);
		}
		else throw new NotExistingEntityException("VSD with ID " + vsdId + " not found");
	}
	
	private UUID storeVsd(VsDescriptor vsd) throws AlreadyExistingEntityException, FailedOperationException {
		log.debug("On boarding VSD with name " + vsd.getName() + " and version " + vsd.getVersion() + " for tenant " + vsd.getTenantId());
		if (vsDescriptorRepository.findByNameAndVersionAndTenantId(vsd.getName(), vsd.getVersion(), vsd.getTenantId()).isPresent()) {
			log.error("The VSD is already present");
			throw new AlreadyExistingEntityException("VSD with name " + vsd.getName() + " and version " + vsd.getVersion() + " for tenant " + vsd.getTenantId() + " already present");
		}
		vsDescriptorRepository.saveAndFlush(vsd);

		log.debug("Added VS Descriptor with ID " + vsd.getVsDescriptorId());

		return vsd.getVsDescriptorId();
	}




}
