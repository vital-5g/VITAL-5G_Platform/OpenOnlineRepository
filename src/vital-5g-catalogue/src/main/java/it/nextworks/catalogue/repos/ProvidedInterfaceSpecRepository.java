package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.ProvidedInterfaceServiceSpec;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvidedInterfaceSpecRepository extends JpaRepository<ProvidedInterfaceServiceSpec, Long> {
}
