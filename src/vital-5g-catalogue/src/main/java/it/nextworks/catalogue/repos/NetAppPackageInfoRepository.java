package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.NetAppPackageInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface NetAppPackageInfoRepository extends JpaRepository<NetAppPackageInfo, UUID> {

    Optional<NetAppPackageInfo> findByNetAppPackageId(UUID id);
    Optional<NetAppPackageInfo> findByNameAndVersion(String name, String version);
}
