package it.nextworks.catalogue.sbi.netappvalidator.interfaces;

import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.exceptions.FailedOperationException;

public interface NetAppValidatorInterface {

    NetAppValidationResponse validateNetAppBlueprint(NetAppBlueprint blueprint) throws FailedOperationException;

}
