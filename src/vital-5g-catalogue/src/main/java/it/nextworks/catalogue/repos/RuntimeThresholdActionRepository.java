package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.RuntimeAction;
import it.nextworks.catalogue.elements.RuntimeThresholdAction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuntimeThresholdActionRepository extends JpaRepository<RuntimeThresholdAction, Long> {
}
