package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TestCaseBlueprintRepository extends JpaRepository<TestCaseBlueprint, Long> {

    Optional<TestCaseBlueprint> findByTestCaseBlueprintId(UUID testCaseBlueprintId);
}
