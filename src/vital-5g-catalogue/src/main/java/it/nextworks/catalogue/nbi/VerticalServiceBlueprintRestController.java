package it.nextworks.catalogue.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.services.VsBlueprintCatalogueService;
import it.nextworks.catalogue.messages.OnboardVSBRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

@Api(tags = "NetApp Package Catalogue API")
@RestController
@CrossOrigin
@RequestMapping("/portal/catalogue/vsbs")

public class VerticalServiceBlueprintRestController {

    private static final Logger log = LoggerFactory.getLogger(VerticalServiceBlueprintRestController.class);

    @Autowired
    private VsBlueprintCatalogueService vsbCatalogueService;

    @ApiOperation(value = "Onboard Vertical Service Bleuprint")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Vertical Service Blueprint onboarded", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"","/"}, method = RequestMethod.POST)
    public ResponseEntity<?> onboardVerticalServiceBlueprint(@RequestParam("vsb") OnboardVSBRequest request, @RequestParam("nsd") MultipartFile nsd) {
        log.debug("Received request to Onboard a Vertical Service Blueprint  .");
        try {
            String fileExtension = nsd.getOriginalFilename().split("\\.")[1];

            Path pTempFile = Files.createTempFile("nsd-", "."+fileExtension);

            File lFile = new File(pTempFile.toString());

            OutputStream os = new FileOutputStream(lFile);
            os.write(nsd.getBytes());
            UUID verticalServiceBlueprint    = vsbCatalogueService.onboardVsBlueprint(request, lFile);
            return new ResponseEntity<UUID>(verticalServiceBlueprint, HttpStatus.CREATED);
        } catch (MalformattedElementException e) {
            log.error("Malformatted request"+e.getMessage(), e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (AlreadyExistingEntityException e) {
            log.error("NetApp package already existing");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get a Vertical Service Blueprint")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve Vertical Service Blueprint given its ID.", response = it.nextworks.catalogue.elements.VerticalServiceBlueprint.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{vsBlueprintId}", method = RequestMethod.GET)
    public ResponseEntity<?> getVsBlueprint(@PathVariable UUID vsBlueprintId) {
        log.debug("Received request to retrieve a NetApp blueprint.");
        try {

            it.nextworks.catalogue.elements.VerticalServiceBlueprint bp = vsbCatalogueService.getVerticalServiceBlueprint(vsBlueprintId);
            return new ResponseEntity<VerticalServiceBlueprint>(bp, HttpStatus.OK);
        }catch (NotExistingEntityException e ) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve All Vertical Service Blueprints.", response = it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })

    @RequestMapping(value = {"","/"}, method = RequestMethod.GET)
    public ResponseEntity<?> getAllVerticalServiceBlueprints() {
        log.debug("Received request to retrieve all Vertical Service Blueprints");
        try {

            List<it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo> infos = vsbCatalogueService.getAllVerticalServiceBlueprints();
            return new ResponseEntity<>(infos, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }




    @ApiOperation(value = "Query Vertical Service Blueprints")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of Vertical Service Blueprints.", response = it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/blueprint", method = RequestMethod.GET)
    public ResponseEntity<?> queryVerticalServiceBlueprint(@RequestParam(required = false) it.nextworks.catalogue.elements.Testbed testbed, @RequestParam(required = false) it.nextworks.catalogue.elements.AccessLevel accessLevel) {
        log.debug("Received request to query Vertical Service Blueprint");
        try {

            List<it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo> infos = vsbCatalogueService.queryVerticalServiceBlueprint(testbed, accessLevel);
            return new ResponseEntity<>(infos, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Delete Vertical Service Blueprint")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Deleted an existing Vertical Service Blueprint"),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "The request contains conflict elements", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{vsBlueprintId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteNetAppPackage(@PathVariable UUID vsBlueprintId, @RequestParam(required = false) boolean forced) {
        log.debug("Received request to delete a Vertical Service Blueprint:"+vsBlueprintId);
        try {

            vsbCatalogueService.deleteVsBlueprint(vsBlueprintId, forced);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch ( UnAuthorizedRequestException e) {
            log.warn("Unauthorized request", e);

            return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }catch (FailedOperationException e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
        }catch (NotExistingEntityException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }




}
