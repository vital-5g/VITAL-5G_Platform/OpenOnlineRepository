/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.services;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.repos.*;
import it.nextworks.catalogue.interfaces.VsBlueprintCatalogueInterface;
import it.nextworks.catalogue.messages.OnboardVSBRequest;

import it.nextworks.catalogue.sbi.nfvo.interfaces.NfvoCatalogueInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class VsBlueprintCatalogueService implements VsBlueprintCatalogueInterface {

	private static final Logger log = LoggerFactory.getLogger(VsBlueprintCatalogueService.class);
	
	@Autowired
	private VsBlueprintRepository vsBlueprintRepository;

	@Autowired
	private VsBlueprintInfoRepository vsBlueprintInfoRepository;

	@Autowired
	private VsDescriptorCatalogueService vsDescriptorCatalogueService;

	@Autowired
	private InfrastructureMetricRepository infrastructureMetricRepository;


	@Autowired
	private NetAppConnectivityServiceRepository connectivityServiceRepository;

	@Autowired
	private NetAppPackageService netAppPackageService;

	@Autowired
	private NfvoCatalogueInterface nfvoCatalogueInterface;

	@Autowired
	private ServiceComponentRepository serviceComponentRepository;

	@Autowired
	private ServiceEndpointRepository serviceEndpointRepository;


	@Autowired
	private TranslationRuleRepository translationRuleRepository;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private RuntimeActionRepository runtimeActionRepository;
	@Autowired
	private RuntimeThresholdActionRepository runtimeThresholdActionRepository;

	@Autowired
	private RuntimeDiagnosticActionRepository runtimeDiagnosticActionRepository;

	public VsBlueprintCatalogueService() {	}
	
	@Override
	public synchronized UUID onboardVsBlueprint(OnboardVSBRequest request, File nsd) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		log.debug("Processing request to onboard a new VS blueprint");
		request.isValid();

		if(nsd!=null){
			log.debug("Onboarding NSD on "+request.getVsBlueprint().getTestbed());
			try{
				nfvoCatalogueInterface.onboardNSD(nsd, request.getVsBlueprint().getTestbed());
			}catch (AlreadyExistingEntityException e){
				log.warn("Ignoring CONFLICT exception while onboarding NSD");
			}
			
		}
		UUID vsbId = storeVsBlueprint(request);

		storeTranslationRules(request, getVsBlueprintInfo(vsbId));
		return vsbId;
			

	}




	@Override
	public it.nextworks.catalogue.elements.VerticalServiceBlueprint getVerticalServiceBlueprint(UUID vsbId) throws NotExistingEntityException, UnAuthorizedRequestException {
		log.debug("Received request to retrieve Vertical Service Blueprint:"+vsbId);
		Optional<it.nextworks.catalogue.elements.VerticalServiceBlueprint> vsb = vsBlueprintRepository.findByVerticalServiceBlueprintId(vsbId);
		if(!vsb.isPresent()) throw new NotExistingEntityException("VSB with given ID not found");
		if(authorizeVSBAccess(vsbId)){
			return vsb.get();
		}else throw new UnAuthorizedRequestException("Unauthorized access to VS Blueprint");

	}


	@Override
	public List<it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo> queryVerticalServiceBlueprint(it.nextworks.catalogue.elements.Testbed testbed, it.nextworks.catalogue.elements.AccessLevel accessLevel) throws MalformattedElementException {
		log.debug("Processing request to query VS blueprints");



		List<it.nextworks.catalogue.elements.VerticalServiceBlueprint> vsbs = new ArrayList<>();
		if (testbed != null) {
			log.debug("Querying VSB by testbed");
			vsbs = vsBlueprintRepository.findByTestbed(testbed);
		} else if (accessLevel != null){
			log.debug("Querying VSB by access level");

			vsbs = vsBlueprintRepository.findByAccessLevel(accessLevel);
		}else {
            log.error("Received query VS Blueprint with attribute selector. Not supported at the moment.");
            throw new MalformattedElementException("Received query VS Blueprint with attribute selector. Not supported at the moment.");
        }

		return vsbs.stream().map(vsb -> vsb.getVerticalServiceBlueprintInfo()).filter(info->authorizeVSBAccess(info.getVerticalServiceBlueprintId())).collect(Collectors.toList());
	}

	@Override
	public List<it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo> getAllVerticalServiceBlueprints() {
		log.debug("Received request to retrieve all VSBs");
		List<VerticalServiceBlueprintInfo> infos =vsBlueprintInfoRepository.findAll();
		return infos.stream().filter(info -> authorizeVSBAccess(info.getVerticalServiceBlueprintId())).collect(Collectors.toList());
	}


	@Override
	public synchronized void deleteVsBlueprint(UUID vsBlueprintId, boolean force) throws NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {


		log.debug("Processing request to delete a VS blueprint with ID " + vsBlueprintId);

		if (vsBlueprintId == null) throw new FailedOperationException("VSB ID not provided");

		String username = securityService.getUsername();
		it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo vsbi = getVsBlueprintInfo(vsBlueprintId);
		if(!authorizeVSBAccess(vsBlueprintId))
			throw new UnAuthorizedRequestException("Unauthorized access to VSB");
		if(!(vsbi.getActiveVsdId().isEmpty())&& !force){
			log.error("There are some VSDs associated to the VS Blueprint. Try to force delete.");
			throw new FailedOperationException("There are some VSDs associated to the VS Blueprint. Try to force delete");

		}
		if(vsbi.getOwner()!=null && !vsbi.getOwner().equals(username) && !securityService.userIsPlatformAdmin()){
			throw new UnAuthorizedRequestException("Only the VSB owner or the platform admin may delete a VSB");
		}


		it.nextworks.catalogue.elements.VerticalServiceBlueprint vsb = getVsBlueprint(vsBlueprintId);
		log.debug("Removing NetApp references");
		for(ServiceComponent component : vsb.getAtomicComponents()){
			try{
				netAppPackageService.removeVsbReference(component.getNetAppBlueprintId(),vsb.getVerticalServiceBlueprintId() );
			}catch( NotExistingEntityException e){
				log.warn("Skipping NetApp {}", component.getNetAppBlueprintId());
			}

		}
		log.debug("Removing RuntimeActions");
		for(RuntimeAction ra: vsb.getRuntimeActions()){
			if(ra.getTriggerType().equals(RuntimeActionTriggerType.THRESHOLD)){
				log.debug("Removing Threshold Action {}",ra.getId());
				runtimeThresholdActionRepository.deleteById(ra.getId());
				runtimeThresholdActionRepository.flush();
			}else if(ra.getTriggerType().equals(RuntimeActionTriggerType.THRESHOLD)){
				log.debug("Removing Diagnostic Action {}",ra.getId());
				runtimeDiagnosticActionRepository.deleteById(ra.getId());
				runtimeDiagnosticActionRepository.flush();
			}

			runtimeActionRepository.delete(ra);
			runtimeActionRepository.flush();
		}
		vsb.setRuntimeActions(new ArrayList<>());
		vsBlueprintRepository.saveAndFlush(vsb);
		vsBlueprintRepository.delete(vsb);
		log.debug("Removed VSB from DB.");

		vsBlueprintInfoRepository.delete(vsbi);
		log.debug("Removed VSB info from DB.");



	}


	
	public synchronized void addVsdInBlueprint(UUID vsBlueprintId, UUID vsdId)
			throws NotExistingEntityException {
		log.debug("Adding VSD " + vsdId + " to blueprint " + vsBlueprintId);
		it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo vsbi = getVsBlueprintInfo(vsBlueprintId);
		vsbi.addVsd(vsdId);
		vsBlueprintInfoRepository.saveAndFlush(vsbi);
		log.debug("Added VSD " + vsdId + " to blueprint " + vsBlueprintId);
	}
	
	public synchronized void removeVsdInBlueprint(UUID vsBlueprintId, UUID vsdId)
			throws NotExistingEntityException {
		log.debug("Removing VSD " + vsdId + " from blueprint " + vsBlueprintId);
		it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo vsbi = getVsBlueprintInfo(vsBlueprintId);
		vsbi.removeVsd(vsdId);
		vsBlueprintInfoRepository.saveAndFlush(vsbi);
		log.debug("Removed VSD " + vsdId + " from blueprint " + vsBlueprintId);
	}
	
	private UUID storeVsBlueprint(OnboardVSBRequest request) throws AlreadyExistingEntityException, MalformattedElementException {
		it.nextworks.catalogue.elements.VerticalServiceBlueprint vsBlueprint = request.getVsBlueprint();
		log.debug("Onboarding VS blueprint with name " + vsBlueprint.getName() + " and version " + vsBlueprint.getVersion());
		if ( (vsBlueprintInfoRepository.findByNameAndVersion(vsBlueprint.getName(), vsBlueprint.getVersion()).isPresent()) ) {
			log.error("VS Blueprint with name " + vsBlueprint.getName() + " and version " + vsBlueprint.getVersion() + " already present in DB.");
			throw new AlreadyExistingEntityException("VS Blueprint with name " + vsBlueprint.getName() + " and version " + vsBlueprint.getVersion() + " already present in DB.");
		}
		log.debug("Validate NetApp package availability");
		List<String> requiredLicenses = new ArrayList<>();
		for(it.nextworks.catalogue.elements.ServiceComponent sc: vsBlueprint.getAtomicComponents()){
			try{
				NetAppBlueprint netAppBlueprint = netAppPackageService.getNetAppBlueprint(sc.getNetAppBlueprintId());
				if(netAppBlueprint.getTestbed()!=vsBlueprint.getTestbed()){
					throw  new MalformattedElementException("NetApp "+sc.getNetAppBlueprintId()+" not in vertical service testbed "+vsBlueprint.getTestbed());
				}
				//Add required licenses
				for(SoftwareLicense lic: netAppBlueprint.getSoftwareLicenses()){
					if(!lic.isOpenLicense()&&lic.getType().equals(LicenseType.INSTANCE_BASED)){
						requiredLicenses.add(netAppBlueprint.getNetAppPackageId().toString()+"."+lic.getSoftwareLicenseId());
					}
				}
			} catch (NotExistingEntityException | UnAuthorizedRequestException e) {
				throw new MalformattedElementException("NetApp package with ID:"+sc.getNetAppBlueprintId()+" not found or not available");
			}

		}
		log.debug("Creating Vertical Service Blueprint Info resource");
		String username = securityService.getUsername();
		it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo vsBlueprintInfo =
				new it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo(request.getVsBlueprint().getVersion(), request.getVsBlueprint().getName(), username, request.getVsBlueprint().getTestbed());
		vsBlueprintInfoRepository.saveAndFlush(vsBlueprintInfo);

		log.debug("Storing Netapp infrastructure metrics");


		it.nextworks.catalogue.elements.VerticalServiceBlueprint target = new it.nextworks.catalogue.elements.VerticalServiceBlueprint( vsBlueprintInfo, vsBlueprint.getName(), vsBlueprint.getDescription(),
				vsBlueprint.getVersion(), vsBlueprint.getNsdPath(), vsBlueprint.getTestbed(), vsBlueprint.getConfigurableParameters(),
				vsBlueprint.getServiceParameters(), vsBlueprint.getAccessLevel(),
				vsBlueprint.getUseCase(),
				vsBlueprint.getRequiredEquipments(),
				vsBlueprint.getApplicationMetrics(),
				null, //to be filled after
				null, //to be filled after
				null,//connectivity serviceS: to be filled after
				null,
				null //runtime actions
		);
		target.setRequiredLicenses(requiredLicenses);
		vsBlueprintRepository.saveAndFlush(target);

		log.debug("Storing VSB Infrastructure Metrics");
		for(it.nextworks.catalogue.elements.InfrastructureMetric im: vsBlueprint.getInfrastructureMetrics()){
			it.nextworks.catalogue.elements.InfrastructureMetric nIm = new it.nextworks.catalogue.elements.InfrastructureMetric(null, target, im.getMetricId(), im.getName(), im.getMetricCollectionType(), im.getUnit(),
					im.getInterval(), im.getType(), im.getMetricGraphType(), im.getComponentIds(), im.getVnfReference());
			infrastructureMetricRepository.saveAndFlush(nIm);
		}

		log.debug("Storing VSB RuntimeActions");
		List<RuntimeAction>  runtimes = new ArrayList<>();
		for(RuntimeAction ra: vsBlueprint.getRuntimeActions()){
			RuntimeTarget nTaget=null;
			RuntimeAction nRa = null;
			if(ra.getTarget().getRuntimeTargetType().equals(RuntimeTargetType.NS_SCALE)){
				RuntimeNSScaleTarget oRT = (RuntimeNSScaleTarget) ra.getTarget();
				nTaget= new RuntimeNSScaleTarget(oRT.getScalingAspect(), oRT.getVnfdId(), oRT.getCpus(), oRT.getRam());
			}else if(ra.getTarget().getRuntimeTargetType().equals(RuntimeTargetType.SLICE_MODIFICATION)){
				RuntimeSliceModifyTarget oRT = (RuntimeSliceModifyTarget) ra.getTarget();
				nTaget= new RuntimeSliceModifyTarget(oRT.getSliceProfile());
			}
			if(ra.getTriggerType().equals(RuntimeActionTriggerType.THRESHOLD)){
				RuntimeThresholdAction oRa = (RuntimeThresholdAction) ra;
				nRa = new RuntimeThresholdAction(target, ra.getRuntimeActionId(), nTaget, oRa.getMetricId(), oRa.getMetricValue(), oRa.getLogicOperator());

			}else if(ra.getTriggerType().equals(RuntimeActionTriggerType.DIAGNOSTIC_EVENT)){
				RuntimeDiagnosticAction oRa = (RuntimeDiagnosticAction) ra;
				nRa = new RuntimeDiagnosticAction(target, ra.getRuntimeActionId(), nTaget, oRa.getDiagnosticEvent());

			}
			runtimes.add(nRa);
			runtimeActionRepository.saveAndFlush(nRa);
		}

		log.debug("Storing VSB components");
		for(it.nextworks.catalogue.elements.ServiceComponent am :  vsBlueprint.getAtomicComponents()){

			it.nextworks.catalogue.elements.ServiceComponent nAm = new it.nextworks.catalogue.elements.ServiceComponent(target,  am.getServiceComponentId(), am.getNetAppBlueprintId(), am.getServiceEndpointIds());
			serviceComponentRepository.saveAndFlush(nAm);
		}

		log.debug("Storing VSB connectivity services");
		for(it.nextworks.catalogue.elements.ConnectivityService cs: vsBlueprint.getConnectivityServices()){
			it.nextworks.catalogue.elements.ConnectivityService nCs = new it.nextworks.catalogue.elements.ConnectivityService(null, target , cs.getConnectivityServiceId(), cs.getEndPointIds(),
					cs.isExternal(), cs.getConnectivityProperties(), cs.getName(), cs.isManagement());
			connectivityServiceRepository.saveAndFlush(nCs);
		}

		log.debug("Storing VSB Endpoints");
		for(it.nextworks.catalogue.elements.ServiceEndpoint se: vsBlueprint.getEndPoints()){
			it.nextworks.catalogue.elements.ServiceEndpoint nSe =
					new it.nextworks.catalogue.elements.ServiceEndpoint(target,
							se.getSliceProfile() , se.getNetAppBlueprintId(), se.getEndpointId(),
							se.getServiceEndpointId());
			serviceEndpointRepository.saveAndFlush(nSe);
		}

		UUID vsbId = target.getVerticalServiceBlueprintId();
		log.debug("Adding NetApp references");
		for(ServiceComponent component : vsBlueprint.getAtomicComponents()){
			try {
				netAppPackageService.addVsbReference(component.getNetAppBlueprintId(),vsBlueprint.getVerticalServiceBlueprintId() );
			} catch (NotExistingEntityException e) {
				log.error("Error associating NetApp to VSB. This should not happen", e);
			}
		}
		vsBlueprintRepository.saveAndFlush(target);
		log.debug("Added VS Blueprint with ID " + vsbId.toString());
		



		return vsbId;
	}

	private it.nextworks.catalogue.elements.VerticalServiceBlueprint getVsBlueprint(String name, String version) throws NotExistingEntityException {
		if (vsBlueprintRepository.findByNameAndVersion(name, version).isPresent()){

			return vsBlueprintRepository.findByNameAndVersion(name, version).get();
		}
		else throw new NotExistingEntityException("VS Blueprint with name " + name + " and version " + version + " not found in DB.");
	}
	
	private it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo getVsBlueprintInfo(String name, String version) throws NotExistingEntityException {
		it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo vsBlueprintInfo;
		if (vsBlueprintInfoRepository.findByNameAndVersion(name, version).isPresent()) vsBlueprintInfo = vsBlueprintInfoRepository.findByNameAndVersion(name, version).get();
		else throw new NotExistingEntityException("VS Blueprint info for VSB with name " + name + " and version " + version + " not found in DB.");
		it.nextworks.catalogue.elements.VerticalServiceBlueprint vsb = getVsBlueprint(name, version);
		vsBlueprintInfo.setVerticalServiceBlueprint(vsb);
		return vsBlueprintInfo;
	}
	
	private VerticalServiceBlueprint getVsBlueprint(UUID vsbId) throws NotExistingEntityException {
		if (vsBlueprintRepository.findByVerticalServiceBlueprintId(vsbId).isPresent()) return vsBlueprintRepository.findByVerticalServiceBlueprintId(vsbId).get();
		else throw new NotExistingEntityException("VS Blueprint with ID " + vsbId + " not found in DB.");
	}
	
	private it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo getVsBlueprintInfo(UUID vsbId) throws NotExistingEntityException {
		Optional<it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo> vsBlueprintInfo = vsBlueprintInfoRepository.findByVerticalServiceBlueprintId(vsbId);
		if (!vsBlueprintInfo.isPresent()) throw new NotExistingEntityException("VS Blueprint info for VSB with ID " + vsbId + " not found in DB.");

		return vsBlueprintInfo.get();
	}
	






	private void storeTranslationRules (OnboardVSBRequest request, it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo vsBlueprintInfo){

		log.debug("Storing translation rules for:"+vsBlueprintInfo.getVerticalServiceBlueprintId());



		List<it.nextworks.catalogue.elements.VsdNsdTranslationRule> trs = request.getTranslationRules();
		for (it.nextworks.catalogue.elements.VsdNsdTranslationRule tr : trs) {
			tr.setBlueprintId(vsBlueprintInfo.getVerticalServiceBlueprintId());
			translationRuleRepository.saveAndFlush(tr);
		}
		log.debug("Translation rules saved in internal DB.");

	}

	private boolean authorizeVSBAccess(UUID vsBlueprintId){
		String username = securityService.getUsername();

		Optional<VerticalServiceBlueprintInfo>  info = vsBlueprintInfoRepository.findByVerticalServiceBlueprintId(vsBlueprintId);
		//TODO: List of useCases or single element
		//String vsbUseCase = info.get().getVerticalServiceBlueprint().getUseCase();
		String useCase = null;
		if(securityService.userIsPlatformAdmin()){
			log.debug("User is platform admin. Access granted");
			return true;
		}else if(info.get().getOwner()!=null && info.get().getOwner().equals(username)){
			log.debug("User is owner. Access granted");
			return true;
		}else if(info.get().getVerticalServiceBlueprint().getAccessLevel().equals(AccessLevel.PUBLIC)){
			log.debug("VSB is public. Access granted");
			return true;
		}else if(info.get().getVerticalServiceBlueprint().getAccessLevel().equals(AccessLevel.RESTRICTED) &&
				securityService.userInUseCase(useCase)){
			log.debug("VSB is Restricted, user in Use Case. Access granted");
			return true;
		}else return false;
	}
}
