package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.InfrastructureMetric;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfrastructureMetricRepository extends JpaRepository<InfrastructureMetric, Long> {
}
