package it.nextworks.catalogue.services;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.IDToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Map;

@Service
public class SecurityService {

    private static final Logger log = LoggerFactory.getLogger(SecurityService.class);


    @Value("${catalogue.admin-role:Platform_admin}")
    private String adminRole;

    @Value("${catalogue.technician-role:Platform_technician}")
    private String technicianRole;

    @Value("${catalogue.netapp-developer-role:NetApp_Developer}")
    private String netAppDeveloperRole;

    @Value("${catalogue.vertical-service-provider-role:Vertical_Service_Provider}")
    private String vspRole;

    @Value("${catalogue.experimenter-role:Experimenter}")
    private String experimenterRole;

    @Value("${catalogue.testbed-admin-role:Testbed_Admin}")
    private String testbedAdminRole;

    @Value("${catalogue.site-infrastructure-manager-role:T&L_Site Infrastructure_Manager}")
    private String simRole;


    public boolean userInUseCase(String useCase){

        String userUsecase= userUsecase();
        if(userUsecase!=null && useCase!=null){
            return userUsecase.equals(useCase);
        }else return false;

    }

    public String getUsername(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {

            return ((UserDetails)principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    public String getToken(){
        Authentication principal = SecurityContextHolder.getContext().getAuthentication();
        KeycloakAuthenticationToken kp = (KeycloakAuthenticationToken) principal;

        SimpleKeycloakAccount simpleKeycloakAccount = (SimpleKeycloakAccount) kp.getDetails();

        AccessToken token  = simpleKeycloakAccount.getKeycloakSecurityContext().getToken();
        return token.getAccessTokenHash();
    }

    public boolean userIsTestbedAdmin(){
        return userHasRole(testbedAdminRole);
    }

    public boolean userIsPlatformTechnician(){
        return userHasRole(technicianRole);
    }

    public boolean userIsExperimenter(){
        return userHasRole(experimenterRole);
    }

    public boolean userIsVerticalServiceProvider(){
        return userHasRole(vspRole);
    }

    public boolean userIsNetAppDeveloper(){
        return userHasRole(netAppDeveloperRole);
    }

    public boolean userIsPlatformAdmin(){
        return userHasRole(adminRole);
    }

    public String userUsecase(){
        Principal principal = (Principal)  SecurityContextHolder.getContext().getAuthentication().getPrincipal();


        if (principal instanceof KeycloakPrincipal) {
            KeycloakPrincipal kPrincipal = (KeycloakPrincipal) principal;
            //IDToken token = kPrincipal.getKeycloakSecurityContext().getIdToken();
            Object  useCaseObj = kPrincipal.getKeycloakSecurityContext().getToken().getOtherClaims().get("use_case");
            if(useCaseObj!=null){
                return  useCaseObj.toString();
            }else return null;

            



        }else return null;
    }

    public boolean userHasRole(String role){
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(role));
    }
}
