package it.nextworks.catalogue.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.services.NetAppPackageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

@Api(tags = "NetApp Package Catalogue API")
@RestController
@CrossOrigin
@RequestMapping("/portal/catalogue/netapppackages")

public class NetAppPackageRestController {

    private static final Logger log = LoggerFactory.getLogger(NetAppPackageRestController.class);

    @Autowired
    private NetAppPackageService netAppPackageService;

    @ApiOperation(value = "Onboard NetApp Package")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Onboarded a new NetApp package its ID.", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "The request contains conflict elements", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"","/"}, method = RequestMethod.POST)
    public ResponseEntity<?> onboardNetAppPackage(@RequestParam("file")MultipartFile file, @RequestParam(value = "vnfPackage", required = false) MultipartFile vnfPackage ) {
        log.debug("Received request to Onboard a NetApp Package .");
        try {
            Path pTempFile = Files.createTempFile("netapp-", ".zip");

            File lFile = new File(pTempFile.toString());

            OutputStream os = new FileOutputStream(lFile);
            os.write(file.getBytes());
            if(vnfPackage!=null){
                log.debug("Onboarding NetApp Package and VNF");
                Path pTempFileVnf = Files.createTempFile("vnfp-", ".tar.gz");

                File lFileVnf = new File(pTempFileVnf.toString());

                OutputStream osVnf = new FileOutputStream(lFileVnf);
                osVnf.write(vnfPackage.getBytes());
                UUID netAppPackageId = netAppPackageService.onboardNetAppPackage(lFile, lFileVnf);
                return new ResponseEntity<UUID>(netAppPackageId, HttpStatus.CREATED);
            }else{
                log.debug("Onboarding NetApp Package without VNF");
                UUID netAppPackageId = netAppPackageService.onboardNetAppPackage(lFile);
                return new ResponseEntity<UUID>(netAppPackageId, HttpStatus.CREATED);
            }

        } catch (MalformattedElementException e) {
            log.error("Malformatted request"+e.getMessage(), e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (AlreadyExistingEntityException e) {
            log.error("NetApp package already existing");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
        }catch ( UnAuthorizedRequestException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (FailedOperationException e) {
            log.error("Failed operation exception", e);
            return new ResponseEntity<String>(e.getMessage()+". Please contact a Platform ADMIN.",
                    HttpStatus.INTERNAL_SERVER_ERROR);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get a NetApp Blueprint")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve NetApp blueprint of the NetApp package its ID.", response = it.nextworks.catalogue.elements.NetAppBlueprint.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{netAppPackageId}/blueprint", method = RequestMethod.GET)
    public ResponseEntity<?> getNetAppBlueprint(@PathVariable UUID netAppPackageId) {
        log.debug("Received request to retrieve a NetApp blueprint.");
        try {

            it.nextworks.catalogue.elements.NetAppBlueprint bp = netAppPackageService.getNetAppBlueprint(netAppPackageId);
            return new ResponseEntity<NetAppBlueprint>(bp, HttpStatus.OK);

        }catch ( UnAuthorizedRequestException e) {
            log.warn("Unauthorized request", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (NotExistingEntityException e){
                return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Get a NetApp Package")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve NetApp package using its ID."),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{netAppPackageId}", method = RequestMethod.GET)
    public ResponseEntity<?> getNetAppPacakge(@PathVariable UUID netAppPackageId) {
        log.debug("Received request to retrieve a NetApp Package.");
        try {
            File netAppPackage = netAppPackageService.getNetAppPackage(netAppPackageId);
            InputStream inputStream = new FileInputStream(netAppPackage);
            String type=netAppPackage.toURL().openConnection().guessContentTypeFromName(netAppPackage.getName());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("content-disposition", "attachment; filename=" + netAppPackage.getName());
            responseHeaders.add("Content-Type",type);
            byte[]out=org.apache.commons.io.IOUtils.toByteArray(inputStream);
            it.nextworks.catalogue.elements.NetAppBlueprint bp = netAppPackageService.getNetAppBlueprint(netAppPackageId);
            return  new ResponseEntity(out, responseHeaders,HttpStatus.OK);
        }catch ( UnAuthorizedRequestException e){
            log.warn("Unauthorized request",e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (NotExistingEntityException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get NetApp Packages")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve NetApp Packages.", response = it.nextworks.catalogue.elements.NetAppPackageInfo.class, responseContainer = "List"),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public ResponseEntity<?> getAllNetAppPackages() {
        log.debug("Received request to retrieve all NetApp Packages");
        try {

            List<it.nextworks.catalogue.elements.NetAppPackageInfo> infos = netAppPackageService.getAllNetAppPackages();
            return new ResponseEntity<>(infos, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Query NetApp Blueprints")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of NetApp Packages.", response = it.nextworks.catalogue.elements.NetAppPackageInfo.class, responseContainer = "List"),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/blueprint", method = RequestMethod.GET)
    public ResponseEntity<?> queryNetAppPackages(@RequestParam(required = false) it.nextworks.catalogue.elements.Testbed testbed, @RequestParam(required = false) it.nextworks.catalogue.elements.AccessLevel accessLevel, @RequestParam(required = false) it.nextworks.catalogue.elements.NetAppSpecLevel specLevel) {
        log.debug("Received request to query NetApp Packages");
        try {

            List<it.nextworks.catalogue.elements.NetAppPackageInfo> infos = netAppPackageService.queryNetAppPackages(testbed, accessLevel, specLevel);
            return new ResponseEntity<>(infos, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Delete NetApp Package")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Deleted an existing NetApp", response = it.nextworks.catalogue.elements.NetAppPackageInfo.class, responseContainer = "List"),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "The request contains conflict elements", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{netAppPackageId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteNetAppPackage(@PathVariable UUID netAppPackageId, @RequestParam(required = false) boolean forced) {
        log.debug("Received request to delete a NetApp Packages:"+netAppPackageId);
        try {

            netAppPackageService.deleteNetAppPackage(netAppPackageId, forced);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }catch ( UnAuthorizedRequestException e) {
            log.warn("Unauthorized request", e);

            return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }catch (FailedOperationException e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
        }catch (NotExistingEntityException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get a NetApp Software Documentation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve NetApp package using its ID."),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element not found", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{netAppPackageId}/softwaredocs/{softwareDocId}", method = RequestMethod.GET)
    public ResponseEntity<?> getNetAppPacakgeSoftwareDoc(@PathVariable UUID netAppPackageId, String softwareDocId) {
        log.debug("Received request to retrieve a NetApp Package.");
        try {
            File softwareDoc = netAppPackageService.getNetAppPackageSoftwareDoc(netAppPackageId, softwareDocId);
            InputStream inputStream = new FileInputStream(softwareDoc);
            String type=softwareDoc.toURL().openConnection().guessContentTypeFromName(softwareDoc.getName());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("content-disposition", "attachment; filename=" + softwareDoc.getName());
            responseHeaders.add("Content-Type",type);
            byte[]out=org.apache.commons.io.IOUtils.toByteArray(inputStream);

            return  new ResponseEntity(out, responseHeaders,HttpStatus.OK);
        }catch ( UnAuthorizedRequestException e){
            log.warn("Unauthorized request",e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (NotExistingEntityException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>("Internal server error. Please contact a Platform ADMIN",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



}
