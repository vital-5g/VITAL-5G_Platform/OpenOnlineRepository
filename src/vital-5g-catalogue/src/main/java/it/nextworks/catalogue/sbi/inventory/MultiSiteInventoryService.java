package it.nextworks.catalogue.sbi.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.sbi.inventory.elements.NfvoDriverType;
import it.nextworks.catalogue.sbi.inventory.elements.TestbedNfvoInformation;

import it.nextworks.catalogue.sbi.inventory.rest.MultiSiteInventoryRestClient;
import it.nextworks.inventory.elements.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@Service
public class MultiSiteInventoryService {

    private static final Logger log = LoggerFactory.getLogger(MultiSiteInventoryService.class);

    @Value("${site-inventory.url:http://localhost:8084/multisite-inventory}")
    private String multisiteInventoryUrl;

    private MultiSiteInventoryRestClient restClient;

    @PostConstruct
    void setupMultiSiteInventoryClient(){
        log.debug("Configuring Multisite Inventory client with URL:"+ multisiteInventoryUrl);
        restClient = new MultiSiteInventoryRestClient(multisiteInventoryUrl);
    }


    public TestbedNfvoInformation getTestbedNfvoInformation(Testbed testbed, String environment) throws FailedOperationException {
        log.debug("Received request to retrieve Testbed NFVO information: "+testbed + " "+environment);
        //return new TestbedNfvoInformation(nfvoBaseUrl, vimAccountId, username, password, nfvoDriverType, project, vimExternalNetwork);
        List<it.nextworks.inventory.elements.TestbedNfvoInformation> nfvos = restClient.getNfvoInformation(it.nextworks.inventory.elements.Testbed.valueOf(testbed.toString()), Environment.valueOf(environment));
        if(!nfvos.isEmpty()){
            it.nextworks.inventory.elements.TestbedNfvoInformation rNfvo = nfvos.get(0);
            return new TestbedNfvoInformation(rNfvo.getBaseUrl(), rNfvo.getVimAccountId(), rNfvo.getUsername(), rNfvo.getPassword(),
                    NfvoDriverType.valueOf(rNfvo.getNfvoDriverType().toString()), rNfvo.getProject(), rNfvo.getVimExternalNetwork());
        }else{
            log.error("Could not find NFVO information for specified testbed/environment");
            return null;
        }



    }

}
