package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.SliceProfile;
import it.nextworks.catalogue.elements.SoftwareLicense;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SoftwareLicenseRepo  extends JpaRepository<SoftwareLicense, Long> {
}
