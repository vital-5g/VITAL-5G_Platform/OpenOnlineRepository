/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.services;


import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.NetAppPackageInfo;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpBlueprintInfo;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprintInfo;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.interfaces.ExpDescriptorCatalogueInterface;
import it.nextworks.catalogue.interfaces.TcBlueprintCatalogueInterface;
import it.nextworks.catalogue.repos.ExpDescriptorRepository;
import it.nextworks.catalogue.repos.TestCaseBlueprintInfoRepository;
import it.nextworks.catalogue.repos.TestCaseBlueprintRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TcBlueprintCatalogueService implements TcBlueprintCatalogueInterface {

	private static final Logger log = LoggerFactory.getLogger(TcBlueprintCatalogueService.class);


	@Autowired
	private SecurityService securityService;



	@Autowired
	private TestCaseBlueprintInfoRepository testCaseBlueprintInfoRepository;
	


	@Autowired
	private TestCaseBlueprintRepository testCaseBlueprintRepository;

	@Autowired
	private ExpBlueprintCatalogueService expBlueprintCatalogueService;




    @Override
    public UUID onboardTcBlueprint(TestCaseBlueprint request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException {
        log.debug("Processing onboarding TestCaseBlueprint request");
        request.isValid();

       if(testCaseBlueprintInfoRepository.findByNameAndVersion(request.getName(), request.getVersion()).isPresent())
		   throw new AlreadyExistingEntityException("Already existing TestCaseBlueprint with given name and version");



		try{
			log.debug("Storing TestCaseBlueprint associated information element");
			String username = securityService.getUsername();

			TestCaseBlueprintInfo info = new TestCaseBlueprintInfo(request.getVersion(), request.getName(),username);
			testCaseBlueprintInfoRepository.saveAndFlush(info);
			AccessLevel accessLevel= request.getAccessLevel();
			if(accessLevel==null){
				log.debug("No Access Level specified, assuming PUBLIC");
				accessLevel=AccessLevel.RESTRICTED;
			}
			TestCaseBlueprint testCaseBlueprint = new TestCaseBlueprint(
					info,
					request.getVersion(),
					request.getName(),
					request.getExecutionAction(),
					request.getConfigurationAction(),
					request.getResetAction(),
					request.getUseCase(),
					accessLevel


				);

			testCaseBlueprintRepository.saveAndFlush(testCaseBlueprint);
			info.setTcBlueprint(testCaseBlueprint);
			testCaseBlueprintInfoRepository.saveAndFlush(info);

			log.debug("Added TestCaseBlueprint " + info.getTestCaseBlueprintId());



			return info.getTestCaseBlueprintId();
		}catch (Exception e){
			log.error("Error creating TestCaseBlueprint:",e);
			throw  new FailedOperationException("Error during TestCaseBlueprint creation. Please contact a Platform Administrator");
		}
    }

	@Override
	public TestCaseBlueprint getTestCaseBlueprint(UUID tcbId) throws MalformattedElementException, NotExistingEntityException, UnAuthorizedRequestException {
		log.debug("Received reuquest to retrieve TestCaseBlueprint with ID:"+tcbId);
		if(tcbId==null){
			throw new MalformattedElementException("TestCaseBlueprint ID not provided");
		}
		Optional<TestCaseBlueprint> tcb = testCaseBlueprintRepository.findByTestCaseBlueprintId(tcbId);
		if(!tcb.isPresent())
			throw  new NotExistingEntityException("TestCaseBlueprint not found in DB");

		if(authorizeTCBAccess(tcbId)){
			return tcb.get();
		}else throw  new UnAuthorizedRequestException("Unauthorized access to TCB");


	}



    @Override
    public List<TestCaseBlueprintInfo> queryTcBlueprint() throws MalformattedElementException, NotExistingEntityException, FailedOperationException{
    	log.debug("Processing a query for TestCaseBlueprint");

		return testCaseBlueprintInfoRepository.findAll().stream().filter(info -> authorizeTCBAccess(info.getTestCaseBlueprintId()))
				.collect(Collectors.toList());

    }



	@Override
    public void deleteTcBlueprint(UUID testCaseBlueprintId, boolean force)
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {
    	log.debug("Processing request to delete an TestCaseBlueprint");

		if  (testCaseBlueprintId == null) throw new MalformattedElementException("TestCaseBlueprint ID is not provided");
		
		Optional<TestCaseBlueprint> tcbOpt = testCaseBlueprintRepository.findByTestCaseBlueprintId(testCaseBlueprintId);
		if (tcbOpt.isPresent()) {
			TestCaseBlueprint tcb = tcbOpt.get();
			String username = securityService.getUsername();

			Optional<TestCaseBlueprintInfo> optInfo = testCaseBlueprintInfoRepository.findByTestCaseBlueprintId(testCaseBlueprintId);
			if(optInfo.isPresent()){
				if(!optInfo.get().getOwner().equals(username) && !securityService.userIsPlatformAdmin()){
					throw new UnAuthorizedRequestException("Only the TCB owner or the platform admin may delete a TCB");
				}
				List<ExpBlueprintInfo> expBlueprints = expBlueprintCatalogueService.getExperimentBlueprintsByTestCaseBlueprint(testCaseBlueprintId);
				if( force){
					log.debug("Forcing TCB removal");
				}else if(expBlueprints!=null && !expBlueprints.isEmpty())

					throw new FailedOperationException("TestCaseBlueprint "+testCaseBlueprintId+" is associated with ExpBs: "+
							expBlueprints.stream().map(e->e.getExpBlueprintId()).collect(Collectors.toList()));
				}


				testCaseBlueprintRepository.delete(tcb);
				log.debug("Removing TestCaseBlueprint associated info element:" + testCaseBlueprintId);
				testCaseBlueprintInfoRepository.delete(optInfo.get());




		} else {
			log.error("TestCaseBlueprint " + testCaseBlueprintId + " not found");
			throw new NotExistingEntityException("TestCaseBlueprint " + testCaseBlueprintId + " not found");
		}
    }



	private boolean authorizeTCBAccess(UUID testCaseId){
		String username = securityService.getUsername();

		Optional<TestCaseBlueprintInfo>  info = testCaseBlueprintInfoRepository.findByTestCaseBlueprintId(testCaseId);
		if(info.get().getTcBlueprint()==null)
			return false;
		String useCase = info.get().getTcBlueprint().getUseCase();

		if(securityService.userIsPlatformAdmin()){
			log.debug("User is platform admin. Access granted");
			return true;
		}else if(info.get().getOwner()!=null && info.get().getOwner().equals(username)){
			log.debug("User is owner. Access granted");
			return true;
		}else if(info.get().getTcBlueprint().getAccessLevel()==null ||
				info.get().getTcBlueprint().getAccessLevel().equals(AccessLevel.PUBLIC)){
			log.debug("TCB is public. Access granted");
			return true;
		}else if(info.get().getTcBlueprint().getAccessLevel().equals(AccessLevel.RESTRICTED) &&
				securityService.userInUseCase(useCase)){
			log.debug("NetApp is Restricted, user in Use Case. Access granted");
			return true;
		}else return false;
	}






}
