package it.nextworks.catalogue.sbi.inventory.rest;

import it.nextworks.inventory.elements.Environment;
import it.nextworks.inventory.elements.Equipment;
import it.nextworks.inventory.elements.Testbed;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.inventory.elements.monitoring.MonitoringInfo;
import it.nextworks.inventory.elements.slicing.SlicingInfo;
import it.nextworks.inventory.exceptions.NotExistingEntityException;
import it.nextworks.inventory.interfaces.MultiSiteInventoryInterface;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

public class MultiSiteInventoryRestClient implements MultiSiteInventoryInterface {


    private String baseUrl;
    public MultiSiteInventoryRestClient(String baseUrl){
        this.baseUrl= baseUrl;
    }
    @Override
    public List<TestbedNfvoInformation> getNfvoInformation(Testbed testbed, Environment environment) {
        RestTemplate restTemplate = new RestTemplate();

        String nfvoResourceUrl
                = baseUrl+"/{testbed}/nfvos?environment={environment}";
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<List<TestbedNfvoInformation>> response
                = restTemplate.exchange(nfvoResourceUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<TestbedNfvoInformation>>() {
        }, testbed, environment);
        return response.getBody();
    }

    @Override
    public UUID onboardTestbedNfvo(TestbedNfvoInformation testbedNfvoInformation) {
        return null;
    }

    @Override
    public UUID onboardTestbedEquipment(Equipment equipment) {
        return null;
    }

    @Override
    public List<Equipment> getEquipment(Testbed testbed) {
        return null;
    }

    @Override
    public Equipment getEquipment(Testbed testbed, UUID uuid) throws NotExistingEntityException {
        return null;
    }

    @Override
    public UUID onboardTestbedSlicingInfo(SlicingInfo slicingInfo) {
        return null;
    }

    @Override
    public SlicingInfo getTestbedSlicingInfo(Testbed testbed) throws NotExistingEntityException {
        return null;
    }

    @Override
    public UUID onboardTestbedMonitoringInfo(MonitoringInfo monitoringInfo) {
        return null;
    }

    @Override
    public MonitoringInfo getTestbedMonitoringInfo(Testbed testbed) throws NotExistingEntityException {
        return null;
    }

    @Override
    public void deleteEquipment(Testbed testbed, UUID uuid) throws NotExistingEntityException {

    }
}
