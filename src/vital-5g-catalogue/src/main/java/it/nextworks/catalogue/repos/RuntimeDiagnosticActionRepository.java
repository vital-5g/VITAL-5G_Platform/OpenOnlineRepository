package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.RuntimeDiagnosticAction;
import it.nextworks.catalogue.elements.RuntimeThresholdAction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuntimeDiagnosticActionRepository extends JpaRepository<RuntimeDiagnosticAction, Long> {
}
