package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.NetAppEndpoint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NetAppEndpointRepository extends JpaRepository<NetAppEndpoint, Long> {
}
