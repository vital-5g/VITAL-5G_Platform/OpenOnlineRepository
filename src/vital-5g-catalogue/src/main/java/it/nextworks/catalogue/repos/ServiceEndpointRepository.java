package it.nextworks.catalogue.repos;

import it.nextworks.catalogue.elements.ServiceEndpoint;
import it.nextworks.catalogue.elements.SoftwareLicense;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceEndpointRepository extends JpaRepository<ServiceEndpoint, Long> {
}
