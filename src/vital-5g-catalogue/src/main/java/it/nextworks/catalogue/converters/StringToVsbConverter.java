package it.nextworks.catalogue.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.catalogue.messages.OnboardVSBRequest;
import it.nextworks.catalogue.nbi.VsDescriptorCatalogueRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StringToVsbConverter implements Converter<String, OnboardVSBRequest> {
    private static final Logger log = LoggerFactory.getLogger(StringToVsbConverter.class);
    @Override
    public OnboardVSBRequest convert(String s) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(s, OnboardVSBRequest.class);
        } catch (IOException e) {
            log.error("Error during VSB Deserialization", e);
            throw new RuntimeException(e);
        }

    }
}
