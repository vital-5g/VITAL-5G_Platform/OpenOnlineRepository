/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.services;



import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.repos.ExpDescriptorRepository;
import it.nextworks.catalogue.repos.ExpDescriptorInfoRepository;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.interfaces.ExpDescriptorCatalogueInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExpDescriptorCatalogueService implements ExpDescriptorCatalogueInterface {

	private static final Logger log = LoggerFactory.getLogger(ExpDescriptorCatalogueService.class);
	




	@Autowired
	private ExpDescriptorInfoRepository expDescriptorInfoRepository;
	
	@Autowired
	private ExpBlueprintCatalogueService expBlueprintCatalogueService;
	

	@Autowired
	private ExpDescriptorRepository expDescriptorRepository;
	




    @Override
    public UUID onboardExpDescriptor(it.nextworks.catalogue.elements.exp.ExpDescriptor request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException {
        log.debug("Processing onboarding experiment descriptor request");
        request.isValid();
        verifyExperimentBlueprintDependencies(request);
        verifyKpiThresholds(request);

        
       if(expDescriptorInfoRepository.findByNameAndVersion(request.getName(), request.getVersion()).isPresent())
		   throw new AlreadyExistingEntityException("Already existing ExpD with given name and version");
        

        //onboard experiment descriptor
		//onboard experiment descriptor
		String idStr=null;
		try{
			log.debug("Storing ExpD associated information element");
			it.nextworks.catalogue.elements.exp.ExpDescriptorInfo expdInfo = new it.nextworks.catalogue.elements.exp.ExpDescriptorInfo(request.getName(), request.getVersion(), request.getExpBlueprintId());
			expDescriptorInfoRepository.saveAndFlush(expdInfo);
			it.nextworks.catalogue.elements.exp.ExpDescriptor expDescriptor = new it.nextworks.catalogue.elements.exp.ExpDescriptor(
					expdInfo,
					request.getName(),
					request.getVersion(),
					request.getExpBlueprintId(),
					request.getAccessLevel(),
					request.getTenantId(),
					request.getKpiThresholds(),
					request.getConfigActionParameters(),
					request.getResetActionParameters(),
					request.getExecutionActionParameters());
			expDescriptorRepository.saveAndFlush(expDescriptor);



			log.debug("Added Experiment Descriptor with ID " + expdInfo.getExpDescriptorId());

			try {
				expBlueprintCatalogueService.addExpdInBlueprint(request.getExpBlueprintId(), expdInfo.getExpDescriptorId());
			} catch (NotExistingEntityException e) {
				throw new FailedOperationException(e.getMessage());
			}



			return expdInfo.getExpDescriptorId();
		}catch (Exception e){
			log.error("Error creating EXPD:",e);
			throw  new FailedOperationException(e.getMessage());
		}
    }

	@Override
	public it.nextworks.catalogue.elements.exp.ExpDescriptor getExpDescriptor(UUID expdId) throws MalformattedElementException, NotExistingEntityException {
		log.debug("Received reuquest to retrieve ExpD with ID:"+expdId);
		if(expdId==null){
			throw new MalformattedElementException("ExpD ID not provided");
		}
		Optional<it.nextworks.catalogue.elements.exp.ExpDescriptor> expd = expDescriptorRepository.findByExpDescriptorId(expdId);
		if(!expd.isPresent())
			throw  new NotExistingEntityException("ExpD not found in DB");
		return expd.get();

	}

	private void verifyKpiThresholds(it.nextworks.catalogue.elements.exp.ExpDescriptor request) throws MalformattedElementException {
        log.debug("Verifiying KPI thresholds");

        if(request.getKpiThresholds()!=null && !request.getKpiThresholds().isEmpty()){
			it.nextworks.catalogue.elements.exp.ExpBlueprint expBlueprint = null;
			try {
				expBlueprint = expBlueprintCatalogueService.getExperimentBlueprint(request.getExpBlueprintId());
			} catch (NotExistingEntityException e) {
				log.error("Failed to retrieve ExpB");
			}
			if(expBlueprint.getKpis()!=null && !expBlueprint.getKpis().isEmpty()){
                List<String> expbKpis = expBlueprint.getKpis().stream()
                        .map(expbK -> expbK.getKpiId())
                        .collect(Collectors.toList());
                for(it.nextworks.catalogue.elements.exp.KpiSpecification kpi : request.getKpiThresholds()){
                    if(!expbKpis.contains(kpi.getKpiId())) {
						log.error("KPI " + kpi.getKpiId() + " not defined for the experiment");
						throw new MalformattedElementException("KPI " + kpi.getKpiId() + " not defined for the experiment");
					}
                }
            }else throw new MalformattedElementException("KPI threshold for experiment without KPIs");


        }


    }

    @Override
    public List<it.nextworks.catalogue.elements.exp.ExpDescriptorInfo> queryExpDescriptor() throws MalformattedElementException, NotExistingEntityException, FailedOperationException{
    	log.debug("Processing a query for an Experiment descriptor");


		return expDescriptorInfoRepository.findAll();
    }



	@Override
    public void deleteExpDescriptor(UUID expDescriptorId, boolean force)
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
    	log.debug("Processing request to delete an Exp descriptor");

		if  (expDescriptorId == null) throw new MalformattedElementException("ExpD ID is null");
		
		Optional<it.nextworks.catalogue.elements.exp.ExpDescriptor> expdOpt = expDescriptorRepository.findByExpDescriptorId(expDescriptorId);
		if (expdOpt.isPresent()) {
			it.nextworks.catalogue.elements.exp.ExpDescriptor expd = expdOpt.get();

			Optional<it.nextworks.catalogue.elements.exp.ExpDescriptorInfo> optExpDInfo = expDescriptorInfoRepository.findByExpDescriptorId(expDescriptorId);
			if(optExpDInfo.isPresent()){
				List<UUID> activeExperiments =optExpDInfo.get().getActiveExperimentIds();
				if( force){
					log.debug("Forcing ExpD removal");
				}else if(activeExperiments!=null && !activeExperiments.isEmpty())

					throw new FailedOperationException("ExpD "+expDescriptorId+" still has active experiments: "+activeExperiments);
				}

				UUID expbId = expd.getExpBlueprintId();
				expDescriptorRepository.delete(expd);
				log.debug("Removing ExpD associated info element:" + expDescriptorId);
				expDescriptorInfoRepository.delete(optExpDInfo.get());

				expBlueprintCatalogueService.removeExpdInBlueprint(expbId, expDescriptorId);
				log.debug("EXPD " + expDescriptorId + " removed from the internal DB.");



		} else {
			log.error("ExpD " + expDescriptorId + " not found");
			throw new NotExistingEntityException("ExpD " + expDescriptorId + " not found");
		}
    }


    

    

    

    
    /**
     * Verify if all the related blueprints are available in the DB and the provided parameters match with the ones in the blueprints
     * 
     * @param request input
     * @throws NotExistingEntityException
     */
    private void verifyExperimentBlueprintDependencies(ExpDescriptor request) throws NotExistingEntityException {
    	log.debug("Verifying dependencies for experiment descriptor.");
    	//verify experiment blueprint presence

		try {
			expBlueprintCatalogueService.getExperimentBlueprint(request.getExpBlueprintId());
		} catch (MalformattedElementException e) {
			log.error("ERROR", e);
		}

    	log.debug("Verified all the dependencies of the descriptor");
    	
    }



	@Override
    public void useExpDescriptor(UUID expdId, UUID experimentId)throws  MalformattedElementException, NotExistingEntityException {
		log.debug("Received request to add experiment: "+experimentId+" from ExpD:"+expdId);
		if  (expdId == null || experimentId==null) throw new MalformattedElementException("ExpD ID or experiment is null");

		Optional<it.nextworks.catalogue.elements.exp.ExpDescriptorInfo> expdInfoOpt = expDescriptorInfoRepository.findByExpDescriptorId(expdId);
		if(expdInfoOpt.isPresent()){
			it.nextworks.catalogue.elements.exp.ExpDescriptorInfo expDInfo = expdInfoOpt.get();
			List<UUID> activeExperimentIds = expDInfo.getActiveExperimentIds();
			if(!activeExperimentIds.contains(experimentId)){
				activeExperimentIds.add(experimentId);
				expDInfo.setActiveExperimentIds(activeExperimentIds);
				expDescriptorInfoRepository.saveAndFlush(expDInfo);
				log.debug("Correctly added experiment:"+experimentId+" from ExpD:"+expdId);

			}else{
				log.warn("Experiment ID: "+experimentId+" already in the list of ExpD: "+ expdId+"active experiments, ignoring");
			}


		}else throw  new NotExistingEntityException("Could not find ExpD Info with id"+expdId);

	}

	@Override
	public void releaseExpDescriptor(UUID expdId, UUID experimentId) throws  MalformattedElementException, NotExistingEntityException {
		log.debug("Received request to remove experiment: "+experimentId+" from ExpD:"+expdId);
		if  (expdId == null) throw new MalformattedElementException("ExpD ID is null");

		Optional<it.nextworks.catalogue.elements.exp.ExpDescriptorInfo> expdInfoOpt = expDescriptorInfoRepository.findByExpDescriptorId(expdId);
    	if(expdInfoOpt.isPresent()){
    		it.nextworks.catalogue.elements.exp.ExpDescriptorInfo expDInfo = expdInfoOpt.get();
    		List<UUID> activeExperimentIds = expDInfo.getActiveExperimentIds();
    		boolean removed = activeExperimentIds.remove(experimentId);
    		if(removed){
    			expDInfo.setActiveExperimentIds(activeExperimentIds);
    			expDescriptorInfoRepository.saveAndFlush(expDInfo);
    			log.debug("Correctly removed experiment:"+experimentId+" from ExpD:"+expdId);
			}else throw new NotExistingEntityException("Failed to remove experiment:"+experimentId+" from ExpD:"+expdId);

		}else throw  new NotExistingEntityException("Could not find ExpD with id"+expdId);
	}







}
