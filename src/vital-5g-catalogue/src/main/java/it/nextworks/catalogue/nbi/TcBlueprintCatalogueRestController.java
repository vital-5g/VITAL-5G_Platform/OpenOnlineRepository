/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.exp.ExpDescriptorInfo;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprintInfo;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.services.TcBlueprintCatalogueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@Api(tags = "Test Case Blueprint Catalogue API")
@RestController
@CrossOrigin
@RequestMapping("/portal/catalogue/tcbs")
public class TcBlueprintCatalogueRestController {

	private static final Logger log = LoggerFactory.getLogger(TcBlueprintCatalogueRestController.class);

	@Autowired
	private TcBlueprintCatalogueService tcBlueprintCatalogueService;




	public TcBlueprintCatalogueRestController() { }
	
	@ApiOperation(value = "Onboard Experiment Descriptor")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Element created. Returns the id of the element created.", response = UUID.class),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> createTestCaseBlueprint(@RequestBody TestCaseBlueprint request) {
		log.debug("Received request to onboard an TestCaseBlueprint");

		try {
			UUID id = tcBlueprintCatalogueService.onboardTcBlueprint(request);
			return new ResponseEntity<UUID>(id, HttpStatus.CREATED);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request"+e.getMessage(), e );
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (AlreadyExistingEntityException e) {
			log.error("TestCaseBlueprint already existing");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}


	}
	
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAllTestCaseBlueprints() {
		log.debug("Received request to retrieve all the TestCaseBlueprints");

		try {
			List<TestCaseBlueprintInfo> response= tcBlueprintCatalogueService.queryTcBlueprint();


			return new ResponseEntity<List<TestCaseBlueprintInfo>>(response, HttpStatus.OK);
		}  catch (Exception e) {
			log.error("Internal exception", e);
			return new ResponseEntity<String>("Generic error retrieving TCBs. Please contact a Platform admin", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTestCaseBlueprint(@PathVariable UUID id) {
		log.debug("Received request to retrieve TestCaseBlueprint with ID " + id);
		try {


			return new ResponseEntity<TestCaseBlueprint>(tcBlueprintCatalogueService.getTestCaseBlueprint(id), HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request", e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("TestCaseBlueprint not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTestCaseBlueprint(@PathVariable UUID id, @RequestParam(required = false) boolean force) {
		log.debug("Received request to delete TestCaseBlueprint with ID " + id);

		try {

			tcBlueprintCatalogueService.deleteTcBlueprint(id, force);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("TestCaseBlueprint not found");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	
}
