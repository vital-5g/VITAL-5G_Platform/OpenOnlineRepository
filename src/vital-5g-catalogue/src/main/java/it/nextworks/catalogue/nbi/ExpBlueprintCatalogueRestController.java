/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


import it.nextworks.catalogue.services.ExpBlueprintCatalogueService;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpBlueprintInfo;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@Api(tags = "Experiment Blueprint Catalogue API")
@RestController
@CrossOrigin
@RequestMapping("/portal/catalogue/expbs")
public class ExpBlueprintCatalogueRestController {

	private static final Logger log = LoggerFactory.getLogger(ExpBlueprintCatalogueRestController.class);

	@Autowired
	private ExpBlueprintCatalogueService expBlueprintCatalogueService;



	public ExpBlueprintCatalogueRestController() { }

	@ApiOperation(value = "Onboard ExpBlueprint")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Creates a new experiment blueprint and returns its ID.", response = UUID.class),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> createExpBlueprint(@RequestBody ExpBlueprint request) {
		log.debug("Received request to create a EXP blueprint.");

		try {
			UUID expBlueprintId = expBlueprintCatalogueService.onboardExperimentBlueprint(request);
			return new ResponseEntity<UUID>(expBlueprintId, HttpStatus.CREATED);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request"+e.getMessage(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (AlreadyExistingEntityException e) {
			log.error("EXP Blueprint already existing");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
		} catch (Exception e) {
			log.error("Internal exception", e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}


	@ApiOperation(value = "Get ALL ExpBlueprints")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retrieve the list of all the experiment blueprints of the user", response = ExpBlueprintInfo.class, responseContainer = "Set"),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 404, message = "The element with the supplied id was not found", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
	})

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAllExpBlueprints() {
		log.debug("Received request to retrieve all the EXP blueprints.");


		try {
			List<ExpBlueprintInfo> expBlueprintId = expBlueprintCatalogueService.getAllExperimentBlueprints();
			return new ResponseEntity<>(expBlueprintId, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Internal exception");
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}


	}

	@ApiOperation(value = "Get ExpBlueprint")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Returns the experiment blueprint with the given ID", response = ExpBlueprint.class),
			//@ApiResponse(code = 400, message = "The supplied element contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 404, message = "The element with the supplied id was not found", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@RequestMapping(value = "/{expbId}", method = RequestMethod.GET)
	public ResponseEntity<?> getExpBlueprint(@PathVariable UUID expbId) {

			log.debug("Received request to retrieve EXP blueprint with ID " + expbId);

			try {

				return new ResponseEntity<ExpBlueprint>(expBlueprintCatalogueService.getExperimentBlueprint(expbId), HttpStatus.OK);
			} catch (MalformattedElementException e) {
				log.error("Malformatted request"+e.getMessage());
				return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
			} catch (NotExistingEntityException e) {
				log.error("EXP Blueprints not found");
				return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
			} catch (Exception e) {
				log.error("Internal exception");
				return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}


	}





	@ApiOperation(value = "Delete ExpBlueprint")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Delete the experiment blueprint with the given ID", response = ResponseEntity.class),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 404, message = "The element with the supplied id was not found", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)
	})
	@RequestMapping(value = "/{expbId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteExpBlueprint(@PathVariable UUID expbId, @RequestParam(required = false) boolean forced) {



        try {
            expBlueprintCatalogueService.deleteExperimentBlueprint(expbId, forced);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (MalformattedElementException e) {
            log.error("Malformatted request");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NotExistingEntityException e) {
            log.error("EXP Blueprints not found");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Internal exception");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }


	}

}
