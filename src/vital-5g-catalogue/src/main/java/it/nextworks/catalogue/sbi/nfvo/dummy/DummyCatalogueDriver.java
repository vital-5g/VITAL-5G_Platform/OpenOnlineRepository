package it.nextworks.catalogue.sbi.nfvo.dummy;

import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.sbi.nfvo.interfaces.NfvoCatalogueInterface;

import java.io.File;
import java.util.UUID;

public class DummyCatalogueDriver implements NfvoCatalogueInterface {
    @Override
    public UUID onboardNSD(File nsd, Testbed testbed) throws FailedOperationException {
        return  UUID.randomUUID();
    }

    @Override
    public UUID onboardVNF(File vnfPackage, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException {
        return  UUID.randomUUID();
    }
}
