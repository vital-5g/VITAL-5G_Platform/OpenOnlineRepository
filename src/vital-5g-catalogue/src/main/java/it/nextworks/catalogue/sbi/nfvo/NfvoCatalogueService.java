package it.nextworks.catalogue.sbi.nfvo;

import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.nbi.VerticalServiceBlueprintRestController;
import it.nextworks.catalogue.sbi.inventory.MultiSiteInventoryService;
import it.nextworks.catalogue.sbi.inventory.elements.NfvoDriverType;
import it.nextworks.catalogue.sbi.inventory.elements.TestbedNfvoInformation;
import it.nextworks.catalogue.sbi.nfvo.dummy.DummyCatalogueDriver;
import it.nextworks.catalogue.sbi.nfvo.interfaces.NfvoCatalogueInterface;
import it.nextworks.catalogue.sbi.nfvo.osm.Osm10CatalogueClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class NfvoCatalogueService implements NfvoCatalogueInterface {

    @Autowired
    private MultiSiteInventoryService multiSiteInventoryService;


    private static final Logger log = LoggerFactory.getLogger(NfvoCatalogueService.class);

    @Value("${environment:DEVELOPMENT}")
    private String environment;

    private Map<Testbed, NfvoCatalogueInterface> nfvoDrivers = new HashMap<>();


    @Override
    public UUID onboardNSD(File nsd, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException {
        log.debug("Onboarding NSD");
        NfvoCatalogueInterface driver = null;

            log.debug("Retrieving testbed NFVO information");

            TestbedNfvoInformation nfvoInformation = multiSiteInventoryService.getTestbedNfvoInformation(testbed, environment );
            log.debug("Info:" +nfvoInformation.getBaseUrl()+" "+ nfvoInformation.getNfvoDriverType());
            if(nfvoInformation.getNfvoDriverType().equals(NfvoDriverType.OSM10)){
                driver= new Osm10CatalogueClient(nfvoInformation);
                nfvoDrivers.put(testbed, driver);
            }else if(nfvoInformation.getNfvoDriverType().equals(NfvoDriverType.DUMMY)) {
                driver= new DummyCatalogueDriver();
                nfvoDrivers.put(testbed, driver);
            }else throw new FailedOperationException("Unknown NFVO Driver Type");

        return driver.onboardNSD(nsd, testbed);


    }

    @Override
    public UUID onboardVNF(File vnfPackage, Testbed testbed) throws FailedOperationException, AlreadyExistingEntityException {
        log.debug("Onboarding VNF Package");
        NfvoCatalogueInterface driver = null;

            log.debug("Retrieving testbed NFVO information");

            TestbedNfvoInformation nfvoInformation = multiSiteInventoryService.getTestbedNfvoInformation(testbed, environment );
            log.debug("Info:" +nfvoInformation.getBaseUrl()+" "+ nfvoInformation.getNfvoDriverType());
            if(nfvoInformation.getNfvoDriverType().equals(NfvoDriverType.OSM10)){
                driver= new Osm10CatalogueClient(nfvoInformation);
                nfvoDrivers.put(testbed, driver);
            }else if(nfvoInformation.getNfvoDriverType().equals(NfvoDriverType.DUMMY)) {
                driver= new DummyCatalogueDriver();
                nfvoDrivers.put(testbed, driver);
            }else throw new FailedOperationException("Unknown NFVO Driver Type");

        return driver.onboardVNF(vnfPackage, testbed);

    }
}
