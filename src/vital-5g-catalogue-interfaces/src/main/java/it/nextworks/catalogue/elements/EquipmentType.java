package it.nextworks.catalogue.elements;

public enum EquipmentType {
    SENSOR,
    IOT_GW,
    ACTUATOR,
    OTHER


}
