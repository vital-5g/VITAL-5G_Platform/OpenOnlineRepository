/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements.exp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.*;
import java.util.*;

@Entity
public class ExpDescriptor implements DescriptorInformationElement {

	@Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

	private UUID expDescriptorId;

	@JsonIgnore
	@OneToOne(fetch=FetchType.EAGER, cascade= CascadeType.REMOVE)
	@JoinColumn(name="exp_descriptor_info")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private it.nextworks.catalogue.elements.exp.ExpDescriptorInfo expDescriptorInfo;

	private String name;
	private String version;
	private UUID expBlueprintId;


	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private Map<String, String> configActionParameters=new HashMap<>();

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private Map<String, String> resetActionParameters=new HashMap<>();

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private Map<String, String> executionActionParameters=new HashMap<>();


	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<KpiSpecification> kpiThresholds = new ArrayList<>();

	private AccessLevel accessLevel;
	
	@JsonIgnore
	private String tenantId;
	
	public ExpDescriptor() {	}
	
	

	/**
	 * Constructor
	 * 
	 * @param name
	 * @param version
	 * @param expBlueprintId

	 * @param tenantId


	 * @param kpiThresholds
	 */
	public ExpDescriptor(
			it.nextworks.catalogue.elements.exp.ExpDescriptorInfo expDescriptorInfo,
			String name,
			String version, 
			UUID expBlueprintId,
			AccessLevel accessLevel,
			String tenantId,

			List<KpiSpecification> kpiThresholds,
			Map<String, String> configActionParameters,
			Map<String,String> resetActionParameters,
			Map<String, String> executionActionParameters) {
		this.expDescriptorInfo=expDescriptorInfo;
		if(expDescriptorInfo!=null) this.expDescriptorId=expDescriptorInfo.getExpDescriptorId();
		this.name = name;
		this.version = version;
		this.expBlueprintId = expBlueprintId;
		this.accessLevel=accessLevel;
		this.tenantId = tenantId;
		if(configActionParameters!=null)
			this.configActionParameters=configActionParameters;
		if(executionActionParameters!=null)
			this.executionActionParameters=executionActionParameters;
		if(resetActionParameters!=null)
			this.resetActionParameters=resetActionParameters;


		if (kpiThresholds != null) this.kpiThresholds = kpiThresholds;
	}

	public AccessLevel getAccessLevel() {
		return accessLevel;
	}

	/**
	 * @return the expDescriptorId
	 */
	public UUID getExpDescriptorId() {
		return expDescriptorId;
	}

	public ExpDescriptorInfo getExpDescriptorInfo() {
		return expDescriptorInfo;
	}


	public Map<String, String> getConfigActionParameters() {
		return configActionParameters;
	}

	public Map<String, String> getResetActionParameters() {
		return resetActionParameters;
	}

	public Map<String, String> getExecutionActionParameters() {
		return executionActionParameters;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}



	/**
	 * @return the expBlueprintId
	 */
	public UUID getExpBlueprintId() {
		return expBlueprintId;
	}




	/**
	 * @return the tenantId
	 */
	@JsonIgnore
	public String getTenantId() {
		return tenantId;
	}




	



	/**
	 * @return the kpiThresholds
	 */
	public List<KpiSpecification> getKpiThresholds() {
		return kpiThresholds;
	}



	@Override
	public void isValid() throws MalformattedElementException {
		if (name == null) throw new MalformattedElementException("ExpD without name");
		if (version == null) throw new MalformattedElementException("ExpD without version");
		if (expBlueprintId == null) throw new MalformattedElementException("ExpD without blueprint ID");
	}

}
