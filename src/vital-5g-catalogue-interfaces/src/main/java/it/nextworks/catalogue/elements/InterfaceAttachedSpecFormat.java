package it.nextworks.catalogue.elements;

public enum InterfaceAttachedSpecFormat {
    OPEN_API,
    SQL_SCHEMA,
    JSON_SCHEMA,
    DOCUMENTATION,
    OTHER
}
