package it.nextworks.catalogue.elements;

public enum EquipmentAccessLevel {
    READ,
    READ_WRITE,
    WRITE
}
