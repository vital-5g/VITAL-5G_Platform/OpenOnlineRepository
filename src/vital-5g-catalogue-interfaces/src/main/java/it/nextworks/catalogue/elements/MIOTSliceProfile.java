package it.nextworks.catalogue.elements;

import javax.persistence.Embeddable;

@Embeddable
public class MIOTSliceProfile extends  SliceProfile{


    public MIOTSliceProfile(){
        super(SliceProfileType.M_IoT, null);
    }
}
