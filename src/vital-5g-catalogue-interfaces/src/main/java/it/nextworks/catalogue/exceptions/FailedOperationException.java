package it.nextworks.catalogue.exceptions;

import java.io.IOException;

public class FailedOperationException extends Exception {
    public FailedOperationException(Exception e) {
        super(e);
    }

    public FailedOperationException(String s) {
        super(s);
    }
}
