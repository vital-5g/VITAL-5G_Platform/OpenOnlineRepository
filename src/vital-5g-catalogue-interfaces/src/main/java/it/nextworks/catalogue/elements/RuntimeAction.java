package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "triggerType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RuntimeThresholdAction.class, name = "THRESHOLD"),
        @JsonSubTypes.Type(value = RuntimeDiagnosticAction.class, name = "DIAGNOSTIC_EVENT"),

})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class RuntimeAction {

    @Id
    @GeneratedValue
    @JsonIgnore
    private long id;

    @JsonIgnore
    @ManyToOne
    private VerticalServiceBlueprint verticalServiceBlueprint;

    private String runtimeActionId;

    private RuntimeActionTriggerType triggerType;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "target_id", referencedColumnName = "id")
    private RuntimeTarget target;

    public RuntimeAction() {
    }

    public RuntimeAction(RuntimeActionTriggerType triggerType) {
        this.triggerType=triggerType;
    }
    public RuntimeAction(VerticalServiceBlueprint verticalServiceBlueprint, String runtimeActionId, RuntimeActionTriggerType triggerType, RuntimeTarget target) {
        this.runtimeActionId = runtimeActionId;
        this.triggerType = triggerType;
        this.verticalServiceBlueprint=verticalServiceBlueprint;
        this.target = target;
    }

    public String getRuntimeActionId() {
        return runtimeActionId;
    }

    public void setRuntimeActionId(String runtimeActionId) {
        this.runtimeActionId = runtimeActionId;
    }

    public RuntimeActionTriggerType getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(RuntimeActionTriggerType triggerType) {
        this.triggerType = triggerType;
    }


    public RuntimeTarget getTarget() {
        return target;
    }

    public void setTarget(RuntimeTarget target) {
        this.target = target;
    }

    public VerticalServiceBlueprint getVerticalServiceBlueprint() {
        return verticalServiceBlueprint;
    }

    public long getId() {
        return id;
    }
}
