package it.nextworks.catalogue.interfaces;

import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;

import java.io.File;
import java.util.List;
import java.util.UUID;

public interface NetAppPackageManagementInterface {


    UUID onboardNetAppPackage(File file) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException;
    UUID onboardNetAppPackage(File file, File vnfPackage) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException;

    NetAppBlueprint getNetAppBlueprint(UUID netAppPackageId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException;

    File getNetAppPackage(UUID netAppPackageId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException;


    List<it.nextworks.catalogue.elements.NetAppPackageInfo> queryNetAppPackages(it.nextworks.catalogue.elements.Testbed testbed, it.nextworks.catalogue.elements.AccessLevel accessLevel, it.nextworks.catalogue.elements.NetAppSpecLevel specLevel);
    List<it.nextworks.catalogue.elements.NetAppPackageInfo> getAllNetAppPackages();

    void deleteNetAppPackage(UUID netAppPackageId, boolean forced) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException;


    File getNetAppPackageSoftwareDoc(UUID netAppPackageId, String softwareDocId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException;
}
