package it.nextworks.catalogue.elements;

public enum RuntimeThresholdOperator {
    LE,
    LT,
    GE,
    GT,
    EQ
}
