/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.interfaces;


import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.exp.ExpDescriptorInfo;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprintInfo;
import it.nextworks.catalogue.exceptions.*;

import java.util.List;
import java.util.UUID;

public interface TcBlueprintCatalogueInterface {

	/**
	 * Method to create a new TCB
	 * 
	 * @param request
	 * @return

	 * @throws MalformattedElementException
	 * @throws AlreadyExistingEntityException
	 * @throws FailedOperationException
	 */
	public UUID onboardTcBlueprint(TestCaseBlueprint request)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException;
	
	/**
	 * Method to query existing TcBs
	 * 

	 * @return
	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	List<TestCaseBlueprintInfo> queryTcBlueprint()
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException;


	TestCaseBlueprint getTestCaseBlueprint(UUID tcbId) throws MalformattedElementException, NotExistingEntityException, UnAuthorizedRequestException;
	/**
	 * Method to remove a TCB
	 * 
	 * @param testCaseBlueprintId

	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	public void deleteTcBlueprint(UUID testCaseBlueprintId, boolean force)
            throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException;






}
