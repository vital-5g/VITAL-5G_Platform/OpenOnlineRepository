package it.nextworks.catalogue.elements;

public enum InterfaceRole {

    CONSUMER,
    PROVIDER
}
