package it.nextworks.catalogue.elements;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

@Entity
public class RuntimeNSScaleTarget extends RuntimeTarget{

    @Basic
    private String scalingAspect;

    private String vnfdId;

    private Integer cpus;

    private Integer ram;




    public RuntimeNSScaleTarget() {
        super(RuntimeTargetType.NS_SCALE);
    }

    public RuntimeNSScaleTarget(String scalingAspect, String vnfdId, Integer cpus, Integer ram) {
        super(RuntimeTargetType.NS_SCALE);
        this.scalingAspect = scalingAspect;
        this.cpus=cpus;
        this.ram=ram;
        this.vnfdId=vnfdId;

    }

    public String getScalingAspect() {
        return scalingAspect;
    }

    public void setScalingAspect(String scalingAspect) {
        this.scalingAspect = scalingAspect;
    }

    public String getVnfdId() {
        return vnfdId;
    }

    public Integer getCpus() {
        return cpus;
    }

    public Integer getRam() {
        return ram;
    }
}
