package it.nextworks.catalogue.interfaces;

import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpBlueprintInfo;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;

import java.util.List;
import java.util.UUID;

public interface ExpBlueprintCatalogueInterface {

     UUID onboardExperimentBlueprint(ExpBlueprint vsb) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException;


     ExpBlueprint getExperimentBlueprint(UUID expbId) throws NotExistingEntityException, MalformattedElementException;
     List<ExpBlueprintInfo> queryExperimentBlueprint(Testbed testbed) throws MalformattedElementException, FailedOperationException;
     List<ExpBlueprintInfo> getAllExperimentBlueprints();
     void deleteExperimentBlueprint(UUID expbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException, MalformattedElementException;
     List<ExpBlueprintInfo> getExperimentBlueprintsByTestCaseBlueprint(UUID testCaseBlueprintId);
}
