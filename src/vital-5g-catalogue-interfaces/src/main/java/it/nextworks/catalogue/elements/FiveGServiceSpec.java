package it.nextworks.catalogue.elements;

import javax.persistence.Embeddable;

@Embeddable
public class FiveGServiceSpec {

    private String fiveGServiceSpecid;

    private String version;

    private it.nextworks.catalogue.elements.FiveGFunction function;

    private boolean mandatory;

    private String name;

    public FiveGServiceSpec(String fiveGServiceSpecid, String version, it.nextworks.catalogue.elements.FiveGFunction function, boolean mandatory, String name) {
        this.fiveGServiceSpecid = fiveGServiceSpecid;
        this.version = version;
        this.function = function;
        this.mandatory = mandatory;
        this.name = name;
    }

    public FiveGServiceSpec() {
    }

    public String getFiveGServiceSpecid() {
        return fiveGServiceSpecid;
    }

    public String getVersion() {
        return version;
    }

    public FiveGFunction getFunction() {
        return function;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public String getName() {
        return name;
    }
}
