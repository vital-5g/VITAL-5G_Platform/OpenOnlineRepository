/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements.exp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.exceptions.MalformattedElementException;


import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.*;

@Entity
public class ExpBlueprintInfo implements DescriptorInformationElement {
	
	@Id
    @GeneratedValue

    private UUID expBlueprintId;
	private String version;
	private String name;
	@JsonIgnore
	private String owner;

	@JsonIgnore
	@OneToOne(fetch= FetchType.EAGER, mappedBy = "expBlueprintInfo", cascade=CascadeType.ALL, orphanRemoval=true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ExpBlueprint expBlueprint;
	

	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<UUID> activeExpdId = new ArrayList<>();







	public ExpBlueprintInfo() {	}
	
	

	/**

	 * @param version
	 * @param name
	 * @param owner
	 */
	public ExpBlueprintInfo(String version, String name, String owner) {

		this.version = version;
		this.name = name;
		this.owner=owner;


	}








	/**
	 * @return the ExpBlueprint
	 */
	public ExpBlueprint getExpBlueprint() {
		return expBlueprint;
	}

	/**
	 * @param expBlueprint the ExpBlueprint to set
	 */
	public void setExpBlueprint(ExpBlueprint expBlueprint) {
		this.expBlueprint = expBlueprint;
	}

	/**
	 * @return the expBlueprintId
	 */
	public UUID getExpBlueprintId() {
		return expBlueprintId;
	}

	/**
	 * @return the expBlueprintVersion
	 */
	public String getVersion() {
		return version;
	}



	/**
	 * @return the activeExpdId
	 */
	public List<UUID> getActiveExpdId() {
		return activeExpdId;
	}
	
	public void addExpd(UUID expdId) {
		if (!(activeExpdId.contains(expdId)))
			activeExpdId.add(expdId);
	}
	
	public void removeExpd(UUID expdId) {
		if (activeExpdId.contains(expdId))
			activeExpdId.remove(expdId);
	}




	
	@Override
	public void isValid() throws MalformattedElementException {

		if (version == null) throw new MalformattedElementException("EXP Blueprint info without EXP version");
		if (name == null) throw new MalformattedElementException("EXP Blueprint info without Exp name");
	}
	
	public String getName() {
		return name;
	}

	@JsonIgnore
	public String getOwner() {
		return owner;
	}
}
