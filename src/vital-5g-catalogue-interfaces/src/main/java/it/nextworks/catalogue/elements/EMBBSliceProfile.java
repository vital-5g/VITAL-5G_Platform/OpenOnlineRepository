package it.nextworks.catalogue.elements;

import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.Entity;

@Entity
public class EMBBSliceProfile extends SliceProfile{

    public it.nextworks.catalogue.elements.EMBBSliceProfileParams profileParams;

    public EMBBSliceProfile(){
        super(SliceProfileType.EMBB,null);

    }

    public EMBBSliceProfile(it.nextworks.catalogue.elements.EMBBSliceProfileParams profileParams, NetAppEndpoint netAppEndpoint) {
        super(SliceProfileType.EMBB, netAppEndpoint);
        this.profileParams = profileParams;
    }

    public EMBBSliceProfileParams getProfileParams() {
        return profileParams;
    }

    @Override
    public void isValid() throws MalformattedElementException{
        profileParams.isValid();
    }
}
