package it.nextworks.catalogue.elements;

import it.nextworks.catalogue.exceptions.MalformattedElementException;

public interface DescriptorInformationElement {

    void isValid() throws MalformattedElementException;
}
