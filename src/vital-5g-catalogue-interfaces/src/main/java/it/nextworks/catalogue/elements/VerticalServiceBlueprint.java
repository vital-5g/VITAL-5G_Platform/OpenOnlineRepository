package it.nextworks.catalogue.elements;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.*;

@Entity
public class VerticalServiceBlueprint implements DescriptorInformationElement {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
    @JoinColumn(name="vsb_info")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private VerticalServiceBlueprintInfo verticalServiceBlueprintInfo;

    private UUID verticalServiceBlueprintId;

    private String name;
    private String description;
    private String version;
    private String nsdPath;
    private Testbed testbed;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection
    private Map<String, String> configurableParameters = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection
    private Map<String, String> serviceParameters = new HashMap<>();
    private it.nextworks.catalogue.elements.AccessLevel accessLevel;


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection
    private List<String> requiredLicenses = new ArrayList<>();


    @ElementCollection
    private List<String> useCase = new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch= FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<it.nextworks.catalogue.elements.RequiredEquipment> requiredEquipments = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch= FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<it.nextworks.catalogue.elements.ApplicationMetric> applicationMetrics = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<it.nextworks.catalogue.elements.InfrastructureMetric> infrastructureMetrics = new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ServiceComponent> atomicComponents = new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ConnectivityService> connectivityServices= new ArrayList<>();



    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceBlueprint", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ServiceEndpoint> endPoints= new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<RuntimeAction> runtimeActions = new ArrayList<>();

    public VerticalServiceBlueprint() {
    }

    public VerticalServiceBlueprint(VerticalServiceBlueprintInfo verticalServiceBlueprintInfo,
                                    String name,
                                    String description,
                                    String version,
                                    String nsdPath,
                                    Testbed testbed,
                                    Map<String, String> configurableParameters,
                                    Map<String, String> serviceParameters,
                                    it.nextworks.catalogue.elements.AccessLevel accessLevel,
                                    List<String> useCase,
                                    List<it.nextworks.catalogue.elements.RequiredEquipment> requiredEquipments,
                                    List<it.nextworks.catalogue.elements.ApplicationMetric> applicationMetrics,
                                    List<it.nextworks.catalogue.elements.InfrastructureMetric> infrastructureMetrics,
                                    List<ServiceComponent> atomicComponents,
                                    List<ConnectivityService> connectivityServices,
                                    List<ServiceEndpoint> endPoints,
                                    List<RuntimeAction> runtimeActions) {

        this.verticalServiceBlueprintInfo = verticalServiceBlueprintInfo;
        if(verticalServiceBlueprintInfo!=null) this.verticalServiceBlueprintId = verticalServiceBlueprintInfo.getVerticalServiceBlueprintId();
        this.name = name;
        this.description = description;
        this.version = version;
        this.nsdPath = nsdPath;
        this.testbed = testbed;
        if(configurableParameters!=null) this.configurableParameters = configurableParameters;
        if(serviceParameters!=null) this.serviceParameters = serviceParameters;
        this.accessLevel = accessLevel;
        this.useCase = useCase;
        if(requiredEquipments!=null) this.requiredEquipments = requiredEquipments;
        if(applicationMetrics!=null) this.applicationMetrics = applicationMetrics;
        if(infrastructureMetrics!=null) this.infrastructureMetrics = infrastructureMetrics;
        if(atomicComponents!=null) this.atomicComponents = atomicComponents;
        if(connectivityServices!=null) this.connectivityServices= connectivityServices;
        if(endPoints!=null) this.endPoints=endPoints;
        if(runtimeActions!=null) this.runtimeActions=runtimeActions;
    }

    public VerticalServiceBlueprintInfo getVerticalServiceBlueprintInfo() {
        return verticalServiceBlueprintInfo;
    }

    public List<ConnectivityService> getConnectivityServices() {
        return connectivityServices;
    }

    public UUID getVerticalServiceBlueprintId() {
        return verticalServiceBlueprintId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getVersion() {
        return version;
    }

    public String getNsdPath() {
        return nsdPath;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public Map<String, String> getConfigurableParameters() {
        return configurableParameters;
    }

    public Map<String, String> getServiceParameters() {
        return serviceParameters;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public List<String> getUseCase() {
        return useCase;
    }

    public List<RequiredEquipment> getRequiredEquipments() {
        return requiredEquipments;
    }

    public List<it.nextworks.catalogue.elements.ApplicationMetric> getApplicationMetrics() {
        return applicationMetrics;
    }

    public List<it.nextworks.catalogue.elements.InfrastructureMetric> getInfrastructureMetrics() {
        return infrastructureMetrics;
    }

    public List<ServiceEndpoint> getEndPoints() {
        return endPoints;
    }

    public List<ServiceComponent> getAtomicComponents() {
        return atomicComponents;
    }

    public List<RuntimeAction> getRuntimeActions() {
        return runtimeActions;
    }

    public void setRuntimeActions(List<RuntimeAction> runtimeActions) {
        this.runtimeActions = runtimeActions;
    }


    public List<String> getRequiredLicenses() {
        return requiredLicenses;
    }

    public void setRequiredLicenses(List<String> requiredLicenses) {
        this.requiredLicenses = requiredLicenses;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(name==null) throw new MalformattedElementException("Vertical Service Blueprint without name");
        if(version==null) throw new MalformattedElementException("Vertical Service Blueprint without version");
        if(nsdPath==null) throw new MalformattedElementException("Vertical Service Blueprint without NSD  Path");


        if(accessLevel==null) throw new MalformattedElementException("Vertical Service Blueprint without access level");
        if(testbed==null) throw new MalformattedElementException("Vertical Service Blueprint without testbed");
        if(useCase==null) throw new MalformattedElementException("Vertical Service Blueprint without use case");
        for(ApplicationMetric am: applicationMetrics){
            am.isValid();
        }
        for(InfrastructureMetric im: infrastructureMetrics){
            im.isValid();
        }

        for(ServiceComponent component: atomicComponents){
            component.isValid();
        }

    }
}
