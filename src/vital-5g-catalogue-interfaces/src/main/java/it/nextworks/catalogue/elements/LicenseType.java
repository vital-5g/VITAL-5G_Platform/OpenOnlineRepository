package it.nextworks.catalogue.elements;

public enum LicenseType {
    APPLICATION_PROPRIETARY,
    INSTANCE_BASED,
    TIME_BASED,
    FLAT
}
