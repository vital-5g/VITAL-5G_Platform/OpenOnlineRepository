package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.Embeddable;

@Embeddable
public class URLLCSliceProfileParams {



    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Float availability;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double expDataRateUL;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double expDataRateDL;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double latency;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double jitter;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double userDensity;

    public URLLCSliceProfileParams() {
    }

    public URLLCSliceProfileParams(Float availability, Double expDataRateUL, Double expDataRateDL, Double latency, Double jitter, Double userDensity) {
        this.availability = availability;
        this.expDataRateUL = expDataRateUL;
        this.expDataRateDL=expDataRateDL;
        this.latency = latency;
        this.jitter = jitter;
        this.userDensity = userDensity;
    }

    public Double getExpDataRateDL() {
        return expDataRateDL;
    }

    public Float getAvailability() {
        return availability;
    }

    public Double getExpDataRateUL() {
        return expDataRateUL;
    }

    public Double getLatency() {
        return latency;
    }

    public Double getJitter() {
        return jitter;
    }

    public Double getUserDensity() {
        return userDensity;
    }


    public void isValid() throws MalformattedElementException{
        if(latency==null)
            throw new MalformattedElementException("URLLC slice profile without latency");

        if(expDataRateUL==null)
            throw new MalformattedElementException("URLLC slice profile without expDataRateUL");

        if(expDataRateDL==null)
            throw new MalformattedElementException("URLLC slice profile without expDataRateDL");

        if(jitter==null)
            throw new MalformattedElementException("URLLC slice profile without jitter");



    }
}
