package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.*;

@Entity
public class NetAppBlueprint  implements DescriptorInformationElement {


    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;


    @JsonIgnore
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)

    @OnDelete(action = OnDeleteAction.CASCADE)
    private NetAppPackageInfo netAppPackageInfo;

    private UUID netAppPackageId;
    private String name;
    private String description;
    private String version;
    private String vnfPackagePath;



    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch= FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<it.nextworks.catalogue.elements.ApplicationMetric> applicationMetrics = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<it.nextworks.catalogue.elements.InfrastructureMetric> infrastructureMetrics = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<NetAppComponent> atomicComponents = new ArrayList<>();

    private it.nextworks.catalogue.elements.NetAppType type;
    private it.nextworks.catalogue.elements.NetAppSpecLevel specLevel;
    private it.nextworks.catalogue.elements.AccessLevel accessLevel;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection
    private Map<String, String> configurableParameters = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection
    private Map<String, String> serviceParameters = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ProvidedInterfaceServiceSpec> providedInterfaceSpec = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<RequiredInterfaceServiceSpec> requiredInterfaceSpec = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch= FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<it.nextworks.catalogue.elements.RequiredEquipment> requiredEquipments = new ArrayList<>();

    private String useCase;

    private Testbed testbed;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<it.nextworks.catalogue.elements.SoftwareLicense> softwareLicenses= new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ConnectivityService> connectivityServices= new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "netAppBlueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<NetAppEndpoint> endPoints= new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch= FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<it.nextworks.catalogue.elements.TestCaseSpec> testCases = new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch= FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<FiveGServiceSpec> required5GCoreServices = new ArrayList<>();





    public NetAppBlueprint() {
    }

    public NetAppBlueprint(NetAppPackageInfo netAppPackageInfo,
                           String name,
                           String description,
                           String version,
                           String vnfPackagePath,
                           List<it.nextworks.catalogue.elements.ApplicationMetric> applicationMetrics,
                           List<it.nextworks.catalogue.elements.InfrastructureMetric> infrastructureMetrics,
                           List<NetAppComponent> atomicComponents,
                           it.nextworks.catalogue.elements.NetAppType type,
                           it.nextworks.catalogue.elements.NetAppSpecLevel specLevel,
                           it.nextworks.catalogue.elements.AccessLevel accessLevel,
                           Map<String, String> configurableParameters,
                           Map<String, String> serviceParameters,
                           List<ProvidedInterfaceServiceSpec> providedInterfaceSpec,
                           List<RequiredInterfaceServiceSpec> requiredInterfaceSpec,
                           List<it.nextworks.catalogue.elements.RequiredEquipment> requiredEquipments,
                           List<ConnectivityService> connectivityServices,
                           String useCase,
                           Testbed testbed,
                           List<it.nextworks.catalogue.elements.SoftwareLicense> softwareLicenses,
                           List<it.nextworks.catalogue.elements.TestCaseSpec> testCases,
                           List<NetAppEndpoint> endPoints,
                           List<FiveGServiceSpec> required5GCoreServices) {
        this.netAppPackageInfo = netAppPackageInfo;
        if(netAppPackageInfo!=null)
            this.netAppPackageId = netAppPackageInfo.getNetAppPackageId();
        this.name = name;
        this.description = description;
        this.version = version;
        this.vnfPackagePath = vnfPackagePath;
        if(applicationMetrics!=null) this.applicationMetrics = applicationMetrics;
        if(infrastructureMetrics!=null) this.infrastructureMetrics = infrastructureMetrics;
        if(atomicComponents!=null) this.atomicComponents = atomicComponents;
        if(connectivityServices!=null) this.connectivityServices= connectivityServices;
        this.type = type;
        this.specLevel = specLevel;
        this.accessLevel = accessLevel;
        if(configurableParameters!=null) this.configurableParameters = configurableParameters;
        if(serviceParameters!=null) this.serviceParameters = serviceParameters;
        if(providedInterfaceSpec!=null) this.providedInterfaceSpec = providedInterfaceSpec;
        if(requiredInterfaceSpec!=null) this.requiredInterfaceSpec = requiredInterfaceSpec;
        if(requiredEquipments!=null) this.requiredEquipments = requiredEquipments;
        this.useCase = useCase;
        this.testbed = testbed;
        if(softwareLicenses!=null) this.softwareLicenses = softwareLicenses;
        if(testCases!=null) this.testCases = testCases;
        if(endPoints!=null) this.endPoints= endPoints;
        if(required5GCoreServices!=null) this.required5GCoreServices=required5GCoreServices;
    }


    public NetAppBlueprint(NetAppPackageInfo netAppPackageInfo,
                           String name,
                           String description,
                           String version,
                           String vnfPackagePath,
                           List<it.nextworks.catalogue.elements.ApplicationMetric> applicationMetrics,
                           it.nextworks.catalogue.elements.NetAppType type,
                           it.nextworks.catalogue.elements.NetAppSpecLevel specLevel,
                           it.nextworks.catalogue.elements.AccessLevel accessLevel,
                           Map<String, String> configurableParameters,
                           Map<String, String> serviceParameters,
                           List<it.nextworks.catalogue.elements.RequiredEquipment> requiredEquipments,
                           String useCase,
                           Testbed testbed,
                           List<FiveGServiceSpec> required5GCoreServices) {

        this.netAppPackageInfo =netAppPackageInfo;
        if(netAppPackageInfo!=null)
            this.netAppPackageId = netAppPackageInfo.getNetAppPackageId();
        this.name = name;
        this.description = description;
        this.version = version;
        this.vnfPackagePath = vnfPackagePath;
        if(applicationMetrics!=null) this.applicationMetrics = applicationMetrics;
        if(infrastructureMetrics!=null) this.infrastructureMetrics = infrastructureMetrics;
        if(atomicComponents!=null) this.atomicComponents = atomicComponents;
        if(connectivityServices!=null) this.connectivityServices= connectivityServices;
        this.type = type;
        this.specLevel = specLevel;
        this.accessLevel = accessLevel;
        if(configurableParameters!=null) this.configurableParameters = configurableParameters;
        if(serviceParameters!=null) this.serviceParameters = serviceParameters;
        if(providedInterfaceSpec!=null) this.providedInterfaceSpec = providedInterfaceSpec;
        if(requiredInterfaceSpec!=null) this.requiredInterfaceSpec = requiredInterfaceSpec;
        if(requiredEquipments!=null) this.requiredEquipments = requiredEquipments;
        this.useCase = useCase;
        this.testbed = testbed;
        if(softwareLicenses!=null) this.softwareLicenses = softwareLicenses;
        if(testCases!=null) this.testCases = testCases;
        if(endPoints!=null) this.endPoints= endPoints;
        if(required5GCoreServices!=null) this.required5GCoreServices=required5GCoreServices;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(name==null) throw new MalformattedElementException("NetApp Blueprint without name");
        if(version==null) throw new MalformattedElementException("NetApp Blueprint without version");
        if(vnfPackagePath==null) throw new MalformattedElementException("NetApp Blueprint without VNF Package Path");
        if(type==null) throw new MalformattedElementException("NetApp Blueprint without type");
        if(specLevel==null) throw new MalformattedElementException("NetApp Blueprint without specialization level");
        if(accessLevel==null) throw new MalformattedElementException("NetApp Blueprint without access level");
        if(testbed==null) throw new MalformattedElementException("NetApp Blueprint without testbed");
        if(useCase==null) throw new MalformattedElementException("NetApp Blueprint without use case");
        if(endPoints==null ) throw new MalformattedElementException("NetApp Blueprint without endpoints");
        for(it.nextworks.catalogue.elements.ApplicationMetric am: applicationMetrics){
            am.isValid();
        }
        for(it.nextworks.catalogue.elements.InfrastructureMetric im: infrastructureMetrics){
            im.isValid();
        }
        for(NetAppComponent component: atomicComponents){
            component.isValid();
        }
        for(SoftwareLicense license: softwareLicenses){
            license.isValid();
        }
    }


    public List<FiveGServiceSpec> getRequired5GCoreServices() {
        return required5GCoreServices;
    }

    public List<ConnectivityService> getConnectivityServices() {
        return connectivityServices;
    }

    public NetAppPackageInfo getNetAppPackageInfo() {
        return netAppPackageInfo;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getVersion() {
        return version;
    }

    public String getVnfPackagePath() {
        return vnfPackagePath;
    }

    public List<ApplicationMetric> getApplicationMetrics() {
        return applicationMetrics;
    }

    public List<InfrastructureMetric> getInfrastructureMetrics() {
        return infrastructureMetrics;
    }

    public List<NetAppComponent> getAtomicComponents() {
        return atomicComponents;
    }

    public NetAppType getType() {
        return type;
    }

    public NetAppSpecLevel getSpecLevel() {
        return specLevel;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public Map<String, String> getConfigurableParameters() {
        return configurableParameters;
    }

    public Map<String, String> getServiceParameters() {
        return serviceParameters;
    }

    public List<ProvidedInterfaceServiceSpec> getProvidedInterfaceSpec() {
        return providedInterfaceSpec;
    }

    public List<RequiredInterfaceServiceSpec> getRequiredInterfaceSpec() {
        return requiredInterfaceSpec;
    }

    public List<RequiredEquipment> getRequiredEquipments() {
        return requiredEquipments;
    }

    public String getUseCase() {
        return useCase;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public List<SoftwareLicense> getSoftwareLicenses() {
        return softwareLicenses;
    }

    public List<TestCaseSpec> getTestCases() {
        return testCases;
    }

    public List<NetAppEndpoint> getEndPoints() {
        return endPoints;
    }

    public UUID getNetAppPackageId() {
        return netAppPackageId;
    }



}
