package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TestCaseSpec {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;


    private String testcaseId;
    private String name;
    private String testcasePath;


    public TestCaseSpec() {
    }

    public TestCaseSpec(String testcaseId, String name, String testcasePath) {
        this.testcaseId = testcaseId;
        this.name = name;
        this.testcasePath = testcasePath;
    }

    public String getTestcaseId() {
        return testcaseId;
    }

    public String getName() {
        return name;
    }

    public String getTestcasePath() {
        return testcasePath;
    }
}
