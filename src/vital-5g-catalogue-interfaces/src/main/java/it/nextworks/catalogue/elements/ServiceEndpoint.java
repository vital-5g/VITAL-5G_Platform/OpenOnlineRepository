package it.nextworks.catalogue.elements;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class ServiceEndpoint {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @ManyToOne
    private VerticalServiceBlueprint verticalServiceBlueprint;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "serviceEndpoint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<SliceProfile> sliceProfile= new ArrayList<>();

    private UUID netAppBlueprintId;


    private String serviceEndpointId;
    private String endpointId;

    public ServiceEndpoint() {
    }

    public ServiceEndpoint(VerticalServiceBlueprint verticalServiceBlueprint,
                           List<SliceProfile> sliceProfile,
                           UUID netAppBlueprintId,
                           String endpointId,
                           String serviceEndpointId) {
        this.verticalServiceBlueprint = verticalServiceBlueprint;
        this.sliceProfile = sliceProfile;
        this.netAppBlueprintId = netAppBlueprintId;
        this.endpointId = endpointId;
        this.serviceEndpointId=serviceEndpointId;
    }

    public VerticalServiceBlueprint getVerticalServiceBlueprint() {
        return verticalServiceBlueprint;
    }

    public List<SliceProfile> getSliceProfile() {
        return sliceProfile;
    }

    public UUID getNetAppBlueprintId() {
        return netAppBlueprintId;
    }

    public String getEndpointId() {
        return endpointId;
    }

    public String getServiceEndpointId() {
        return serviceEndpointId;
    }
}
