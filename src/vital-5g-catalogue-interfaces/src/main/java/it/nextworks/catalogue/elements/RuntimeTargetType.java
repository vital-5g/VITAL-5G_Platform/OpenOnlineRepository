package it.nextworks.catalogue.elements;

public enum RuntimeTargetType {
    NS_SCALE,
    SLICE_MODIFICATION
}
