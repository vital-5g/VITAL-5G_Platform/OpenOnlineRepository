package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.Embeddable;

@Embeddable
public class EMBBSliceProfileParams {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double areaTrafficCapUL;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double areaTrafficCapDL;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double expDataRateDL;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double expDataRateUL;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double userSpeed;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double userDensity;

    public EMBBSliceProfileParams() {
    }

    public EMBBSliceProfileParams(Double areaTrafficCapUL, Double areaTrafficCapDL, Double expDataRateDL, Double expDataRateUL, Double userSpeed, Double userDensity) {
        this.areaTrafficCapUL = areaTrafficCapUL;
        this.areaTrafficCapDL = areaTrafficCapDL;
        this.expDataRateDL = expDataRateDL;
        this.expDataRateUL = expDataRateUL;
        this.userSpeed = userSpeed;
        this.userDensity = userDensity;
    }


    public Double getAreaTrafficCapUL() {
        return areaTrafficCapUL;
    }

    public Double getAreaTrafficCapDL() {
        return areaTrafficCapDL;
    }

    public Double getExpDataRateDL() {
        return expDataRateDL;
    }

    public Double getExpDataRateUL() {
        return expDataRateUL;
    }

    public Double getUserSpeed() {
        return userSpeed;
    }

    public Double getUserDensity() {
        return userDensity;
    }


    public void isValid() throws MalformattedElementException{
        if(areaTrafficCapDL==null)
            throw new MalformattedElementException("EMBB slice profile without areaTrafficCapDL");

        if(areaTrafficCapUL==null)
            throw new MalformattedElementException("EMBB slice profile without areaTrafficCapUL");

        if(expDataRateDL==null)
            throw new MalformattedElementException("EMBB slice profile without expDataRateDL");

        if(expDataRateUL==null)
            throw new MalformattedElementException("EMBB slice profile without expDataRateUL");

        if(userSpeed==null)
            throw new MalformattedElementException("EMBB slice profile without userSpeed");


    }
}
