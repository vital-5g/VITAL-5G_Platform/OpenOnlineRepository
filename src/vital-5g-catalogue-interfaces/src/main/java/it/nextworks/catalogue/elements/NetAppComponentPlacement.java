package it.nextworks.catalogue.elements;

public enum NetAppComponentPlacement {

    EDGE,
    FAR_EDGE,
    CORE
}
