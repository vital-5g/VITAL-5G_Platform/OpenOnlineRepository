package it.nextworks.catalogue.elements;

public enum SliceProfileType {
    URLLC,
    EMBB,
    M_IoT,
    NONE

}
