package it.nextworks.catalogue.elements;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

@Entity
public class RuntimeDiagnosticAction extends RuntimeAction {

    private RuntimeDiactionticActionType diagnosticEvent;


    public RuntimeDiagnosticAction() {
        super(RuntimeActionTriggerType.DIAGNOSTIC_EVENT);
    }

    public RuntimeDiagnosticAction(VerticalServiceBlueprint verticalServiceBlueprint, String runtimeActionId, RuntimeTarget target, RuntimeDiactionticActionType diagnosticEvent) {
        super(verticalServiceBlueprint, runtimeActionId, RuntimeActionTriggerType.DIAGNOSTIC_EVENT, target);
        this.diagnosticEvent = diagnosticEvent;
    }

    public RuntimeDiactionticActionType getDiagnosticEvent() {
        return diagnosticEvent;
    }

    public void setDiagnosticEvent(RuntimeDiactionticActionType diagnosticEvent) {
        this.diagnosticEvent = diagnosticEvent;
    }
}
