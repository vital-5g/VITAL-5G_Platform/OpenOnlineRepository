package it.nextworks.catalogue.messages;

import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.elements.VsdNsdTranslationRule;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import java.util.ArrayList;
import java.util.List;

public class OnboardVSBRequest {

    private VerticalServiceBlueprint vsBlueprint;
    private List<VsdNsdTranslationRule> translationRules = new ArrayList<>();


    public OnboardVSBRequest(VerticalServiceBlueprint vsBlueprint, List<VsdNsdTranslationRule> translationRules) {
        this.vsBlueprint = vsBlueprint;
        this.translationRules = translationRules;
    }

    public OnboardVSBRequest() {
    }

    public VerticalServiceBlueprint getVsBlueprint() {
        return vsBlueprint;
    }

    public List<VsdNsdTranslationRule> getTranslationRules() {
        return translationRules;
    }

    public void isValid() throws MalformattedElementException{
        if(vsBlueprint==null) throw new MalformattedElementException("VSB Onboarding request vertical service blueprint");
    }
}
