/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.catalogue.elements;


import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;


@Entity
public class InfrastructureMetric implements DescriptorInformationElement {


	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;



	@JsonIgnore
	@ManyToOne
	private NetAppBlueprint netAppBlueprint;
	@JsonIgnore
	@ManyToOne
	private VerticalServiceBlueprint verticalServiceBlueprint;

	private InfrastructureMetricType type;

	private String metricId;
    private String name;
    private MetricCollectionType metricCollectionType;
    private String unit;
    private String interval;
    private MetricGraphType metricGraphType;

	private String vnfReference;
	@ElementCollection
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	@JoinColumn(name = "im_components")
	@OnDelete(action= OnDeleteAction.CASCADE)
	private List<String> componentIds = new ArrayList<String>();



	public InfrastructureMetric() {
		// JPA only
	}
	
	public InfrastructureMetric(
			NetAppBlueprint netAppBlueprint,
			VerticalServiceBlueprint verticalServiceBlueprint,
			String metricId,
			String name,
			MetricCollectionType metricCollectionType, 
			String unit, 
			String interval,
			InfrastructureMetricType type,
			MetricGraphType metricGraphType,
								List<String> componentIds,
			String vnfReference) {
		this.metricId = metricId;
        this.name = name;
	    this.metricCollectionType = metricCollectionType;
        this.unit = unit;
        this.interval = interval;
        this.type = type;
        this.metricGraphType = metricGraphType;

		this.vnfReference = vnfReference;
		if(componentIds!=null) this.componentIds= componentIds;
		this.netAppBlueprint= netAppBlueprint;
		this.verticalServiceBlueprint=verticalServiceBlueprint;
    }

	public String getVnfReference() {
		return vnfReference;
	}

	public VerticalServiceBlueprint getVerticalServiceBlueprint() {
		return verticalServiceBlueprint;
	}

	public NetAppBlueprint getNetAppBlueprint() {
		return netAppBlueprint;
	}

	public List<String> getComponentIds() {
		return componentIds;
	}

	/**
	 * @return the metricId
	 */
	public String getMetricId() {
		return metricId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the metricCollectionType
	 */
	public MetricCollectionType getMetricCollectionType() {
		return metricCollectionType;
	}

	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @return the interval
	 */
	public String getInterval() {
		return interval;
	}

	/**
	 * @return the iMetricType
	 */
	public InfrastructureMetricType getType() {
		return type;
	}

    public MetricGraphType getMetricGraphType() {
        return metricGraphType;
    }

    @Override
    public void isValid() throws MalformattedElementException {

        if(metricId == null || metricId.isEmpty())
            throw new MalformattedElementException("Metric without metricId");
        if(name == null || name.isEmpty())
            throw new MalformattedElementException("Metric without name");
        if(unit == null || unit.isEmpty())
            throw new MalformattedElementException("Metric without unit");
        if(metricCollectionType == null)
            throw new MalformattedElementException("Metric without MetricCollectionType");
        if(metricGraphType == null)
            throw new MalformattedElementException("Metric without MetricGraphType");
		if((isComputeMetric())&&(vnfReference==null||vnfReference.isEmpty())){
			throw new MalformattedElementException("Compute related metric without VNF reference");
		}

    }


	public boolean isComputeMetric(){
		if((type.equals(InfrastructureMetricType.CPU_USAGE)||type.equals(InfrastructureMetricType.CPU_LOAD)
				||type.equals(InfrastructureMetricType.RAM_USAGE)
				|| type.equals(InfrastructureMetricType.MEMORY_USAGE)
				|| type.equals(InfrastructureMetricType.DISK_USAGE)
				|| type.equals(InfrastructureMetricType.GPU_LOAD))){
			return true;
		}else return false;
	}


	public boolean isNetworkMetric(){
		return !isComputeMetric();
	}

}
