package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class NetAppPackageInfo {

    @Id
    @GeneratedValue
    private UUID netAppPackageId;

    private String name;
    private String version;
    private Testbed testbed;

    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<UUID> referencedVsbs = new ArrayList<>();
    
    
    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<UUID> netappLicenses = new ArrayList<>();

    private String owner;

    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "netAppPackageInfo", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private it.nextworks.catalogue.elements.NetAppBlueprint netAppBlueprint;

    public NetAppPackageInfo() {
    }

    public NetAppPackageInfo(String name, String version, NetAppBlueprint netAppBlueprint, Testbed testbed, String owner) {
        this.name = name;
        this.version = version;
        this.netAppBlueprint = netAppBlueprint;
        this.testbed = testbed;
        this.owner = owner;
    }


    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public UUID getNetAppPackageId() {
        return netAppPackageId;
    }

    public NetAppBlueprint getNetAppBlueprint() {
        return netAppBlueprint;
    }


    public Testbed getTestbed() {
        return testbed;
    }

    public String getOwner() {
        return owner;
    }

    public void setNetAppBlueprint(NetAppBlueprint netAppBlueprint) {
        this.netAppBlueprint = netAppBlueprint;
    }


    public void addReferencedVsb(UUID vsbId){
        this.referencedVsbs.add(vsbId);
    }

    public List<UUID> getReferencedVsbs() {
        return referencedVsbs;
    }

    public void removeReferencedVsb(UUID vsbId){
        this.referencedVsbs.remove(vsbId);
    }

    public List<UUID> getNetappLicenses() {
        return netappLicenses;
    }

    public void addLicense(UUID licenseId){
        netappLicenses.add(licenseId);
    }
}
