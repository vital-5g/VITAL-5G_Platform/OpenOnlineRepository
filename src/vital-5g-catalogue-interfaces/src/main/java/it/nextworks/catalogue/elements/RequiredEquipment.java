package it.nextworks.catalogue.elements;

import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.Embeddable;

@Embeddable
public class RequiredEquipment implements DescriptorInformationElement{



    private String requiredEquipmentId;
    private EquipmentType type;
    private String protocol;
    private EquipmentAccessLevel accessLevel;

    private String location;
    private String description;
    private String interfaceProtocol;
    private boolean mandatory;
    private AccessLevel exposedAccessLevel;

    public RequiredEquipment() {
    }

    public RequiredEquipment(String requiredEquipmentId, EquipmentType type, String protocol, EquipmentAccessLevel accessLevel, String location, String description, String interfaceProtocol, boolean mandatory, AccessLevel exposedAccessLevel) {
        this.requiredEquipmentId = requiredEquipmentId;
        this.type = type;
        this.protocol = protocol;
        this.accessLevel = accessLevel;
        this.location = location;
        this.description = description;
        this.interfaceProtocol = interfaceProtocol;
        this.mandatory = mandatory;
        this.exposedAccessLevel = exposedAccessLevel;
    }

    public String getRequiredEquipmentId() {
        return requiredEquipmentId;
    }

    public EquipmentType getType() {
        return type;
    }

    public String getProtocol() {
        return protocol;
    }

    public EquipmentAccessLevel getAccessLevel() {
        return accessLevel;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public String getInterfaceProtocol() {
        return interfaceProtocol;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public AccessLevel getExposedAccessLevel() {
        return exposedAccessLevel;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(requiredEquipmentId==null) throw new MalformattedElementException("Required Equipment without Id");
        if(type==null) throw new MalformattedElementException("Required Equipment without type");
        if(accessLevel==null) throw new MalformattedElementException("Required Equipment without access level");
        if(exposedAccessLevel==null) throw new MalformattedElementException("Required Equipment without exposed access level");

    }
}
