package it.nextworks.catalogue.elements;

public enum InterfaceCommType {

    REQUEST_RESPONSE,
    SUBSCRIBE_NOTIFY
}
