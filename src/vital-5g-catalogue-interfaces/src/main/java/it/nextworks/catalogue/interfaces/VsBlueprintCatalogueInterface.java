package it.nextworks.catalogue.interfaces;

import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.elements.VerticalServiceBlueprintInfo;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.messages.OnboardVSBRequest;

import java.io.File;
import java.util.List;
import java.util.UUID;

public interface VsBlueprintCatalogueInterface {

     UUID onboardVsBlueprint(OnboardVSBRequest vsb, File nsd) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException;


     VerticalServiceBlueprint getVerticalServiceBlueprint(UUID vsbId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException;
     List<VerticalServiceBlueprintInfo> queryVerticalServiceBlueprint(Testbed testbed, AccessLevel accessLevel) throws MalformattedElementException;
     List<VerticalServiceBlueprintInfo> getAllVerticalServiceBlueprints() throws FailedOperationException;
     void deleteVsBlueprint(UUID vsbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException;
}
