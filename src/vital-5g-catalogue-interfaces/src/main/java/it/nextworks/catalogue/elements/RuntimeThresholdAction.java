package it.nextworks.catalogue.elements;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

@Entity
public class RuntimeThresholdAction extends RuntimeAction{
    private String metricId;

    private String metricValue;

    private RuntimeThresholdOperator logicOperator;


    public RuntimeThresholdAction() {
        super(RuntimeActionTriggerType.THRESHOLD);
    }

    public RuntimeThresholdAction(VerticalServiceBlueprint verticalServiceBlueprint, String runtimeActionId, RuntimeTarget target, String metricId, String metricValue, RuntimeThresholdOperator logicOperator) {
        super(verticalServiceBlueprint, runtimeActionId, RuntimeActionTriggerType.THRESHOLD, target);
        this.metricId = metricId;
        this.metricValue = metricValue;
        this.logicOperator = logicOperator;
    }

    public String getMetricId() {
        return metricId;
    }

    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }

    public String getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(String metricValue) {
        this.metricValue = metricValue;
    }

    public RuntimeThresholdOperator getLogicOperator() {
        return logicOperator;
    }

    public void setLogicOperator(RuntimeThresholdOperator logicOperator) {
        this.logicOperator = logicOperator;
    }
}
