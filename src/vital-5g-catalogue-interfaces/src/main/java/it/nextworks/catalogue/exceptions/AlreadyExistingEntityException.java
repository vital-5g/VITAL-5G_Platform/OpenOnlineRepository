package it.nextworks.catalogue.exceptions;

public class AlreadyExistingEntityException extends Exception {

    public AlreadyExistingEntityException(String s){
        super(s);
    }
}
