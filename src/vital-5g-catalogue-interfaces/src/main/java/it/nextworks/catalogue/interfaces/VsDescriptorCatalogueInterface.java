/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.interfaces;

import it.nextworks.catalogue.elements.VsDescriptor;
import it.nextworks.catalogue.exceptions.*;

import java.util.List;
import java.util.UUID;


public interface VsDescriptorCatalogueInterface {

	/**
	 * Method to create a new VSD
	 * 
	 * @param request
	 * @return

	 * @throws MalformattedElementException
	 * @throws AlreadyExistingEntityException
	 * @throws FailedOperationException
	 */
	 UUID onBoardVsDescriptor(VsDescriptor request)
             throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, UnAuthorizedRequestException;
	
	/**
	 * Method to request info about an existing VSD
	 * 

	 * @return

	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	 List<VsDescriptor> queryVsDescriptor()
			throws  MalformattedElementException,  FailedOperationException;


	VsDescriptor getVsd(UUID vsdId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException;

	/**
	 * Method to remove a VSD
	 * 
	 * @param vsDescriptorId
	 * @param force the deletion of the descriptor

	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	 void deleteVsDescriptor(UUID vsDescriptorId, boolean force)
			 throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException;


	void useVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException;

	void releaseVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException;
}
