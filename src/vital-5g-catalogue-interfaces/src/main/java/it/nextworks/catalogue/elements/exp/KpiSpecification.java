package it.nextworks.catalogue.elements.exp;

import javax.persistence.Embeddable;

@Embeddable
public class KpiSpecification {
    private String kpiId;

    private it.nextworks.catalogue.elements.exp.KpiThreshold threshold;

    public KpiSpecification(String kpiId, it.nextworks.catalogue.elements.exp.KpiThreshold threshold) {
        this.kpiId = kpiId;
        this.threshold = threshold;
    }

    public KpiSpecification() {
    }

    public String getKpiId() {
        return kpiId;
    }

    public KpiThreshold getThreshold() {
        return threshold;
    }
}
