package it.nextworks.catalogue.elements;

public enum NetAppSpecLevel {
    VERTICAL_AGNOSTIC,
    VERTICAL_SPECIFIC
}
