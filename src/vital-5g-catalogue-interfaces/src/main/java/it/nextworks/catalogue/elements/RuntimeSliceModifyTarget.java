package it.nextworks.catalogue.elements;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

@Entity
public class RuntimeSliceModifyTarget extends RuntimeTarget{

    @Basic
    private String sliceProfile;


    public RuntimeSliceModifyTarget( String sliceProfile) {
        super(RuntimeTargetType.SLICE_MODIFICATION);
        this.sliceProfile = sliceProfile;
    }

    public String getSliceProfile() {
        return sliceProfile;
    }

    public void setSliceProfile(String sliceProfile) {
        this.sliceProfile = sliceProfile;
    }
}
