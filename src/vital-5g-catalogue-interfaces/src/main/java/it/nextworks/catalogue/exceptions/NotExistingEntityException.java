package it.nextworks.catalogue.exceptions;

public class NotExistingEntityException extends Exception {
    public NotExistingEntityException(String s) {
        super(s);
    }
}
