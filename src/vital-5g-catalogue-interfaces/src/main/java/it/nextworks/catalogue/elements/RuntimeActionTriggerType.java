package it.nextworks.catalogue.elements;

public enum RuntimeActionTriggerType {
    THRESHOLD,
    DIAGNOSTIC_EVENT
}
