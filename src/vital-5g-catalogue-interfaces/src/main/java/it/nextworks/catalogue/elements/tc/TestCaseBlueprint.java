package it.nextworks.catalogue.elements.tc;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class TestCaseBlueprint  implements DescriptorInformationElement {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    private UUID testCaseBlueprintId;

    private String version;
    private String name;
    private String useCase;
    private AccessLevel accessLevel;


    @JsonIgnore
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
    @JoinColumn(name="tcb_info")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TestCaseBlueprintInfo testCaseBlueprintInfo;

    @AttributeOverrides({
            @AttributeOverride(name = "nfvoActionId", column = @Column(name = "execution_nfvo_action_id")),
            @AttributeOverride(name = "inputParameters", column = @Column(name = "execution_input_parameters")),
            @AttributeOverride(name = "nfvoReferenceId", column = @Column(name = "execution_nfvo_reference_id"))

    })
    private NfvoLcmAction executionAction;

    @AttributeOverrides({
            @AttributeOverride(name = "nfvoActionId", column = @Column(name = "configuration_nfvo_action_id")),
            @AttributeOverride(name = "inputParameters", column = @Column(name = "configuration_input_parameters")),
            @AttributeOverride(name = "nfvoReferenceId", column = @Column(name = "configuration_nfvo_reference_id"))

    })
    private NfvoLcmAction configurationAction;


    @AttributeOverrides({
            @AttributeOverride(name = "nfvoActionId", column = @Column(name = "reset_nfvo_action_id")),
            @AttributeOverride(name = "inputParameters", column = @Column(name = "reset_input_parameters")),
            @AttributeOverride(name = "nfvoReferenceId", column = @Column(name = "reset_nfvo_reference_id"))

    })
    private NfvoLcmAction resetAction;

    public TestCaseBlueprint() {
    }

    public TestCaseBlueprint(TestCaseBlueprintInfo info,
                             String version,
                             String name,
                             NfvoLcmAction executionAction,
                             NfvoLcmAction configurationAction,
                             NfvoLcmAction resetAction,
                             String useCase,
                             AccessLevel accessLevel
    ) {
        if(info!=null) testCaseBlueprintId=info.getTestCaseBlueprintId();
        this.testCaseBlueprintInfo=info;
        this.version = version;
        this.name = name;
        this.executionAction = executionAction;
        this.configurationAction = configurationAction;
        this.resetAction = resetAction;
        this.useCase=useCase;
        this.accessLevel=accessLevel;
    }

    public UUID getTestCaseBlueprintId() {
        return testCaseBlueprintId;
    }

    public String getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public NfvoLcmAction getExecutionAction() {
        return executionAction;
    }

    public NfvoLcmAction getConfigurationAction() {
        return configurationAction;
    }

    public NfvoLcmAction getResetAction() {
        return resetAction;
    }

    public TestCaseBlueprintInfo getTestCaseBlueprintInfo() {
        return testCaseBlueprintInfo;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        //if(configurationAction==null)
        //    throw new MalformattedElementException("TestCaseBlueprint without configuration action");
        //if(executionAction==null)
        //    throw new MalformattedElementException("TestCaseBlueprint without execution action");

        //if(resetAction==null)
        //    throw new MalformattedElementException("TestCaseBlueprint without reset action");

        if(name==null)
            throw new MalformattedElementException("TestCaseBlueprint without name");

        if(version==null)
            throw new MalformattedElementException("TestCaseBlueprint without version action");
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public String getUseCase() {
        return useCase;
    }
}
