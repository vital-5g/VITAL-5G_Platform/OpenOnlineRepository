package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "runtimeTargetType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RuntimeNSScaleTarget.class, name = "NS_SCALE"),
        @JsonSubTypes.Type(value = RuntimeSliceModifyTarget.class, name = "SLICE_MODIFICATION"),

})
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class RuntimeTarget {

    @Id
    @GeneratedValue
    @JsonIgnore
    private long id;

    @Basic
    private RuntimeTargetType runtimeTargetType;


    public RuntimeTarget() {
    }

    public RuntimeTarget(RuntimeTargetType runtimeTargetTypeType) {
        this.runtimeTargetType = runtimeTargetTypeType;
    }

    public RuntimeTargetType getRuntimeTargetType() {
        return runtimeTargetType;
    }

    public void setRuntimeTargetType(RuntimeTargetType runtimeTargetType) {
        this.runtimeTargetType = runtimeTargetType;
    }
}
