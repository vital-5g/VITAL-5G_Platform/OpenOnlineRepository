package it.nextworks.catalogue.elements.tc;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class TestCaseBlueprintInfo {

    @Id
    @GeneratedValue
    private UUID testCaseBlueprintId;
    private String version;
    private String name;
    private String owner;



    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "testCaseBlueprintInfo", cascade= CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TestCaseBlueprint tcBlueprint;

    public TestCaseBlueprintInfo() {
    }

    public TestCaseBlueprintInfo(String version, String name, String owner) {
        this.version = version;
        this.name = name;
        this.owner = owner;
    }

    public UUID getTestCaseBlueprintId() {
        return testCaseBlueprintId;
    }

    public String getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public TestCaseBlueprint getTcBlueprint() {
        return tcBlueprint;
    }

    public void setTcBlueprint(TestCaseBlueprint tcBlueprint) {
        this.tcBlueprint = tcBlueprint;
    }

    public String getOwner() {
        return owner;
    }
}
