/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class VerticalServiceConnectivityService implements DescriptorInformationElement {

	@Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

	@JsonIgnore
	@ManyToOne
	private it.nextworks.catalogue.elements.VerticalServiceBlueprint verticalServiceBlueprint;


	private String connectivityServiceId;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<String> endPointIds = new ArrayList<>();

	private boolean external;


	private boolean management;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<String> connectivityProperties = new ArrayList<>();

	public VerticalServiceConnectivityService() {
		// JPA only
	}





	/**
	 * Constructor
	 *
	 * @param verticalServiceBlueprint Blueprint this link belongs to
	 * @Param connectivityServiceId the id of this connectivity service
	 * @param endPointIds list of end points attached to the link
	 * @param external if the link is used to interconnect to an external network
	 * @param connectivityProperties e.g. QoS, protection, restoration
	 * @param name the name of the correspondent virtual link on the NSD
	 * @param management true if the link corresponds to the management network
	 */
	public VerticalServiceConnectivityService(it.nextworks.catalogue.elements.VerticalServiceBlueprint verticalServiceBlueprint,
                                              String connectivityServiceId,
                                              List<String> endPointIds,
                                              boolean external,
                                              List<String> connectivityProperties,
                                              String name,
                                              boolean management) {
		this.verticalServiceBlueprint = verticalServiceBlueprint;
		this.connectivityServiceId =connectivityServiceId;
		if (endPointIds != null) this.endPointIds = endPointIds;
		this.external = external;
		if (connectivityProperties != null) this.connectivityProperties = connectivityProperties;
		this.name = name;
		this.management=management;
	}

	public String getConnectivityServiceId() {
		return connectivityServiceId;
	}

	/**
	 * @return the NetApp blueprint
	 */
	public VerticalServiceBlueprint getVerticalServiceBlueprint() {
		return verticalServiceBlueprint;
	}

	/**
	 * @return the endPointIds
	 */
	public List<String> getEndPointIds() {
		return endPointIds;
	}

	/**
	 * @return the external
	 */
	public boolean isExternal() {
		return external;
	}

	/**
	 * @return the connectivityProperties
	 */
	public List<String> getConnectivityProperties() {
		return connectivityProperties;
	}

	/**
	 * @return the management
	 */

	public boolean isManagement() {
		return management;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if(connectivityServiceId==null) throw new MalformattedElementException("Vertical Service connectivity service without ID");
		if (endPointIds == null || endPointIds.isEmpty()){
			throw new MalformattedElementException("Vertical Service connectivity service without end points");
		}
		if (name == null || name.isEmpty()){
			throw new MalformattedElementException("Vertical Service connectivity service link without name");
		}
	}

}
