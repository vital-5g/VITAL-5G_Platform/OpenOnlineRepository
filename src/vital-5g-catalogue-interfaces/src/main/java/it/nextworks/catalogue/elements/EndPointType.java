package it.nextworks.catalogue.elements;

public enum EndPointType {
    INTERNAL,
    EXTERNAL
}
