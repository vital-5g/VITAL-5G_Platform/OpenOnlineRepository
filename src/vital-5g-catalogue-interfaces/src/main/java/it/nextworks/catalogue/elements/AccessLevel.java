package it.nextworks.catalogue.elements;

public enum AccessLevel {

    PRIVATE,
    RESTRICTED,
    PUBLIC
}
