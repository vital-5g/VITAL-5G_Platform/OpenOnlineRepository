package it.nextworks.catalogue.elements.exp;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

@Entity
public class ExpDescriptorInfo implements DescriptorInformationElement {

    @Id
    @GeneratedValue
    private UUID expDescriptorId;

    private String name;
    private String version;


    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<UUID> activeExperimentIds= new ArrayList<>();


    private UUID expBlueprintId;
    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "expDescriptorInfo", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ExpDescriptor expDescriptor;


    public List<UUID> getActiveExperimentIds() {
        return activeExperimentIds;
    }

    public void setActiveExperimentIds(List<UUID> activeExperimentIds) {
        this.activeExperimentIds = activeExperimentIds;
    }

    public ExpDescriptorInfo(){

    }


    public ExpDescriptorInfo(String name, String version, UUID expBlueprintId){

        this.name=name;
        this.version = version;
        this.expBlueprintId=expBlueprintId;

    }

    public ExpDescriptor getExpDescriptor() {
        return expDescriptor;
    }

    public void setExpDescriptor(ExpDescriptor expDescriptor) {
        this.expDescriptor = expDescriptor;
    }

    public UUID getExpDescriptorId() {
        return expDescriptorId;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public UUID getExpBlueprintId() {
        return expBlueprintId;
    }

    @Override
    public void isValid() throws MalformattedElementException {

        if (version == null) throw new MalformattedElementException("EXPD info without EXPD expDescriptorVersion");
        if (name == null) throw new MalformattedElementException("EXPD info without ExpD name");
    }
}
