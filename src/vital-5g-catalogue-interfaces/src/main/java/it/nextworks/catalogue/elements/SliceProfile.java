package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.*;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "sliceProfileType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EMBBSliceProfile.class, name = "EMBB"),
        @JsonSubTypes.Type(value = URLLCSliceProfile.class, name = "URLLC"),
        @JsonSubTypes.Type(value = MIOTSliceProfile.class, name = "MIOTSliceProfile"),
        @JsonSubTypes.Type(value = SliceProfile.class, name = "NONE"),
})

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class SliceProfile {

    @Id
    @GeneratedValue
    @JsonIgnore
    private long sliceProfileId;

    private it.nextworks.catalogue.elements.SliceProfileType sliceProfileType;

    @JsonIgnore
    @ManyToOne
    private NetAppEndpoint netAppEndpoint;

    @JsonIgnore
    @ManyToOne
    private ServiceEndpoint serviceEndpoint;

    public NetAppEndpoint getNetAppEndpoint() {
        return netAppEndpoint;
    }

    public SliceProfile(){

    }
    public SliceProfile(it.nextworks.catalogue.elements.SliceProfileType type, NetAppEndpoint netAppEndpoint){
        this.sliceProfileType = type;
        this.netAppEndpoint=netAppEndpoint;
    }


    public void setNetAppEndpoint(NetAppEndpoint netAppEndpoint) {
        this.netAppEndpoint = netAppEndpoint;
    }

    public it.nextworks.catalogue.elements.SliceProfileType getSliceProfileType() {
        return sliceProfileType;
    }

    public void setSliceProfileType(SliceProfileType sliceProfileType) {
        this.sliceProfileType = sliceProfileType;
    }

    public long getSliceProfileId() {
        return sliceProfileId;
    }

    public void setSliceProfileId(long sliceProfileId) {
        this.sliceProfileId = sliceProfileId;
    }

    public void isValid() throws MalformattedElementException{

    }
}
