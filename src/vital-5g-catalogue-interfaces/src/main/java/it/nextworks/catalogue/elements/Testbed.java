package it.nextworks.catalogue.elements;

public enum Testbed {
    ATHENS,
    ANTWERP,
    DANUBE,
    GALATI
}
