package it.nextworks.catalogue.elements.translator;

public class NfvNsInstantiationInfo {

    private String nstId;
    private String nsdId;
    private String nsdVersion;
    private String nsFlavourId;
    private String nsInstantiationLevelId;

    public NfvNsInstantiationInfo(String nstId, String nsdId, String nsdVersion, String nsFlavourId, String nsInstantiationLevelId) {
        this.nstId = nstId;
        this.nsdId = nsdId;
        this.nsdVersion = nsdVersion;
        this.nsFlavourId = nsFlavourId;
        this.nsInstantiationLevelId = nsInstantiationLevelId;
    }

    public NfvNsInstantiationInfo() {
    }

    public String getNstId() {
        return nstId;
    }

    public String getNsdId() {
        return nsdId;
    }

    public String getNsdVersion() {
        return nsdVersion;
    }

    public String getNsFlavourId() {
        return nsFlavourId;
    }

    public String getNsInstantiationLevelId() {
        return nsInstantiationLevelId;
    }
}
