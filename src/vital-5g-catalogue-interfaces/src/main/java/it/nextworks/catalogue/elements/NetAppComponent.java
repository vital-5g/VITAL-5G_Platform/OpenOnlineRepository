/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.CascadeType;


@Entity
public class NetAppComponent implements DescriptorInformationElement {

	@Id
    @GeneratedValue
    @JsonIgnore
    private Long id;
	
	@JsonIgnore
	@ManyToOne
	private it.nextworks.catalogue.elements.NetAppBlueprint netAppBlueprint;
	//private VsBlueprint vsb;
	
	private String componentId;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int minInstances;


	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int maxInstances;



	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	@JoinColumn(name = "component_endpoints")
	@OnDelete(action= OnDeleteAction.CASCADE)
	private List<String> endPointsIds = new ArrayList<>();
	


	private it.nextworks.catalogue.elements.NetAppComponentPlacement placement;

	public NetAppComponent() {
		// JPA only
	}
	
	/**
	 * Constructor
	 * 
	 * @param netAppBlueprint this component belongs to
	 * @param componentId ID of the atomic component
	 * @param minInstances minimum number of component instances
	 * @param maxInstances maximum number of component instances
	 * @param endPointsIds IDs of the connection end points of the applications
	 *  @param placement high-level placement indications for the component, used for the VNFs for the moment
	 */
	public NetAppComponent(it.nextworks.catalogue.elements.NetAppBlueprint netAppBlueprint,
                           String componentId,
                           int minInstances,
                           int maxInstances,
                           List<String> endPointsIds,
                           it.nextworks.catalogue.elements.NetAppComponentPlacement placement

						   ) {
		this.netAppBlueprint = netAppBlueprint;
		this.componentId = componentId;
		this.minInstances = minInstances;
		this.maxInstances= maxInstances;


		if (endPointsIds != null) this.endPointsIds = endPointsIds;


		this.placement = placement;



	}

	public NetAppBlueprint getNetAppBlueprint() {
		return netAppBlueprint;
	}

	public int getMinInstances() {
		return minInstances;
	}

	public int getMaxInstances() {
		return maxInstances;
	}

	/**
	 * @return the componentId
	 */
	public String getComponentId() {
		return componentId;
	}


	/**
	 * @return the endPointsIds
	 */
	public List<String> getEndPointsIds() {
		return endPointsIds;
	}


	@Override
	public void isValid() throws MalformattedElementException {
		if (componentId == null) throw new MalformattedElementException("VSB atomic component without ID.");

	}

	public NetAppComponentPlacement getPlacement() {
		return placement;
	}


}
