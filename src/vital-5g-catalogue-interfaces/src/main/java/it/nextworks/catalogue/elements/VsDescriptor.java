/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements;

import java.util.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;

import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


@Entity
public class VsDescriptor implements DescriptorInformationElement {

	
	@Id
    @GeneratedValue
	@JsonIgnore
	private Long id;
    private UUID vsDescriptorId;
	private String name;
	private String version;
	private UUID vsBlueprintId;
	private AccessLevel accessLevel;

	@JsonIgnore
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
	@JoinColumn(name="vsd_info")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private VsDescriptorInfo vsDescriptorInfo;





	//Key: parameter ID as in the blueprint; value: desired value
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private Map<String, String> qosParameters = new HashMap<String, String>();





	@JsonIgnore
	private boolean isPublic;
	
	@JsonIgnore
	private String tenantId;
	

	



	public VsDescriptor() {	}
	
	

	/**
	 * @param name
	 * @param version
	 * @param vsBlueprintId

	 * @param qosParameters

	 * @param tenantId
	 * 
	 */
	public VsDescriptor(
						VsDescriptorInfo vsDescriptorInfo,
						String name,
						String version,
						UUID vsBlueprintId,
						Map<String, String> qosParameters,
						AccessLevel accessLevel,
						String tenantId) {
		this.name = name;
		this.version = version;
		this.vsBlueprintId = vsBlueprintId;
		this.accessLevel=accessLevel;
		this.qosParameters = qosParameters;
		this.vsDescriptorInfo= vsDescriptorInfo;
		this.vsDescriptorId=vsDescriptorInfo.getVsDescriptorId();


	}

	public VsDescriptorInfo getVsDescriptorInfo() {
		return vsDescriptorInfo;
	}

	/**
	 * @return the vsDescriptorId
	 */
	public UUID getVsDescriptorId() {
		return vsDescriptorId;
	}
	
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}



	/**
	 * @return the vsBlueprintId
	 */
	public UUID getVsBlueprintId() {
		return vsBlueprintId;
	}

	/**
	 * @param vsBlueprintId the vsBlueprintId to set
	 */
	public void setVsBlueprintId(UUID vsBlueprintId) {
		this.vsBlueprintId = vsBlueprintId;
	}





	/**
	 * @return the qosParameters
	 */
	public Map<String, String> getQosParameters() {
		return qosParameters;
	}


	public AccessLevel getAccessLevel() {
		return accessLevel;
	}

	/**
	 * @return the tenantId
	 */
	@JsonIgnore
	public String getTenantId() {
		return tenantId;
	}



	/**
	 * @return the isPublic
	 */
	@JsonIgnore
	public boolean isPublic() {
		return isPublic;
	}



	/**
	 * @param isPublic the isPublic to set
	 */
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}




	




	@Override
	public void isValid() throws MalformattedElementException {
		if (name == null) throw new MalformattedElementException("VSD without name");
		if (version == null) throw new MalformattedElementException("VSD without version");
		if (vsBlueprintId == null) throw new MalformattedElementException("VSD without VS blueprint ID");

	}

}
