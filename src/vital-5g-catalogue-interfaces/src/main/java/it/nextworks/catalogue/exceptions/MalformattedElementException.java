package it.nextworks.catalogue.exceptions;

public class MalformattedElementException extends Exception{

    public MalformattedElementException(String message){
        super(message);
    }
}
