package it.nextworks.catalogue.elements;

public enum InterfaceDataFormat {

    JSON,
    YAML,
    PLAIN_TEXT,
    XML,
    OTHER
}
