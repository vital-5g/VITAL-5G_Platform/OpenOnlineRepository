package it.nextworks.catalogue.elements;

public enum MetricGraphType {
    LINE,
    PIE,
    COUNTER,
    GAUGE
}
