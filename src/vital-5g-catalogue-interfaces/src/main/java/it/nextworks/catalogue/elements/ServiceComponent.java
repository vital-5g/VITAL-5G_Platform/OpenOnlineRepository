/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Entity
public class ServiceComponent implements DescriptorInformationElement {


	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;

	@JsonIgnore
	@ManyToOne
	private it.nextworks.catalogue.elements.VerticalServiceBlueprint verticalServiceBlueprint;

	private String serviceComponentId;
	private UUID netAppBlueprintId;


	@ElementCollection
	private List<String> serviceEndpointIds = new ArrayList<>();

	public ServiceComponent() {
		// JPA only
	}

	/**
	 * Constructor
	 *
	 * @param serviceComponentId ID of the atomic component
	 * @param serviceEndpointIds IDs of the connection end points of the applications
	 */
	public ServiceComponent(
							VerticalServiceBlueprint vsb,
                            String serviceComponentId,
							UUID netAppBlueprintId,
                            List<String> serviceEndpointIds


						   ) {

		this.verticalServiceBlueprint= vsb;
		this.serviceComponentId = serviceComponentId;
		this.netAppBlueprintId=netAppBlueprintId;
		if (serviceEndpointIds != null) this.serviceEndpointIds = serviceEndpointIds;

	}




	/**
	 * @return the componentId
	 */
	public String getServiceComponentId() {
		return serviceComponentId;
	}


	/**
	 * @return the endPointsIds
	 */
	public List<String> getServiceEndpointIds() {
		return serviceEndpointIds;
	}


	@Override
	public void isValid() throws MalformattedElementException {
		if (serviceComponentId == null) throw new MalformattedElementException("VSB atomic component without ID.");
		if (netAppBlueprintId == null) throw new MalformattedElementException("VSB atomic component without NetApp Id.");
	}


	public UUID getNetAppBlueprintId() {
		return netAppBlueprintId;
	}
}
