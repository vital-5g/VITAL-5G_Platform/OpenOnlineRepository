package it.nextworks.catalogue.exceptions;

public class UnAuthorizedRequestException extends Exception {

    public UnAuthorizedRequestException(String msg){
        super(msg);
    }

}
