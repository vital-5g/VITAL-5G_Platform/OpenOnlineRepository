package it.nextworks.catalogue.elements;

import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.Embeddable;

@Embeddable
public class InterfaceAttachedSpec implements DescriptorInformationElement{

    private String file;
    private InterfaceAttachedSpecFormat format;

    public InterfaceAttachedSpec() {
    }

    public InterfaceAttachedSpec(String file, InterfaceAttachedSpecFormat format) {
        this.file = file;
        this.format = format;
    }

    public String getFile() {
        return file;
    }

    public InterfaceAttachedSpecFormat getFormat() {
        return format;
    }


    @Override
    public void isValid() throws MalformattedElementException {
        if(file==null) throw new MalformattedElementException("Interface attached specification without file");
        if(format==null) throw new MalformattedElementException("Interface attached specification without format");
    }
}
