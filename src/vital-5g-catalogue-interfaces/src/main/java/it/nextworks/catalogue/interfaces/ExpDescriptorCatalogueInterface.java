/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.interfaces;


import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.exp.ExpDescriptorInfo;
import it.nextworks.catalogue.exceptions.AlreadyExistingEntityException;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;

import java.util.List;
import java.util.UUID;

public interface ExpDescriptorCatalogueInterface {

	/**
	 * Method to create a new EXPD
	 * 
	 * @param request
	 * @return

	 * @throws MalformattedElementException
	 * @throws AlreadyExistingEntityException
	 * @throws FailedOperationException
	 */
	public UUID onboardExpDescriptor(ExpDescriptor request)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException;
	
	/**
	 * Method to query existing ExpDs
	 * 

	 * @return
	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	List<ExpDescriptorInfo> queryExpDescriptor()
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException;


	ExpDescriptor getExpDescriptor(UUID expdId) throws MalformattedElementException, NotExistingEntityException, FailedOperationException;
	/**
	 * Method to remove a ExpD
	 * 
	 * @param expDescriptorId

	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	public void deleteExpDescriptor(UUID expDescriptorId, boolean force)
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException;


	/**
	 * Method to set an ExpD as used by the Experiment
	 *
	 * @param expDescriptorId
	 * @param experimentId

	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	public void useExpDescriptor(UUID expDescriptorId, UUID experimentId)
			throws  MalformattedElementException, NotExistingEntityException, FailedOperationException;

	/**
	 * Method to release ExpD from Experiment
	 *
	 * @param expDescriptorId
	 * @param experimentId

	 * @throws MalformattedElementException
	 * @throws NotExistingEntityException
	 * @throws FailedOperationException
	 */
	public void releaseExpDescriptor(UUID expDescriptorId, UUID experimentId)
			throws  MalformattedElementException, NotExistingEntityException, FailedOperationException;

}
