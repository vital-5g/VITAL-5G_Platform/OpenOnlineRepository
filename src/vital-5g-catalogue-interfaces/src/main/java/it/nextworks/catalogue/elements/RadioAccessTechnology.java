package it.nextworks.catalogue.elements;

public enum RadioAccessTechnology {
    FIVE_G_NSA,
    FIVE_G_SA,
    NB_IoT,
    LTE_M
}
