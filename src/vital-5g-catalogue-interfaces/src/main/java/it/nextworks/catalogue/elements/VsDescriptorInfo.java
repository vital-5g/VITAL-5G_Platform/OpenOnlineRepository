package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class VsDescriptorInfo implements DescriptorInformationElement {

    @Id
    @GeneratedValue
    private UUID vsDescriptorId;

    private String version;
    private String name;
    private String owner;

    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "vsDescriptorInfo", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private VsDescriptor vsDescriptor;


    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<UUID> activeVsiId = new ArrayList<>();

    public VsDescriptorInfo() {	}

    public UUID getVsDescriptorId() {
        return vsDescriptorId;
    }

    /**
     * @param version
     * @param name

     */
    public VsDescriptorInfo(String version, String name, String owner) {

        this.version = version;
        this.name = name;
        this.owner = owner;

    }

    public String getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    /**
     * @return the activeVsdId
     */
    public List<UUID> getActiveVsiId() {
        return activeVsiId;
    }

    public void addVsi(UUID vsiId) {
        if (!(activeVsiId.contains(vsiId)))
            activeVsiId.add(vsiId);
    }



    public void removeVsi(UUID vsiId) {
        if (activeVsiId.contains(vsiId))
            activeVsiId.remove(vsiId);
    }

    public String getOwner() {
        return owner;
    }


    public VsDescriptor getVsDescriptor() {
        return vsDescriptor;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if (vsDescriptorId == null) throw new MalformattedElementException("VS Descriptor info without VS ID");
        if (version == null) throw new MalformattedElementException("VS Descriptor info without VS version");
        if (name == null) throw new MalformattedElementException("VS Descriptor info without VS name");
    }

}
