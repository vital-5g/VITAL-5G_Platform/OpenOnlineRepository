package it.nextworks.catalogue.interfaces;


import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;

import java.util.UUID;

public interface TranslatorInterface {


    NfvNsInstantiationInfo translateVsd(UUID vsdId) throws NotExistingEntityException, FailedOperationException;

}
