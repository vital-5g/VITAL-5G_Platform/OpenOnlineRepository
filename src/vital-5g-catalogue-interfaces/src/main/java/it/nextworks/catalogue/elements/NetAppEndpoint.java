/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;


@Entity
public class NetAppEndpoint implements DescriptorInformationElement {

	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;


	@JsonIgnore
	@ManyToOne
	private it.nextworks.catalogue.elements.NetAppBlueprint netAppBlueprint;

	private String endPointId;

	private EndPointType type;
	private boolean management;
	private boolean mobileConnection;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@OneToMany(mappedBy = "netAppEndpoint", cascade=CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@LazyCollection(LazyCollectionOption.FALSE)

	private List<SliceProfile> sliceProfile= new ArrayList<>();

	private it.nextworks.catalogue.elements.RadioAccessTechnology radioAccessTechnology;

	private String coverageArea;
	
	public NetAppEndpoint() {
		// JPA only
	}
	
	/**
	 * Constructor
	 * 
	 * @param endPointId ID of the end point
	 * @param type Internal or external
	 * @param management true if it is used for management purposes
	 * @param mobileConnection true if it connects to the radio segment
	 */
	public NetAppEndpoint(
							NetAppBlueprint netAppBlueprint,
							String endPointId,
						  EndPointType type,
						  boolean management,
						  boolean mobileConnection,
						  List<SliceProfile> sliceProfile,
						  String coverageArea,
						  it.nextworks.catalogue.elements.RadioAccessTechnology radioAccessTechnology
						  ) {
		this.endPointId = endPointId;
		this.type=type;
		this.management = management;
		this.mobileConnection = mobileConnection;
		if(sliceProfile!=null)
			this.sliceProfile = sliceProfile;
		this.netAppBlueprint=netAppBlueprint;
		this.coverageArea= coverageArea;
		this.radioAccessTechnology= radioAccessTechnology;
	}

	public RadioAccessTechnology getRadioAccessTechnology() {
		return radioAccessTechnology;
	}

	public List<SliceProfile> getSliceProfile() {
		return sliceProfile;
	}

	public String getCoverageArea() {
		return coverageArea;
	}




	/**
	 * @return the endPointId
	 */
	public String getEndPointId() {
		return endPointId;
	}

	public EndPointType getType() {
		return type;
	}

	/**
	 * @return the management
	 */
	public boolean isManagement() {
		return management;
	}

	/**
	 * @return the ranConnection
	 */
	public boolean isMobileConnection() {
		return mobileConnection;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (endPointId == null || endPointId.isEmpty()){
			throw new MalformattedElementException("NetApp end point without ID");
		}

		for(SliceProfile sliceProfileAux: sliceProfile){
			sliceProfileAux.isValid();
		}


	}

}
