package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class ProvidedInterfaceServiceSpec implements DescriptorInformationElement {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @ManyToOne
    private it.nextworks.catalogue.elements.NetAppBlueprint netAppBlueprint;
    private String interfaceServiceSpecId;

    private String name;
    private String version;
    private String protocol;

    private it.nextworks.catalogue.elements.InterfaceCommType commType;
    private it.nextworks.catalogue.elements.InterfaceDataFormat dataFormat;

    @ElementCollection
    private List<it.nextworks.catalogue.elements.InterfaceAttachedSpec> attachedSpecifications = new ArrayList<>();

    private it.nextworks.catalogue.elements.InterfaceRole role;

    @ElementCollection
    private List<String> endpointIds = new ArrayList<>();

    @ElementCollection
    private Map<String, String> protocolParams = new HashMap<>();


    public ProvidedInterfaceServiceSpec() {
    }

    public ProvidedInterfaceServiceSpec(it.nextworks.catalogue.elements.NetAppBlueprint netAppBlueprint, String interfaceServiceSpecId, String name, String version, String protocol, it.nextworks.catalogue.elements.InterfaceCommType commType, it.nextworks.catalogue.elements.InterfaceDataFormat dataFormat, List<it.nextworks.catalogue.elements.InterfaceAttachedSpec> attachedSpecifications, it.nextworks.catalogue.elements.InterfaceRole role, List<String> endpointIds, Map<String, String> protocolParams) {
        this.netAppBlueprint = netAppBlueprint;
        this.interfaceServiceSpecId = interfaceServiceSpecId;
        this.name = name;
        this.version = version;
        this.protocol = protocol;
        this.commType = commType;
        this.dataFormat = dataFormat;
        this.attachedSpecifications = attachedSpecifications;
        this.role = role;
        this.endpointIds = endpointIds;
        this.protocolParams = protocolParams;
    }

    public NetAppBlueprint getNetAppBlueprint() {
        return netAppBlueprint;
    }



    public String getInterfaceServiceSpecId() {
        return interfaceServiceSpecId;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public String getProtocol() {
        return protocol;
    }

    public InterfaceCommType getCommType() {
        return commType;
    }

    public InterfaceDataFormat getDataFormat() {
        return dataFormat;
    }

    public List<it.nextworks.catalogue.elements.InterfaceAttachedSpec> getAttachedSpecifications() {
        return attachedSpecifications;
    }

    public InterfaceRole getRole() {
        return role;
    }

    public List<String> getEndpointIds() {
        return endpointIds;
    }

    public Map<String, String> getProtocolParams() {
        return protocolParams;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(name==null) throw new MalformattedElementException("Interface specification without name");
        if(interfaceServiceSpecId==null) throw new MalformattedElementException("Interface specification without id");
        if(role==null) throw new MalformattedElementException("Interface specification without role");
        for(InterfaceAttachedSpec attachedSpec : attachedSpecifications){
            attachedSpec.isValid();
        }
    }
}
