package it.nextworks.catalogue.elements;

public enum NetAppType {
    SERVICE,
    COMPONENT
}
