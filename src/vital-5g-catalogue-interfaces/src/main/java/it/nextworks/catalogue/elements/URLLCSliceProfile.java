package it.nextworks.catalogue.elements;

import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.Entity;

@Entity
public class URLLCSliceProfile extends SliceProfile {

    private it.nextworks.catalogue.elements.URLLCSliceProfileParams profileParams;


    public URLLCSliceProfile(){
        super(it.nextworks.catalogue.elements.SliceProfileType.URLLC, null);
    }
    public URLLCSliceProfile(it.nextworks.catalogue.elements.URLLCSliceProfileParams profileParams, NetAppEndpoint netAppEndpoint){
        super(SliceProfileType.URLLC, netAppEndpoint);
        this.profileParams=profileParams;
    }

    public URLLCSliceProfileParams getProfileParams() {
        return profileParams;
    }


    @Override
    public  void isValid() throws MalformattedElementException{
        profileParams.isValid();
    }

}



