package it.nextworks.catalogue.elements.tc;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import java.util.List;

@Embeddable
public class NfvoLcmAction {

    private String nfvoActionId;

    @ElementCollection
    private List<String> inputParameters;

    private String nfvoReferenceId;

    public NfvoLcmAction() {
    }

    public NfvoLcmAction(String nfvoActionId, List<String> inputParameters, String nfvoReferenceId) {
        this.nfvoActionId = nfvoActionId;
        this.inputParameters = inputParameters;
        this.nfvoReferenceId = nfvoReferenceId;
    }

    public String getNfvoActionId() {
        return nfvoActionId;
    }

    public List<String> getInputParameters() {
        return inputParameters;
    }

    public String getNfvoReferenceId() {
        return nfvoReferenceId;
    }
}
