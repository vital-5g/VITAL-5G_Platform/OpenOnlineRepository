/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.catalogue.elements.exp;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.elements.InfrastructureMetric;
import it.nextworks.catalogue.elements.Testbed;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
public class ExpBlueprint  implements DescriptorInformationElement {



    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;


    @JsonIgnore
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
    @JoinColumn(name="expb_info")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private it.nextworks.catalogue.elements.exp.ExpBlueprintInfo expBlueprintInfo;

    private UUID expBlueprintId;

    private String version;
    private String name;
    private String description;


    private Testbed testbed;

    private AccessLevel accessLevel;
    private String useCase;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "blueprint", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<KeyPerformanceIndicator> kpis = new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<InfrastructureMetric> metrics = new ArrayList<>();



    private UUID tcBlueprintId;

    private UUID vsBlueprintId;




    
    public ExpBlueprint() {
    	//JPA only
    }

    public ExpBlueprint(
                        it.nextworks.catalogue.elements.exp.ExpBlueprintInfo expBlueprintInfo,
                        String version,
                        String name,
                        String description,
    		            Testbed testbed,
                        UUID vsBlueprintId,
    		         UUID tcBlueprintId,
                        List<InfrastructureMetric> metrics,
                        AccessLevel accessLevel,
                        String useCase) {
        if(expBlueprintInfo!=null)this.expBlueprintId= expBlueprintInfo.getExpBlueprintId();
        this.expBlueprintInfo = expBlueprintInfo;
        this.version = version;
        this.name = name;
        this.description = description;
        this.testbed= testbed;
        this.vsBlueprintId = vsBlueprintId;

        	this.tcBlueprintId = tcBlueprintId;
        if(metrics!=null)
            this.metrics = metrics;

        this.accessLevel=accessLevel;
        this.useCase=useCase;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public String getVersion() {
        return version;
    }

    public UUID getExpBlueprintId() {
        return expBlueprintId;
    }


    public UUID getVsBlueprintId() {
        return vsBlueprintId;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUseCase() {
        return useCase;
    }

    public String getDescription() {
        return description;
    }

    public ExpBlueprintInfo getExpBlueprintInfo() {
        return expBlueprintInfo;
    }

    /**
	 * @return the tcBlueprintIds
	 */
	public UUID getTcBlueprintId() {
		return tcBlueprintId;
	}



	@Override
    public void isValid() throws MalformattedElementException {
        if(name==null || name.isEmpty())
            throw new MalformattedElementException("ExpBlueprint without name");

        if(version==null || version.isEmpty())
            throw new MalformattedElementException("ExpBlueprint without version");

        if(testbed==null)
            throw new MalformattedElementException("ExpBlueprint without testbed");


        Set<String>  kpiIdSet = kpis.stream()
                        .map(kpi -> kpi.getKpiId())
                        .collect(Collectors.toSet());
        //Check for duplicate kpi ids
        if(kpiIdSet.size()!=kpis.size())
            throw  new MalformattedElementException("Duplicate KPI id inside the ExpBlueprint");
        if (kpis != null) {
            for (KeyPerformanceIndicator kpi : kpis) kpi.isValid();
        }

        Set<String>  metricIdSet = metrics.stream()
                .map(metric -> metric.getMetricId())
                .collect(Collectors.toSet());
        //Check for duplicate kpi ids
        if(metricIdSet.size()!=metrics.size())
            throw  new MalformattedElementException("Duplicate Metric id inside the ExpBlueprint");
        if (metrics != null) {
        	for (InfrastructureMetric m : metrics) m.isValid();
        }

        if (vsBlueprintId == null )
            throw new MalformattedElementException("Experiment blueprint without vertical service blueprint");

        if (tcBlueprintId == null )
            throw new MalformattedElementException("Experiment blueprint without test case");
    }

    public List<KeyPerformanceIndicator> getKpis() {
        return kpis;
    }

    public List<InfrastructureMetric> getMetrics() {
        return metrics;
    }

}
