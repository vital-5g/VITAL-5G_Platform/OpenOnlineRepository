package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class VerticalServiceBlueprintInfo implements DescriptorInformationElement {

    @Id
    @GeneratedValue
    private UUID verticalServiceBlueprintId;

    private String version;
    private String name;
    private String owner;
    private Testbed testbed;

    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "verticalServiceBlueprintInfo", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private it.nextworks.catalogue.elements.VerticalServiceBlueprint verticalServiceBlueprint;


    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<UUID> activeVsdId = new ArrayList<>();

    public VerticalServiceBlueprintInfo() {	}

    public Testbed getTestbed() {
        return testbed;
    }

    /**
     * @param version
     * @param name
     * @param owner
     */
    public VerticalServiceBlueprintInfo(String version, String name, String owner, Testbed testbed) {

        this.version = version;
        this.name = name;
        this.owner =owner;
        this.testbed=testbed;
    }






    /**
     * @return the vsBlueprint
     */
    public it.nextworks.catalogue.elements.VerticalServiceBlueprint getVerticalServiceBlueprint() {
        return verticalServiceBlueprint;
    }

    /**
     * @param verticalServiceBlueprint the vsBlueprint to set
     */
    public void setVerticalServiceBlueprint(VerticalServiceBlueprint verticalServiceBlueprint) {
        this.verticalServiceBlueprint = verticalServiceBlueprint;
    }

    /**
     * @return the vsBlueprintId
     */
    public UUID getVerticalServiceBlueprintId() {
        return verticalServiceBlueprintId;
    }

    /**
     * @return the vsBlueprintVersion
     */
    public String getVersion() {
        return version;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }



    /**
     * @return the activeVsdId
     */
    public List<UUID> getActiveVsdId() {
        return activeVsdId;
    }

    public void addVsd(UUID vsdId) {
        if (!(activeVsdId.contains(vsdId)))
            activeVsdId.add(vsdId);
    }


    public String getOwner() {
        return owner;
    }

    public void removeVsd(UUID vsdId) {
        if (activeVsdId.contains(vsdId))
            activeVsdId.remove(vsdId);
    }



    public void removeAllVsds() {
        this.activeVsdId = new ArrayList<UUID>();
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if (verticalServiceBlueprintId == null) throw new MalformattedElementException("VS Blueprint info without VS ID");
        if (version == null) throw new MalformattedElementException("VS Blueprint info without VS version");
        if (name == null) throw new MalformattedElementException("VS Blueprint info without VS name");
    }

}
