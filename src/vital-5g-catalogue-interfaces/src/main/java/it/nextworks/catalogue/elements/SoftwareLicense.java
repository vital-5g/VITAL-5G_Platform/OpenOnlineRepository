package it.nextworks.catalogue.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

import javax.persistence.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class SoftwareLicense implements DescriptorInformationElement{

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @ManyToOne
    private NetAppBlueprint netAppBlueprint;
    private String softwareLicenseId;
    boolean openLicense = false;

    private String licenseFile;

    private String validationURL;

    private LicenseType type;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ElementCollection
    private Map<String, String > licenseParams  = new HashMap<>();
    
    @ElementCollection
    private List<String> componentIds = new ArrayList<>();

    public SoftwareLicense() {


    }

    public SoftwareLicense(NetAppBlueprint netAppBlueprint,
                           String softwareLicenseId,
                           boolean openLicense,
                           String licenseFile,
                           String validationURL,
                           LicenseType type,
                           List<String> componentIds,
                           Map<String, String > licenseParams) {
        this.softwareLicenseId = softwareLicenseId;
        this.openLicense = openLicense;
        this.licenseFile = licenseFile;
        this.validationURL = validationURL;
        this.type = type;
        this.componentIds = componentIds;
        this.netAppBlueprint = netAppBlueprint;
        this.licenseParams=licenseParams;
    }

    public Map<String, String> getLicenseParams() {
        return licenseParams;
    }

    public NetAppBlueprint getNetAppBlueprint() {
        return netAppBlueprint;
    }

    public String getSoftwareLicenseId() {
        return softwareLicenseId;
    }

    public boolean isOpenLicense() {
        return openLicense;
    }

    public String getLicenseFile() {
        return licenseFile;
    }

    public String getValidationURL() {
        return validationURL;
    }

    public LicenseType getType() {
        return type;
    }

    public List<String> getComponentIds() {
        return componentIds;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(Paths.get(licenseFile).isAbsolute())
            throw new MalformattedElementException("Invalid license file path");

    }
}
