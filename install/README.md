# Installation pre-requisites
## Docker and docker-compose
The preferred way for installing and running the VITAL-5G open online repository is via docker and docker compose. Therefore you will need an environment with these two tools

## Gitlab token

In order to automatically clone and compile the source code the build phase of the docker container uses a gitlab token. You will need to generate this, as explained in [link](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), and provide it as a build argument (make sure you grant read_repo permissions).

# Install and execute
## docker-compose.yml

Inside the docker-compose.yml file (in the docker-compose folder), you will need to update the volume which will we used to store the postgres DB data (by default /tmp/posgres-data) 

## Build

Inside the docker-compose folder run: docker-compose build --build-arg GITLAB_TOKEN="<YOUR_GITLAB_TOKEN>" 

This will automatically generate a docker image with the latest version of the VITAL-5G Open online repository

## Execute
Inside the docker-compose folder run: docker-compose up


