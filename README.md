![vital-5g-logo](https://www.vital5g.eu/wp-content/uploads/2020/12/vital-logo-web.png) 
 # Network Applications, Services Experiment Catalogue


[VITAL-5G D2.1](https://www.vital5g.eu/wp-content/uploads/2022/01/VITAL5G_D2.1_Initial_NetApps_blueprints_and_Open_Repository_design_Final.pdf) 

*The Network Applications, Services and Experiments Catalogue is the component responsible for storing Network Applications packages and blueprints, Vertical Service Blueprints and Descriptors (VSB and VSD), Experiment Blueprints and Descriptors (ExpB and ExpD). The Catalogue maintains the synchronization with the NFVO catalogues at the three VITAL-5G sites, to keep the map between Network Application and Vertical Service Blueprints and the corresponding VNF packages and Network Service Descriptors (NSD), respectively.*

## Software architecture
The following figure illustrates the software architecture of this module, highlighting the components currently supported

![vital-5g-catalogue-software-architecture](docs/vital-5g-catalogue-software-architecture.png)

The source code is available in the [SRC](src/) folder of the repository. The code is structured in two different maven projects:
* *vital-5g-catalogue-interfaces* : Containing the JAVA based interfaces and and classes representing the information models of the blueprints and descriptores used in the catalogue.
* *vital-5g-catalogue*: Containing the implementation of the catalogues, the rest controllers and the logic for the management of the different blueprint and descriptors.

## Deployment 

A docker-compose based deployment is detailed in [Readme](installation/)

## Folder structure
* [src](src/): Contains the source code of this module
* [API](API/): OpenAPI specification of the interfaces by this module and Postman collections
* [docs](docs/): Documentation of the module, including example descriptors using during the integration tests  

## License
This module has been developed by [Nextworks](www.nextworks.it) and licensed under the open source [Apache License v2.0](https://www.apache.org/licenses/LICENSE-2.0)



